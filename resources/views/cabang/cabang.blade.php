@extends('component.master_'.Session::get('role'))

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Cabang | Regional {{$kota}}</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAdd">Cabang Baru</button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered datatable mdl-data-table" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>Nama Cabang</center></th>
                    <th><center>Alamat</center></th>
                    <th><center>No Telp</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="modal" tabindex="-1" id="modalAdd" role="dialog">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Cabang Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{route('regional.store_cabang')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                <label for="">Kota</label><br>
                  <input type="text" class="form-control" value="{{$kota}}" disabled>

                  <label for="">Nama Cabang</label><br>
                  <input type="number" name="nama_cabang" class="form-control" >

                  <label for="">No Telp</label><br>
                  <input type="number" name="no_telp" class="form-control" >
                
                  <label for="">Alamat</label>
                  <textarea rows="10" cols="10" required class="form-control" type="text" id="alamat" name="alamat"></textarea><br>

                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="modal" tabindex="-1" id="modalEdit" role="dialog">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Edit Cabang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{route('regional.update_cabang')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" class="form-control" id="id" name="id">

                  <label for="">Kota</label><br>
                  <input type="text" class="form-control" value="{{$kota}}" disabled>

                  <label for="">Nama Cabang</label><br>
                  <input type="text" id="nama_cabang_edit" name="nama_cabang_edit" class="form-control" >

                  <label for="">No Telp</label><br>
                  <input type="number" name="no_telp_edit" id="no_telp_edit" class="form-control" >
                
                  <label for="">Alamat</label>
                  <textarea rows="10" cols="10" required class="form-control" type="text" id="alamat_edit" name="alamat_edit"></textarea><br>

                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){

          $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('regional.get_cabang') }}',
            "columns": [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'nama_cabang', name: 'nama_cabang'},
              {data: 'alamat_cabang', name: 'alamat_cabang'},
              {data: 'no_telp', name: 'no_telp'},
              {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
          ],
          'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ]
        });

        $(document).on('click', '#btn_edit', function() {

            var id  = $(this).data("id");
            var url = '{{ route('regional.show_cabang', ":id") }}'
            url     = url.replace(':id', id)

            $.get(url , function(data, status){
              let element = JSON.parse(data)
              console.log(element.nama_cabang)
              $('#modalEdit').modal('show')
              $('#id').val(element.id)
              $('#nama_cabang_edit').val(element.nama_cabang)
              $('#alamat_edit').val(element.alamat)
              $('#no_telp_edit').val(element.no_telp)
            })

          })

        })
      </script>
@endsection