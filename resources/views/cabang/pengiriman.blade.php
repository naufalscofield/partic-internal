@extends('component.master_'.Session::get('role'))

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 id="nama_cabang" class="h3 mb-2 text-gray-800">Pengiriman Dari Cabang {{$nama}}</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <button class="btn btn-success" data-toggle="modal" id="btn_add" data-target="#modalAdd">+ Pengiriman Baru</button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered datatable mdl-data-table" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>No Resi</center></th>
                    <th><center>Kota Tujuan</center></th>
                    <th><center>Jenis Pengiriman</center></th>
                    <th><center>Jenis Pengangkutan</center></th>
                    <th><center>Berat / Dimensional / Total Unit</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="modal" tabindex="-1" id="modalEdit" role="dialog">
          <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Detail Pengiriman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="col-xl-12">
                  <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">Data Pengiriman</h6>
                    </div>
                    <div class="card-body col-md-12">
                      @csrf
                      <input type="hidden" name="id" id="id">
                      <b><i class="fa fa-number"></i><label for="">Nomor Resi</label></b><br>
                      <p id="no_resi"></p><br>

                      <b><label for="">Jenis Pengangkutan</label></b><br>
                      <p id="jenis_pengangkutan"></p><br>

                      <b><label for="">Deskripsi Barang</label></b><br>
                      <p id="deskripsi_barang"></p><br>

                      <b><label for="">Ukuran Pengiriman (Berat / Dimensi / Unit )</label></b><br>
                      <p id="ukuran"></p><br>

                      <b><label for="">Jenis Pengiriman</label></b><br>
                      <p id="jenis_pengiriman"></p><br>

                      <b><label for="">Asal</label></b><br>
                      <p id="asal"></p><br>

                      <b><label for="">Tujuan</label></b><br>
                      <p id="tujuan"></p><br>

                      <b><label for="">Alamat Tujuan</label></b><br>
                      <p id="alamat_tujuan"></p><br>

                      <b><label for="">Tarif</label></b><br>
                      <p id="tarif"></p><br>

                      <b><label for="">Kode / ID Kelompok Pengiriman</label></b><br>
                      <p id="id_kelompok_pengiriman"></p><br>
                      
                      <b><label for="">Diterima Oleh Cabang</label></b><br>
                      <p id="cabang"></p><br>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xl-6">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Data Pengirim</h6>
                      </div>
                      <div class="card-body col-md-6">
                        @csrf
                        <b><label for="">Nama Pengirim</label></b><br>
                        <p id="nama_pengirim"></p><br>

                        <b><label for="">No Telp Pengirim</label></b><br>
                        <p id="no_telp_pengirim"></p><br>

                        <b><label for="">Member</label></b><br>
                        <p id="id_member"></p><br>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-6">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Data Penerima</h6>
                      </div>
                      <div class="card-body col-md-6">
                        @csrf
                        <b><label for="">Nama Penerima</label></b><br>
                        <p id="nama_penerima"></p><br>

                        <b><label for="">No Telp Penerima</label></b><br>
                        <p id="no_telp_penerima"></p><br>

                        <b><label style="color:white"for="">blank</label></b><br>
                        <p id=""></p><br>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xl-6">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Data Kurir</h6>
                      </div>
                      <div class="card-body col-md-6">
                        @csrf
                        <b><label for="">ID Mitra Kurir</label></b><br>
                        <p id="id_mitra"></p><br>

                        <b><label for="">Nama Mitra Kurir</label></b><br>
                        <p id="nama_mitra"></p><br>

                        <b><label for="">No Telp Mitra Kurir</label></b><br>
                        <p id="no_telp_mitra"></p><br>

                        <b><label for="">ID Tarif Line</label></b><br>
                        <p id="id_tarif_line"></p><br>

                        <b><label for="">Tarif Line</label></b><br>
                        <p id="tarif_line"></p><br>

                        <b><label style="color:white"for="">blank</label></b><br>
                        <p id=""></p><br>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-6">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Keterangan</h6>
                      </div>
                      <div class="card-body col-md-6">
                        @csrf
                        <b><label for="">Tanggal Masuk</label></b><br>
                        <p id="tanggal_masuk"></p><br>

                        <b><label for="">Tanggal Pengiriman</label></b><br>
                        <p id="tanggal_pengiriman"></p><br>

                        <b><label for="">Perkiraan Tanggal Sampai</label></b><br>
                        <p id="perkiraan_tanggal_sampai"></p><br>

                        <b><label for="">Tanggal Sampai</label></b><br>
                        <p id="tanggal_sampai"></p><br>

                        <b><label for="">Jumlah Hari Telat</label></b><br>
                        <p id="jumlah_hari_telat"></p><br>

                        <b><label for="">Keterangan Tambahan</label></b><br>
                        <p id="keterangan"></p><br>

                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-12">
                  <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">Status Pengiriman</h6>
                    </div>
                    <div class="card-body col-md-12">
                      <div class="table-responsive">
                        <table class="display" style="width:100%" id="dataTableStatus" cellspacing="0">
                          <thead>
                            <tr>
                              <th><center>No</center></th>
                              <th><center>Tanggal</center></th>
                              <th><center>Status</center></th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Tambah Pengiriman</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form action="{{route('cabang.store_pengiriman')}}" method="POST">
            @csrf
            <div class="modal-body">
              <div class="col-xl-12">
                <div class="card shadow mb-6">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Data Barang</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="form-group">
                      <b><label for="">Jenis Pengangkutan</label></b>
                      <select class="form-control" required name="jenis_pengangkutan" id="jenis_pengangkutan_input">
                        <option value="">--Pilih Jenis Pengangkutan--</option>
                        <option value="kilo">Kilo</option>
                        <option value="dimensional">Dimensional</option>
                        <option value="koli">Koli</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <b><label for="">Jenis Barang</label></b>
                      <select class="form-control" required name="jenis_barang" id="">
                        <option value="">--Pilih Jenis Barang--</option>
                        <option value="makanan">Makanan</option>
                        <option value="elektronik">Elektronik</option>
                        <option value="pakaian">Pakaian</option>
                        <option value="kendaraan">Kendaraan</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <b><label for="">Jenis Pengiriman</label></b>
                      <select class="form-control" required name="jenis_pengiriman" id="jenis_pengiriman_input">
                        <option value="">--Pilih Jenis Pengiriman--</option>
                        <option value="regular">Regular</option>
                        <option value="express">Express</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <b><label for="">Nama Barang</label></b>
                      <input required type="text" class="form-control" name="nama_barang" id="nama_barang_input">
                    </div>

                    <div id="berat" class="form-group">
                      <b><label for="">Berat Barang</label></b>
                      <input required type="number" value="0" class="form-control" name="berat">
                    </div>

                    <div id="dimensi" class="form-group">
                      <b><label for="">Dimensi Barang</label></b>
                      <input required type="number" value="0" class="form-control" name="dimensi">
                    </div>

                    <div id="total_unit" class="form-group">
                      <b><label for="">Total Unit Barang</label></b>
                      <input required type="number" value="0" class="form-control" name="total_unit">
                    </div>

                    <input type="hidden" id="token_raja">

                    <div class="form-group">
                      <label for="">Kota Asal</label>
                      <input type="text" class="form-control" value="{{$kota}}" name="kota_asal" readonly id="kota_asal_input">
                    </div>

                    <div class="form-group">
                      <b><label for="">Provinsi Tujuan</label></b>
                      <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="provinsi" name="provinsi_tujuan">
                        <option value="">--Pilih Provinsi--</option>
                      </select>
                    </div>
                  
                    <div class="form-group">
                      <b><label for="">Kota Tujuan</label></b>
                      <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="kota_tujuan_input" name="kota_tujuan">
                        <option value="">--Pilih Kota--</option>
                      </select>
                    </div>
                  
                    <div class="form-group">
                      <b><label for="">Kecamatan Tujuan</label></b>
                      <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="kecamatan" name="kecamatan_tujuan">
                        <option value="">--Pilih Kecamatan--</option>
                      </select>
                    </div>
                  
                    <div class="form-group">
                      <b><label for="">Kelurahan Tujuan</label></b>
                      <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="kelurahan" name="kelurahan_tujuan">
                        <option value="">--Pilih Kelurahan--</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <b><label for="">Kode Pos Tujuan</label></b>
                      <input required type="number" value="0" class="form-control" name="kode_pos_tujuan">
                    </div>

                    <div class="form-group">
                      <b><label for="">Alamat Tujuan</label></b>
                      <textarea name="alamat_tujuan" class="form-control" id="" cols="30" rows="10"></textarea>
                    </div>

                    <div class="form-group">
                      <b><label for="">Keterangan</label></b>
                      <textarea name="keterangan" class="form-control" id="" cols="30" rows="10"></textarea>
                    </div>


                    <div class="form-group">
                      <b><label for="">Tarif</label></b>
                      <input required id="tarif_input" type="number" value="0" class="form-control" name="tarif">
                      <input required id="durasi_input" type="hidden" value="0" class="form-control" name="durasi">
                    </div>
                  
                  </div>
                </div>
              </div>
              <br>
              <div class="col-xl-12">
                <div class="card shadow mb-6">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Data Pengirim</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="form-group">
                      <b><label for="">ID Member</label></b>
                      <input type="number" value="0" name="id_member" class="form-control">
                    </div>
                    
                    <div class="form-group">
                      <b><label for="">Nama Pengirim</label></b>
                      <input type="text" name="nama_pengirim" class="form-control">
                    </div>
                    
                    <div class="form-group">
                      <b><label for="">No Telp Pengirim</label></b>
                      <input type="text" name="no_telp_pengirim" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <div class="col-xl-12">
                <div class="card shadow mb-6">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Data Penerima</h6>
                  </div>
                  <div class="card-body col-md-12">
                  
                    <div class="form-group">
                      <b><label for="">Nama Penerima</label></b>
                      <input required type="text" name="nama_penerima" class="form-control">
                    </div>
                    
                    <div class="form-group">
                      <b><label for="">No Telp Penerima</label></b>
                      <input required type="text" name="no_telp_penerima" class="form-control">
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Input Pengiriman</button>
            </div>
          </form>
          </div>
        </div>
      </div>

      </div>
      {{-- <script src="{{URL::to('/')}}../node_modules/moment/moment.js"></script> --}}
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){
          $('#provinsi').select2();
          $('#kota').select2();
          $('#provinsi_edit').select2();
          $('#kota_edit').select2();
          $('#berat').hide()
          $('#dimensi').hide()
          $('#total_unit').hide()

          $('#jenis_pengangkutan_input').change(function(){
            if ($(this).val() == 'kilo') {
              $('#berat').show()
              $('#dimensi').hide()
              $('#total_unit').hide()
            } else if ($(this).val() == 'koli') {
              $('#berat').hide()
              $('#dimensi').hide()
              $('#total_unit').show()
            } else {
              $('#berat').hide()
              $('#dimensi').show()
              $('#total_unit').hide()
            }
          })

          $.get("https://x.rajaapi.com/poe", function(data, status){
          const tokenraja = data.token
          $('#token_raja').val(tokenraja)

          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/provinsi", function(dataProv, statusProv){
              // console.log(dataProv.data)
              $.each(dataProv.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi');
            });
          });

        });

        $('#provinsi').change(function(){
          $('#kota_tujuan_input').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_tujuan_input');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_tujuan_input');
            });
          });
        })

        $('#kota_tujuan_input').change(function(){
          $('#kecamatan').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kecamatan?idkabupaten="+id, function(dataKecamatan, statusKecamatan){
              console.log(dataKecamatan.data)
                $('<option>').val("").text("--Pilih Kecamatan--").appendTo('#kecamatan');
              $.each(dataKecamatan.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kecamatan');
            });
          });

          var jenisPengangkutan  = $('#jenis_pengangkutan_input').val()
          var jenisPengiriman    = $('#jenis_pengiriman_input').val()
          var namaBarang         = $('#nama_barang_input').val()
          var kotaAsal           = $('#kota_asal_input').val()
          var kotaTujuan         = $('#kota_tujuan_input').val()

          var url = '{{ route("cabang.get_tarif") }}';  
          
          $.ajax({
            type: "POST",
            url: url,
            data: { 
              jenisPengangkutan: jenisPengangkutan,
              jenisPengiriman: jenisPengiriman,
              namaBarang: namaBarang,
              kotaAsal: kotaAsal,
              kotaTujuan: kotaTujuan,
              _token: '{{csrf_token()}}' },
            success: function (data) {
              var el = JSON.parse(data)
              $('#tarif_input').val(el.tarif)
              $('#durasi_input').val(el.durasi)
            },
            error: function (data, textStatus, errorThrown) {
                console.log(data);

            },
          });


        })

        $('#kecamatan').change(function(){
          $('#kelurahan').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kelurahan?idkecamatan="+id, function(dataKelurahan, statusKelurahan){
              console.log(dataKelurahan.data)
                $('<option>').val("").text("--Pilih Kelurahan--").appendTo('#kelurahan');
              $.each(dataKelurahan.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kelurahan');
            });
          });
        })

          $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('cabang.get_pengiriman') }}',
            "columns": [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'no_resi', name: 'no_resi'},
              {data: 'kota_tujuan', name: 'kota_tujuan'},
              {data: 'jenis_pengiriman', name: 'jenis_pengiriman'},
              {data: 'jenis_pengangkutan', name: 'jenis_pengangkutan'},
              {data: 'ukuran', name: 'ukuran'},
              {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
          ],
          'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ]
        });

        $(document).on('click', '#btn_edit', function() {
          var id  = $(this).data("id");
          console.log(id)
          var url = '{{ route('cabang.show_pengiriman', ":id") }}'
          url     = url.replace(':id', id)

            $.get(url , function(data, status){
              let element = JSON.parse(data)
              console.log(element)
              $('#modalEdit').modal('show')
              $('#no_resi').text(element.no_resi)
              $('#jenis_pengangkutan').text(element.jenis_pengangkutan)
              $('#deskripsi_barang').text(element.jenis_barang + ' - '+ element.nama_barang)
              if (element.jenis_pengangkutan == 'kilo')
              {
                $('#ukuran').text(element.berat + ' Kilo')
              } else if (element.jenis_pengangkutan == 'koli')
              {
                $('#ukuran').text(element.total_unit + ' Pcs')
              } else {
                $('#ukuran').text(element.dimensi + ' Meter Persegi')
              }
              $('#jenis_pengiriman').text(element.jenis_pengiriman)
              $('#asal').text(element.kota_asal)
              $('#tujuan').text(element.kota_tujuan)
              $('#alamat_tujuan').text(element.kecamatan_tujuan + ', '+ element.kelurahan_tujuan + ', ' + element.alamat_tujuan + ', ' + element.kode_pos_tujuan)
              $('#tarif').text('Rp.'+ element.tarif)
              $('#id_kelompok_pengiriman').text(element.id_kelompok_pengiriman)
              $('#cabang').text(element.nama_cabang)
              $('#nama_pengirim').text(element.nama_pengirim)
              $('#no_telp_pengirim').text(element.no_telp_pengirim)
              if (element.id_member == null) {
                $('#id_member').text('TIDAK') 
              } 
              else {
              $('#id_member').text('YA')
              } 
              $('#nama_penerima').text(element.nama_penerima)
              $('#no_telp_penerima').text(element.no_telp_penerima)
              $('#id_mitra').text(element.id_mitra)
              $('#nama_mitra').text(element.nama_mitra)
              $('#no_telp_mitra').text(element.no_telp_mitra)
              $('#id_tarif_line').text(element.id_tarif_line)
              $('#tarif_line').text(element.tarif_line)
              $('#tanggal_masuk').text(element.tanggal_masuk)
              $('#tanggal_pengiriman').text(element.tanggal_pengiriman)
              $('#perkiraan_tanggal_sampai').text(element.perkiraan_tanggal_sampai)
              $('#tanggal_sampai').text(element.tanggal_sampai)
              $('#jumlah_hari_telat').text(element.jumlah_hari_telat)
              $('#keterangan').text(element.keterangan)

              var t = $('#dataTableStatus').DataTable();
              var counter = 1;

              $.each(element.status_pengiriman, function(index, value){
                var tanggal = moment(value.tanggal).format('dddd, DD MMMM YYYY')
                t.row.add( [
                    counter,
                    tanggal,
                    value.status,
                ] ).draw( false );
        
                counter++;
              })

            })


        })

        $('#provinsi_edit').change(function(){
          $('#kota_edit').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_edit');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_edit');
            });
          });
        })

        })
      </script>
@endsection