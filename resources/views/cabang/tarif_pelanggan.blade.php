@extends('component.master_'.Session::get('role'))

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Tarif Pelanggan</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAdd">Tarif Baru</button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered datatable mdl-data-table" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>Kota Asal</center></th>
                    <th><center>Kota Tujuan</center></th>
                    <th><center>Tarif</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="modal" tabindex="-1" id="modalAdd" role="dialog">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Tarif Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{route('regional.store_tarif_pelanggan')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" id="token_raja">
                  <label for="">Provinsi Asal</label><br>
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="provinsi_asal" name="provinsi_asal"><br>
                    <option value="">--Pilih Provinsi--</option>
                  </select><br>
                  
                  <label for="">Kota Asal</label><br>
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="kota_asal" name="kota_asal">
                    <option value="">--Pilih Kota--</option>
                  </select><br>


                  <input type="hidden" id="token_raja">
                  <label for="">Provinsi Tujuan</label><br>
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="provinsi_tujuan" name="provinsi_tujuan"><br>
                    <option value="">--Pilih Provinsi--</option>
                  </select><br>
                  
                  <label for="">Kota Tujuan</label><br>
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="kota_tujuan" name="kota_tujuan">
                    <option value="">--Pilih Kota--</option>
                  </select><br>

                  <label for="">Tarif</label><br>
                  <input required class="form-control" type="number" id="tarif" name="tarif"><br>

              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
                </form>
            </div>
          </div>
        </div>

        <div class="modal" tabindex="-1" id="modalEdit" role="dialog">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Edit Tarif</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{route('regional.update_tarif_pelanggan')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" id="token_raja">
                  <input type="hidden" id="id_tarif" name="id_tarif">
                  <label for="">Provinsi Asal</label><br>
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="provinsi_asal_edit" name="provinsi_asal_edit"><br>
                    <option value="">--Pilih Provinsi--</option>
                  </select><br>
                  
                  <label for="">Kota Asal</label> : <b><label id="kota_asal_edit_label"></label></b>
                  <input type="hidden" value="kota_asal_edit_input">
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="kota_asal_edit" name="kota_asal_edit">
                    <option value="">--Pilih Kota--</option>
                  </select>

                  <input type="hidden" id="token_raja">
                  <label for="">Provinsi Tujuan</label><br>
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="provinsi_tujuan_edit" name="provinsi_tujuan_edit"><br>
                    <option value="">--Pilih Provinsi--</option>
                  </select><br>
                  
                  <label for="">Kota Tujuan</label> : <b><label id="kota_tujuan_edit_label"></label></b>
                  <input type="hidden" value="kota_tujuan_edit_input">
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="kota_tujuan_edit" name="kota_tujuan_edit">
                    <option value="">--Pilih Kota--</option>
                  </select>

                  <label for="">Tarif</label>
                  <input required class="form-control" type="number" id="tarif_edit" name="tarif_edit"><br>

              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
                </form>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){

          $('#provinsi_asal').select2();
          $('#kota_asal').select2();
          $('#provinsi_tujuan').select2();
          $('#kota_tujuan').select2();
          $('#provinsi_asal_edit').select2();
          $('#kota_asal_edit').select2();
          $('#provinsi_tujuan_edit').select2();
          $('#kota_tujuan_edit').select2();

          $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('regional.get_tarif_pelanggan') }}',
            "columns": [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'asal', name: 'asal'},
              {data: 'tujuan', name: 'tujuan'},
              {data: 'tarif', name: 'tarif'},
              {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
          ],
          'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ]
        });

        $.get("https://x.rajaapi.com/poe", function(data, status){
          const tokenraja = data.token
          $('#token_raja').val(tokenraja)

          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/provinsi", function(dataProv, statusProv){
              $.each(dataProv.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi_asal');
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi_tujuan');
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi_asal_edit');
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi_tujuan_edit');
            });
          });

        });

        $('#provinsi_asal').change(function(){
          $('#kota_asal').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_asal');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_asal');
            });
          });
        })

        $('#provinsi_tujuan').change(function(){
          $('#kota_tujuan').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_tujuan');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_tujuan');
            });
          });
        })

        $('#provinsi_asal_edit').change(function(){
          $('#kota_asal_edit').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_asal_edit');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_asal_edit');
            });
          });
        })
        
        $('#kota_asal_edit').change(function(){
          $('#kota_asal_edit_input').empty()
          var id = $(this).find(':selected').attr('data-id')
          var val = $(this).val()
          $('#kota_asal_edit_input').val(val)
          $('#kota_asal_edit_label').text(val)
        })

        $('#kota_tujuan_edit').change(function(){
          $('#kota_tujuan_edit_input').empty()
          var id = $(this).find(':selected').attr('data-id')
          var val = $(this).val()
          $('#kota_tujuan_edit_input').val(val)
          $('#kota_tujuan_edit_label').text(val)
        })

        $('#provinsi_tujuan_edit').change(function(){
          $('#kota_tujuan_edit').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_tujuan_edit');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_tujuan_edit');
            });
          });
        })

        $(document).on('click', '#btn_edit', function() {
          

          var id  = $(this).data("id");
          var url = '{{ route('regional.show_tarif_pelanggan', ":id") }}'
          url     = url.replace(':id', id)

          $.get(url , function(data, status){
            let element = JSON.parse(data)
            console.log(element)
            $('#modalEdit').modal('show')
            $('#id_tarif').val(element.id)
            // $('#provinsi_asal_edit').val(element.provinsi)
            $('#kota_asal_edit_label').text(element.asal)
            $('#kota_tujuan_edit_label').text(element.tujuan)
            $('#kota_asal_edit_input').val(element.asal)
            $('#kota_tujuan_edit_input').val(element.tujuan)
            $('#tarif_edit').val(element.tarif)
          })

          var tokenraja = $('#token_raja').val()

          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/provinsi", function(dataProv, statusProv){
              $.each(dataProv.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi_edit');
            });
          });
        })

        $('#provinsi_edit').change(function(){
          $('#kota_edit').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_edit');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_edit');
            });
          });
        })
          // var tokenraja = $('#token_raja').val()

       

          // function readURLEdit(input) {
          //   if (input.files && input.files[0]) {
          //     var reader = new FileReader();
              
          //     reader.onload = function(e) {
          //       $('#gambar_edit_preview').attr('src', e.target.result);
          //     }
              
          //     reader.readAsDataURL(input.files[0]); // convert to base64 string
          //   }
          // }

          // $("#gambar_edit").change(function() {
          //   readURLEdit(this);
          // });

          // function readURL(input) {
          //   if (input.files && input.files[0]) {
          //     var reader = new FileReader();
              
          //     reader.onload = function(e) {
          //       $('#gambar_preview').attr('src', e.target.result);
          //     }
              
          //     reader.readAsDataURL(input.files[0]); // convert to base64 string
          //   }
          // }

          // $("#gambar").change(function() {
          //   readURL(this);
          // });

        })
      </script>
@endsection