<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Partic &mdash; Partner Logistik Anda</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script
    src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous">
    </script>

    <style>
        h5 {
            color : #e534ab
        }
    </style>

<link rel="icon" type="image/png" href="{{URL::to('/')}}/img/partic-logo.ico"/>
<link rel="stylesheet" href="{{URL::to('/')}}/logistics/fonts/icomoon/style.css">

<link rel="stylesheet" href="{{URL::to('/')}}/logistics/css/bootstrap.min.css">
<link rel="stylesheet" href="{{URL::to('/')}}/logistics/css/magnific-popup.css">
<link rel="stylesheet" href="{{URL::to('/')}}/logistics/css/jquery-ui.css">
<link rel="stylesheet" href="{{URL::to('/')}}/logistics/css/owl.carousel.min.css">
<link rel="stylesheet" href="{{URL::to('/')}}/logistics/css/owl.theme.default.min.css">

<link rel="stylesheet" href="{{URL::to('/')}}/logistics/css/bootstrap-datepicker.css">

<link rel="stylesheet" href="{{URL::to('/')}}/logistics/fonts/flaticon/font/flaticon.css">

<link rel="stylesheet" href="{{URL::to('/')}}/logistics/css/aos.css">

<link rel="stylesheet" href="{{URL::to('/')}}/logistics/css/style.css">

<div class="site-section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
            <img src="{{URL::to('/')}}/img/partic-logo-trans.png" style="height:100px;width:90px"><br>
            <center>
                <img src="{{URL::to('/')}}/img/wrong.png" style="height:300px;width:480px">
                <h2>Whoops, anda tidak memiliki akses untuk halaman ini!</h2>
                <hr>
                <h5 style="color:#e534ab">Menuju halaman dashboard dalam <span id="seconds"></span> detik</h5>
            </center>
        </div>
      </div>
    </div>
  </div>
    <input type="hidden" id="role" value="{{ Session::get('role') }}">

  <script>
        var seconds = 10; // seconds for HTML
        var foo; // variable for clearInterval() function

        function redirect() {
            var role = $('#role').val()
            if (role == 'ceo')
            {
                document.location.href = "{{route('dashboard')}}";
            } else if (role == 'regional') {
                document.location.href = "{{route('regional.dashboard')}}";
            } else {
                document.location.href = "{{route('cabang.dashboard')}}";
            }
        }

        function updateSecs() {
            document.getElementById("seconds").innerHTML = seconds;
            seconds--;
            if (seconds == -1) {
                clearInterval(foo);
                redirect();
            }
        }

        function countdownTimer() {
            foo = setInterval(function () {
                updateSecs()
            }, 1000);
        }

        countdownTimer();
  </script>