@extends('Home/master')

@section('content')
    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(/logistics/images/hero_bg_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
            <h1 class="text-white font-weight-light text-uppercase font-weight-bold">Daftar Perusahaan</h1>
          </div>
        </div>
      </div>
    </div>  
    
  <form action="{{route('registrasi_perusahaan')}}" method="POST" enctype="multipart/form-data">
    @CSRF
    <div class="site-section bg-light">
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <div class="p-4 mb-3 bg-white">
              <h3>Biodata CEO</h3>
              <div class="row form-group">
                <br>
                <div class="col-md-12">
                  <label class="text-black" for="nama_lengkap">Nama Lengkap</label>
                  <input type="text" required id="nama_lengkap" name="nama_lengkap" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="tempat_lahir">Kota Lahir</label> 
                  <input type="text" required id="tempat_lahir" name="tempat_lahir" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="tanggal_lahir">Tanggal Lahir</label> 
                  <input type="date" required id="tanggal_lahir" name="tanggal_lahir" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="text-black" for="alamat">Alamat</label> 
                  <textarea name="alamat" required id="alamat" cols="30" rows="7" class="form-control"></textarea>
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="no_telp">No Telp</label> 
                  <input type="text" required id="no_telp" name="no_telp" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="foto_ktp">Foto KTP</label> 
                  <input type="file" required id="foto_ktp" name="foto_ktp" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="nik">NIK</label> 
                  <input type="text" required id="nik" name="nik" class="form-control">
                </div>
              </div>
              
              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="foto_npwp">Foto NPWP</label> 
                  <input type="file" required id="foto_npwp" name="foto_npwp" class="form-control">
                </div>
              </div>
             
              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="foto_diri">Foto Diri</label> 
                  <input type="file" required id="foto_diri" name="foto_diri" class="form-control">
                </div>
              </div>
            </div>
          </div>

          <div class="col-md-3">
            <div class="p-4 mb-3 bg-white">
              <h3>Akun CEO</h3>
              <div class="row form-group">
                <br>
                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label>
                  <input type="email" required id="email" name="email" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="password">Password</label> 
                  <input type="password" required id="password" name="password" class="form-control">
                  <b><p id="passwordStrenght"></p></b>
                </div>
              </div>

            </div>
          </div>

          <div class="col-md-5">
            <div class="p-4 mb-3 bg-white">
              <h3>Data Perusahaan</h3>
              <div class="row form-group">
                <br>
                <div class="col-md-12">
                  <label class="text-black" for="nama_perusahaan">Nama Perusahaan</label>
                  <input type="text" required id="nama_perusahaan" name="nama_perusahaan" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="email_perusahaan">Email Perusahaan</label> 
                  <input type="email" required id="email_perusahaan" name="email_perusahaan" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="logo">Logo Perusahaan</label> 
                  <input type="file" required id="logo" name="logo" class="form-control">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="text-black" for="alamat_perusahaan">Alamat Perusahaan</label> 
                  <textarea name="alamat_perusahaan" required id="alamat_perusahaan" cols="30" rows="7" class="form-control"></textarea>
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="no_telp_perusahaan">No Telp Perusahaan</label> 
                  <input type="text" required id="no_telp_perusahaan" name="no_telp_perusahaan" class="form-control">
                </div>
              </div>

              <p class="mb-0 font-weight-bold">Setuju Persyaratan dan Ketentuan</p>
              <p class="mb-4">Dengan mendaftarkan perusahaan anda ke Partic, maka anda sebagai CEO atau Stakeholder yang bertanggung jawab untuk pendaftaran perusahaan ini, telah menyetujui persyaratan dan ketentuan yang diberikan oleh Partic.</p>

              <div class="row form-group">
                <div class="col-md-12">
                  <input type="submit" value="Daftar Perusahaan" class="btn btn-primary py-2 px-4 text-white">
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </form>

    <script>

    $(document).ready(function(){

      $("#password").keyup(function() {
        var pass = $("#password").val();

        var strength = 1;
        var arr = [/.{5,}/, /[a-z]+/, /[0-9]+/, /[A-Z]+/, /[!@#$%^&*]+/];
        jQuery.map(arr, function(regexp) {
          if(pass.match(regexp))
            strength++;
        });

        if (strength == 1){
          $('#passwordStrenght').html('')
        } else if (strength == 2){
          $('#passwordStrenght').html('Kekuatan Password : Sangat Lemah')
          $('#passwordStrenght').css('color','Red')
        } else if (strength == 3){
          $('#passwordStrenght').html('Kekuatan Password : Lemah')
          $('#passwordStrenght').css('color','Crimson')
        } else if (strength == 4){
          $('#passwordStrenght').html('Kekuatan Password : Cukup')
          $('#passwordStrenght').css('color','Gold')
        } else if (strength == 5){
          $('#passwordStrenght').html('Kekuatan Password : Kuat')
          $('#passwordStrenght').css('color','Lime')
        } else if (strength == 6){
          $('#passwordStrenght').html('Kekuatan Password : Sangat Kuat')
          $('#passwordStrenght').css('color','LimeGreen')
        } else if (strength == 0){
          $('#passwordStrenght').html('')
        }
      });

      (function($) {
        $.fn.inputFilter = function(inputFilter) {
          return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
              this.oldValue = this.value;
              this.oldSelectionStart = this.selectionStart;
              this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
              this.value = this.oldValue;
              this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            } else {
              this.value = "";
            }
          });
        };
      }(jQuery));

      $("#no_telp").inputFilter(function(value) {
      return /^-?\d*$/.test(value); 
      });

      $("#nik").inputFilter(function(value) {
      return /^-?\d*$/.test(value); 
      });
      
      $("#no_telp_perusahaan").inputFilter(function(value) {
      return /^-?\d*$/.test(value); 
      });
      
    });
    </script>
@endsection