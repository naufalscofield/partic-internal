@extends('Home/master')

@section('content')
    <div class="site-blocks-cover overlay" style="background-image: url(/logistics/images/hero_bg_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
            

            <h1 class="text-white font-weight-light mb-5 text-uppercase font-weight-bold">Platform Partner Logistik Anda</h1>
          <p><a href="{{ route('daftar') }}" class="btn btn-primary py-3 px-5 text-white">Daftarkan Perusahaan logistik saya</a></p>

          </div>
        </div>
      </div>
    </div>  

    <div class="container">
      <div class="row align-items-center no-gutters align-items-stretch overlap-section">
        <div class="col-md-4">
          <div class="feature-1 pricing h-100 text-center">
            <div class="icon">
              <span class="icon-dollar"></span>
            </div>
            <h2 class="my-4 heading">Tarif Kurir Terbaik</h2>
            <p>Sebagai perusahaan logistik, anda dapat memilih tarif pengiriman yang terbaik dan ideal untuk perusahaan anda!.</p>
            <p>Sebagai kurir, anda dapat bermitra dengan perusahaan logistik yang bekerja sama dengan Partic, melalui pengajuan tarif terbaik anda!.</p>
          </div>
        </div>
        <div class="col-md-4">
          <div class="free-quote bg-dark h-100">
            <h2 class="my-4 heading  text-center">Kami akan senang jika anda punya pertanyaan</h2>
            <form method="post">
              <div class="form-group">
                <label for="fq_name">Nama</label>
                <input type="text" class="form-control btn-block" id="fq_name" name="nama" placeholder="Nama Anda">
              </div>
              <div class="form-group mb-4">
                <label for="fq_email">Email</label>
                <input type="text" class="form-control btn-block" id="fq_email" name="email" placeholder="Email Anda">
              </div>
              <div class="form-group mb-4">
                <label for="fq_pertanyaan">Apa Yang Ingin Anda Tanyakan?</label>
                <textarea class="form-control btn-block" id="fq_pertanyaan" name="pertanyaan" placeholder="Pertanyaan Anda"></textarea>
              </div>
              <div class="form-group">
                <input type="submit" class="btn btn-primary text-white py-2 px-4 btn-block" value="Kirim">  
              </div>
            </form>
          </div>
        </div>
        <div class="col-md-4">
         <div class="feature-1 pricing h-100 text-center">
           <div class="icon">
             <span><i class="fa fa-check" aria-hidden="true"></i></span>
           </div>
           <h2 class="my-4 heading">Metode Pemilihan Kurir Paling Ideal</h2>
           <p>Partic memiliki metode untuk menentukan mana mitra kurir eksternal yang paling ideal untuk setiap pengiriman anda, tidak perlu bingung memilih lagi!.</p>
         </div>
       </div>
      </div>
    </div>

    <section id="pelayanan">
      <div class="site-section bg-light">
        <div class="container">
          <div class="row justify-content-center mb-5">
            <div class="col-md-7 text-center border-primary">
              <h2 class="font-weight-light text-primary">Apa yang kami tawarkan untuk perusahaan logistik</h2>
              <p class="color-black-opacity-5">Sebagai Platform <i>Third Party Logistic (3PL)</i>, Partic menawarkan pelayanan berikut</p>
            </div>
          </div>
          <div class="row align-items-stretch">
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4"><span><i class="fa fa-desktop text-primary" aria-hidden="true"></i></span></div>
                <div>
                  <h3>Sistem Platform <i>Third Party Logistic (3PL)</i></h3>
                  <p>Sebagai perusahaan logistik, anda dapat menggunakan aplikasi Partic sebagai sisem pengelola logistik anda jika anda tidak memiliki sistem logistik sendiri.</p>
                  <p><a href="/#">Pelajari Lebih Lanjut</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4"><span><i class="fa fa-share-alt text-primary" aria-hidden="true"></i></span></div>
                <div>
                  <h3>Berbagi Mitra Kurir</h3>
                  <p>Sebagai perusahaan logistik, anda dapat bekerja sama dan menggunakan mitra kurir manapun yang terdaftar di Partic.</p>
                  <p><a href="/#">Pelajari Lebih Lanjut</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4"><span><i class="text-primary fa fa-building text-primary" aria-hidden="true"></i></span></div>
                <div>
                  <h3>Buat & Kelola Kantor Regional - Cabang Anda</h3>
                  <p>Sebagai perusahaan logistik, anda dapat membuat dan mengelola kantor regional dan cabang perusahaan anda di berbagai kota untuk meng<i>handle</i> input pengiriman.</p>
                  <p><a href="/#">Pelajari Lebih Lanjut</a></p>
                </div>
              </div>
            </div>


            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4"><span><i class="fa fa-magic text-primary" aria-hidden="true"></i></span></div>
                <div>
                  <h3>Metode Pemilihan Kurir</h3>
                  <p>Partic akan membantu anda memilih kurir eksternal paling ideal untuk setiap pengiriman anda menggunakan metode terbaik.</p>
                  <p><a href="/#">Pelajari Lebih Lanjut</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4"><span><i class="text-primary fa fa-search" aria-hidden="true"></i></span></div>
                <div>
                  <h3><i>Tracking</i> Pengiriman</h3>
                  <p>Partic menyediakan fitur tracking pengiriman, untuk perusahaan dan untuk pelanggan perusahaan.</p>
                  <p><a href="/#">Pelajari Lebih Lanjut</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4 mb-4 mb-lg-4">
              <div class="unit-4 d-flex">
                <div class="unit-4-icon mr-4"><span><i class="text-primary fa fa-users" aria-hidden="true"></i></span></div>
                <div>
                  <h3>Pendaftaran Member</h3>
                  <p>Partic menyediakan fitur <i>Member</i> untuk perusahaan logistik yang ingin membuka sistem ke anggotaan untuk pelanggan nya.</p>
                  <p><a href="/#">Pelajari Lebih Lanjut</a></p>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </section>


    <div class="site-section block-13">
      <div class="owl-carousel nonloop-block-13">
        <div>
          <a href="/#" class="unit-1 text-center">
            <img src="/logistics/images/img_7.jpg" alt="Image" class="img-fluid">
            <div class="unit-1-text">
               <h3 class="unit-1-heading">Mitra Kurir Eksternal</h3>
              <p class="px-5">Berbagi dan bekerja sama dengan banyak kurir eksternal diluar.</p>
            </div>
          </a>
        </div>

        <div>
          <a href="/#" class="unit-1 text-center">
            <img src="/logistics/images/img_4.jpg" alt="Image" class="img-fluid">
            <div class="unit-1-text">
               <h3 class="unit-1-heading">Pengelolaan Pengiriman / <i>Supply Chain</i></h3>
              <p class="px-5">Kelola proses bisnis pengiriman paket untuk perusahaan anda.</p>
            </div>
          </a>
        </div>

        <div>
          <a href="/#" class="unit-1 text-center">
            <img src="/logistics/images/img_1.jpg" alt="Image" class="img-fluid img-locked">
            <div class="unit-1-text">
               <h3 class="unit-1-heading">Pergudangan</h3>
               <i class="fa fa-lock" aria-hidden="true"></i>
              <p class="px-5">FITUR BELUM TERSEDIA.</p>
            </div>
          </a>
        </div>
        
        <div>
          <a href="/#" class="unit-1 text-center">
            <img src="/logistics/images/img_6.jpg" alt="Image" class="img-fluid img-locked">
            <div class="unit-1-text">
               <h3 class="unit-1-heading">Akunting</h3>
               <i class="fa fa-lock" aria-hidden="true"></i>
              <p class="px-5">FITUR BELUM TERSEDIA.</p>
            </div>
          </a>
        </div>

      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center border-primary">
            <h2 class="mb-0 text-primary">Apa yang partic tawarkan Untuk Mitra Kurir</h2>
            <p class="color-black-opacity-6">Partic membuka peluang semua kurir individual maupun organisasi untuk berkerja sama.</p>
          </div>
        </div>
        <div class="row align-items-stretch">
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-travel"></span></div>
              <div>
                <h3>Pengiriman Jalur Udara</h3>
                <p>Sebagai mitra kurir, anda dapat mengajukan tarif pengiriman jalur udara pada setiap perusahaan logistik yang teraftar di Partic.</p>
                <p class="mb-0"><a href="/#">Daftar Sebagai Mitra Kurir Eksternal</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-sea-ship-with-containers"></span></div>
              <div>
               <h3>Pengiriman Jalur Laut</h3>
               <p>Sebagai mitra kurir, anda dapat mengajukan tarif pengiriman jalur laut pada setiap perusahaan logistik yang teraftar di Partic.</p>
               <p class="mb-0"><a href="/#">Daftar Sebagai Mitra Kurir Eksternal</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-0">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary flaticon-frontal-truck"></span></div>
              <div>
               <h3>Pengiriman Jalur Darat</h3>
               <p>Sebagai mitra kurir, anda dapat mengajukan tarif pengiriman jalur darat pada setiap perusahaan logistik yang teraftar di Partic.</p>
               <p class="mb-0"><a href="/#">Daftar Sebagai Mitra Kurir Eksternal</a></p>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="site-section-grey border-top">
      <div class="container">
        <div class="row text-center">
          <div class="col-md-12">
            <h2 class="mb-5 text-black">Ingin Menjadi Customer Member Dari Perusahaan Logistik Dibawah?</h2>
            <div class="row justify-content-center d-flex align-items-center">
              <div class="col-lg-3 col-md-6 col-sm-12 single-trainer">
                <div class="thumb d-flex justify-content-sm-center">
                  <img class="img-fluid img-locked" src="/logistics/images/log1.png" alt="" />
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-12 single-trainer">
                <div class="thumb d-flex justify-content-sm-center">
                  <img class="img-fluid img-locked" src="/logistics/images/log2.png" alt=""/>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-12 single-trainer">
                <div class="thumb d-flex justify-content-sm-center">
                  <img class="img-fluid img-locked" src="/logistics/images/log3.png" alt="" />
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-12 single-trainer">
                <div class="thumb d-flex justify-content-sm-center">
                  <img class="img-fluid img-locked" src="/logistics/images/log4.png" alt="" />
                </div>
              </div>
    
            </div>
            <br>
            <p class="mb-0"><a href="/booking.html" class="btn btn-primary py-3 px-5 text-white">Daftar sebagai member</a></p>
          </div>
        </div>
      </div>
    </div>
    @endsection
    {{-- <div class="site-blocks-cover overlay inner-page-cover" style="background-image: url(/logistics/images/hero_bg_2.jpg); background-attachment: fixed;">
      <div class="container">
        <div class="row align-items-center justify-content-center text-center">

          <div class="col-md-7" data-aos="fade-up" data-aos-delay="400">
            <a href="/https://vimeo.com/channels/staffpicks/93951774" class="play-single-big mb-4 d-inline-block popup-vimeo"><span class="icon-play"></span></a>
            <h2 class="text-white font-weight-light mb-5 h1">View Our Services By Watching This Short Video</h2>
            
          </div>
        </div>
      </div>
    </div>  
    
    <div class="site-section border-bottom">
      <div class="container">

        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center border-primary">
            <h2 class="font-weight-light text-primary">Testimonials</h2>
          </div>
        </div>

        <div class="slide-one-item home-slider owl-carousel">
          <div>
            <div class="testimonial">
              <figure class="mb-4">
                <img src="/logistics/images/person_3.jpg" alt="Image" class="img-fluid mb-3">
                <p>John Smith</p>
              </figure>
              <blockquote>
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>
            </div>
          </div>
          <div>
            <div class="testimonial">
              <figure class="mb-4">
                <img src="/logistics/images/person_2.jpg" alt="Image" class="img-fluid mb-3">
                <p>Christine Aguilar</p>
              </figure>
              <blockquote>
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>
            </div>
          </div>

          <div>
            <div class="testimonial">
              <figure class="mb-4">
                <img src="/logistics/images/person_4.jpg" alt="Image" class="img-fluid mb-3">
                <p>Robert Spears</p>
              </figure>
              <blockquote>
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>
            </div>
          </div>

          <div>
            <div class="testimonial">
              <figure class="mb-4">
                <img src="/logistics/images/person_5.jpg" alt="Image" class="img-fluid mb-3">
                <p>Bruce Rogers</p>
              </figure>
              <blockquote>
                <p>&ldquo;Lorem ipsum dolor sit amet consectetur adipisicing elit. Consectetur unde reprehenderit aperiam quaerat fugiat repudiandae explicabo animi minima fuga beatae illum eligendi incidunt consequatur. Amet dolores excepturi earum unde iusto.&rdquo;</p>
              </blockquote>
            </div>
          </div>

        </div>
      </div>
    </div> --}}



    {{-- <div class="site-section">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center border-primary">
            <h2 class="font-weight-light text-primary">Our Blog</h2>
            <p class="color-black-opacity-5">See Our Daily News &amp; Updates</p>
          </div>
        </div>
        <div class="row mb-3 align-items-stretch">
          <div class="col-md-6 col-lg-6 mb-4 mb-lg-4">
            <div class="h-entry">
              <img src="/logistics/images/blog_1.jpg" alt="Image" class="img-fluid">
              <h2 class="font-size-regular"><a href="/#">Warehousing Your Packages</a></h2>
              <div class="meta mb-4">by Theresa Winston <span class="mx-2">&bullet;</span> Jan 18, 2019 at 2:00 pm <span class="mx-2">&bullet;</span> <a href="/#">News</a></div>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus eligendi nobis ea maiores sapiente veritatis reprehenderit suscipit quaerat rerum voluptatibus a eius.</p>
            </div> 
          </div>
          <div class="col-md-6 col-lg-6 mb-4 mb-lg-4">
            <div class="h-entry">
              <img src="/logistics/images/blog_2.jpg" alt="Image" class="img-fluid">
              <h2 class="font-size-regular"><a href="/#">Warehousing Your Packages</a></h2>
              <div class="meta mb-4">by Theresa Winston <span class="mx-2">&bullet;</span> Jan 18, 2019 at 2:00 pm <span class="mx-2">&bullet;</span> <a href="/#">News</a></div>
              <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus eligendi nobis ea maiores sapiente veritatis reprehenderit suscipit quaerat rerum voluptatibus a eius.</p>
            </div>
          </div>
        </div>
      </div>
    </div> --}}
    
    