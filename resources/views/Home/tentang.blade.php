@extends('Home/master')

@section('content')
<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url(/logistics/images/hero_bg_1.jpg);" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
      <div class="row align-items-center justify-content-center text-center">

        <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
          <h1 class="text-white font-weight-light text-uppercase font-weight-bold">Tentang Partic</h1>
        </div>
      </div>
    </div>
  </div>  

  <div class="site-section">
    <div class="container">
      <div class="row mb-5">
        
        <div class="col-md-5 ml-auto mb-5 order-md-2" data-aos="fade">
          <img src="/img/partic-logo-trans.png" alt="Image" class="img-fluid rounded">
        </div>
        <div class="col-md-6 order-md-1" data-aos="fade">
          <div class="text-left pb-1 border-primary mb-4">
            <h2 class="text-primary">Apa itu Partic?</h2>
          </div>
          <p>Partic hadir sebagai <i>3PL (Third Party Logistic)</i> atau platform logistik yang bergerak khusus di sektor manajemen pengiriman barang. Keistimewaan Partic terletak pada fitur yang dapat memungkinkan <b>perusahaan logistik untuk bekerja sama dengan banyak mitra kurir eksternal untuk menjangkau rute yang tidak dimiliki perusahaan</b>, sehingga tentu menghadirkan keuntungan lebih bagi perusahaan logistik.</p>
          {{-- <p class="mb-5">Error minus sint nobis dolor laborum architecto, quaerat. Voluptatum porro expedita labore esse velit veniam laborum quo obcaecati similique iusto delectus quasi!</p> --}}

          {{-- <div class="row">
            <div class="col-md-12 mb-md-5 mb-0 col-lg-6">
              <div class="unit-4">
                <div class="unit-4-icon mb-3 mr-4"><span class="text-primary flaticon-frontal-truck"></span></div>
                <div>
                  <h3>Ground Shipping</h3>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis.</p>
                  <p class="mb-0"><a href="#">Learn More</a></p>
                </div>
              </div>
            </div>
            <div class="col-md-12 mb-md-5 mb-0 col-lg-6">
              <div class="unit-4">
                <div class="unit-4-icon mb-3 mr-4"><span class="text-primary flaticon-travel"></span></div>
                <div>
                  <h3>Air Freight</h3>
                  <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis quis.</p>
                  <p class="mb-0"><a href="#">Learn More</a></p>
                </div>
              </div>
            </div>
          </div>
        </div> --}}
        
      </div>
    </div>
  </div>

  <div class="site-section bg-image overlay" style="background-image: url('/logistics/images/hero_bg_4.jpg');">
    <div class="container">
      <div class="row justify-content-center mb-5">
        <div class="col-md-7 text-center border-primary">
          <h2 class="font-weight-light text-primary" data-aos="fade">Cara Kerja</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
          <div class="how-it-work-item">
            <span class="number">1</span>
            <div class="how-it-work-body">
              <h2>Daftarkan Perusahaan Logistik Anda</h2>
              <p class="mb-5">Ceo perusahaan atau pihak perusahaan pertama tama mendaftarkan perusahaan nya di website partic untuk bisa menggunakan aplikasi Partic sebagai 3PL perusahaan nya.</p>
              <ul class="ul-check list-unstyled white">
                {{-- <li class="text-white">Error minus sint nobis dolor</li> --}}
                {{-- <li class="text-white">Voluptatum porro expedita labore esse</li> --}}
                {{-- <li class="text-white">Voluptas unde sit pariatur earum</li> --}}
              </ul>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
          <div class="how-it-work-item">
            <span class="number">2</span>
            <div class="how-it-work-body">
              <h2>Masuk dan Kelola Regional, Cabang Perusahaan</h2>
              <p class="mb-5">Masuk dan mulai kelola regional dan cabang perusahaan anda untuk nantinya mengelola data kiriman.</p>
              <ul class="ul-check list-unstyled white">
                <li class="text-white">Atur tarif</li>
                <li class="text-white">Membuka kesempatan untuk menjadi member bagi pelanggan</li>
                {{-- <li class="text-white">Voluptas unde sit pariatur earum</li> --}}
              </ul>
            </div>
          </div>
        </div>

        <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
          <div class="how-it-work-item">
            <span class="number">3</span>
            <div class="how-it-work-body">
              <h2>Pilih Mitra Kurir dan Kelola Pengiriman</h2>
              <p class="mb-5">Kelola dengan mudah pengiriman perusahaan dengan pemilihan mitra kurir yang optimal dan ideal untuk setiap pengiriman.</p>
              <ul class="ul-check list-unstyled white">
                <li class="text-white">Metode pemilihan mitra kurir</li>
                <li class="text-white">Tracking status pengiriman</li>
                {{-- <li class="text-white">Voluptas unde sit pariatur earum</li> --}}
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="site-section border-bottom">
    <div class="container">
      <div class="row justify-content-center mb-5">
        <div class="col-md-7 text-center border-primary">
          <h2 class="font-weight-light text-primary" data-aos="fade">Tim Kami</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
          <div class="person">
            <img src="/img/naufal.jpeg" alt="Image" class="img-fluid rounded mb-5">
            <h3>Naufal Ramadhan</h3>
            <p class="position text-muted">Internal Module Partic Developer</p>
            <p class="mb-4">"Saya harap Partic dapat memberikan manfaat yang besar bagi dunia perlogistik an Indonesia".</p>
            <ul class="ul-social-circle">
              <li><a href="https://www.facebook.com/naufalible"><span class="icon-facebook"></span></a></li>
              <li><a href="https://www.twitter.com/@dimsumpanas"><span class="icon-twitter"></span></a></li>
              <li><a href="https://www.linkedin.com/in/naufal-ramadhan-144b82192/"><span class="icon-linkedin"></span></a></li>
              <li><a href="https://www.instagram.com/naufalramadhan"><span class="icon-instagram"></span></a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
          <div class="person">
            <img src="/img/harry.jpg" alt="Image" class="img-fluid rounded mb-5">
            <h3>M. Harry K Saputra</h3>
            <p class="position text-muted">Pembimbing</p>
            <p class="mb-4">"Saya harap Partic dapat memberikan manfaat yang besar bagi dunia perlogistik an Indonesia".</p>
            <ul class="ul-social-circle">
              <li><a href="#"><span class="icon-facebook"></span></a></li>
              <li><a href="#"><span class="icon-twitter"></span></a></li>
              <li><a href="#"><span class="icon-linkedin"></span></a></li>
              <li><a href="#"><span class="icon-instagram"></span></a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="300">
          <div class="person">
            <img src="/img/farhan.jpeg" alt="Image" class="img-fluid rounded mb-5">
            <h3>Farhan Maulana</h3>
            <p class="position text-muted">External Module Partic Developer</p>
            <p class="mb-4">"Saya harap Partic dapat memberikan manfaat yang besar bagi dunia perlogistik an Indonesia".</p>
            <ul class="ul-social-circle">
              <li><a href="#"><span class="icon-facebook"></span></a></li>
              <li><a href="#"><span class="icon-twitter"></span></a></li>
              <li><a href="#"><span class="icon-linkedin"></span></a></li>
              <li><a href="#"><span class="icon-instagram"></span></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  

      </div>
    </div>
  </div>    
@endsection