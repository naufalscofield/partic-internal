<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Partic &mdash; Partner Logistik Anda</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <script
    src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous">
    </script>

  	<link rel="icon" type="image/png" href="/img/partic-logo.ico"/>


    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"> 
    <link rel="stylesheet" href="/logistics/fonts/icomoon/style.css">

    <link rel="stylesheet" href="/logistics/css/bootstrap.min.css">
    <link rel="stylesheet" href="/logistics/css/magnific-popup.css">
    <link rel="stylesheet" href="/logistics/css/jquery-ui.css">
    <link rel="stylesheet" href="/logistics/css/owl.carousel.min.css">
    <link rel="stylesheet" href="/logistics/css/owl.theme.default.min.css">

    <link rel="stylesheet" href="/logistics/css/bootstrap-datepicker.css">

    <link rel="stylesheet" href="/logistics/fonts/flaticon/font/flaticon.css">

    <link rel="stylesheet" href="/logistics/css/aos.css">

    <link rel="stylesheet" href="/logistics/css/style.css">

    <link rel="stylesheet" href="/font-awesome-4.7.0/css/font-awesome.min.css">
    
  </head>
  <style>
     .img-locked {
    -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
    filter: grayscale(100%);
   }
  </style>
  <body>
  
  <div class="site-wrap">

    <div class="site-mobile-menu">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
    
    <header class="site-navbar py-3" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-11 col-xl-2">
            <h1 class="mb-0"><a href="/" class="text-white h2 mb-0"><img src= "/img/partic-logo-trans.png" style="height:100px;width:90px">Partic</a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                <?php
                if (Request::segment(1) == NULL) { ?>
                  <li class="active"><a href="/">Beranda</a></li>
                <?php } else { ?>
                  <li class=""><a href="/">Beranda</a></li>
                <?php } ?>
                
                <?php
                if (Request::segment(1) == 'tentang') { ?>
                  <li class="active"><a href="{{ route('tentang') }}">Tentang Partic</a></li>
                <?php } else { ?>
                  <li><a href="{{ route('tentang') }}">Tentang Partic</a></li>
                <?php } ?>
                
                <?php
                if (Request::segment(1) == '#pelayanan') { ?>
                   <li class="active"><a href="#pelayanan">Pelayanan</a></li>
                <?php } else { ?>
                  <li><a href="#pelayanan">Pelayanan</a></li>
                <?php } ?>

                <?php
                if (Request::segment(1) == 'kontak') { ?>
                  <li class="active"><a href="{{ route('kontak') }}">Kontak</a></li>
                  <?php } else { ?>
                  <li><a href="{{ route('kontak') }}">Kontak</a></li>
                <?php } ?>
                
                <?php
                if (Request::segment(1) == 'tracking') { ?>
                  <li class="active"><a href="{{ route('tracking') }}">Tracking Pengiriman</a></li>
                <?php } else { ?>
                  <li><a href="{{ route('tracking') }}">Tracking Pengiriman</a></li>
                <?php } ?>
                
                <li><a href="{{ route('login') }}" class="btn btn-primary py-1 px-2 text-white">Masuk</a></li>
              </ul>
            </nav>
          </div>


          <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="/#" class="site-menu-toggle js-menu-toggle text-white"><span class="icon-menu h3"></span></a></div>

          </div>

        </div>
      </div>
      
    </header>

    @yield('content')

    <footer class="site-footer">
        <div class="container">
          <div class="row">
            <div class="col-md-9">
              <div class="row">
                <div class="col-md-3">
                  <h2 class="footer-heading mb-4">Fitur</h2>
                  <ul class="list-unstyled">
                  <li><a href="{{ route('tentang') }}">Tentang Partic</a></li>
                    <li>
                      @if (Request::segment(1) == '#pelayanan' || Request::segment(1) == NULL)
                      <a href="#pelayanan">Pelayanan</a>
                      @else
                      <a href="/#pelayanan">Pelayanan</a>
                      @endif
                    </li>
                    <li><a href="{{ route('kontak') }}">Kontak</a></li>
                    <li><a href="{{ route('tracking') }}">Tracking Pengiriman</a></li>
                  </ul>
                </div>
                <div class="col-md-3">
                  <h2 class="footer-heading mb-4">Ikuti Kami</h2>
                  <a href="/#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                  <a href="/#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                  <a href="/#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                  <a href="/#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                </div>
              </div>
            </div>
            <div class="col-md-3">
              <h2 class="footer-heading mb-4">Kirimi Saya Berita Terbaru</h2>
              <form action="#" method="post">
                <div class="input-group mb-3">
                  <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-primary text-white" type="button" id="button-addon2">Kirim</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
          <div class="row pt-5 mt-5 text-center">
            <div class="col-md-12">
              <div class="border-top pt-5">
              <p>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Application by <img src="/img/partic-logo-trans.png" style="height:25px;width:20px"> PARTIC | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="/https://colorlib.com" target="_blank" >Colorlib</a>
              <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
              </p>
              </div>
            </div>
            
          </div>
        </div>
      </footer>
    </div>
  
    <script src="/logistics/js/jquery-3.3.1.min.js"></script>
    <script src="/logistics/js/jquery-migrate-3.0.1.min.js"></script>
    <script src="/logistics/js/jquery-ui.js"></script>
    <script src="/logistics/js/popper.min.js"></script>
    <script src="/logistics/js/bootstrap.min.js"></script>
    <script src="/logistics/js/owl.carousel.min.js"></script>
    <script src="/logistics/js/jquery.stellar.min.js"></script>
    <script src="/logistics/js/jquery.countdown.min.js"></script>
    <script src="/logistics/js/jquery.magnific-popup.min.js"></script>
    <script src="/logistics/js/bootstrap-datepicker.min.js"></script>
    <script src="/logistics/js/aos.js"></script>
  
    <script src="/logistics/js/main.js"></script>
      
    </body>
  </html>