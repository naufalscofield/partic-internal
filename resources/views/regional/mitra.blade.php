@extends('component.master_'.Session::get('role'))

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Mitra Kurir</h1>
        <i><p>*Mitra kurir yang terdaftar regional lewat pengajuan tarif line yang sudah di acc dan memiliki line kota asal yang sama dengan kantor regional</p></i>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered datatable mdl-data-table" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>Email</center></th>
                    <th><center>Nama Lengkap</center></th>
                    <th><center>Kendaraan</center></th>
                    <th><center>Plat</center></th>
                    <th><center>Status</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="modal" tabindex="-1" id="modalEdit" role="dialog">
          <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">PIC Cabang</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="" method="POST" enctype="multipart/form-data">
                  @csrf
                  <b><label for="">Email</label></b>
                  <p id="email_edit"></p><br>
                  
                  <b><label for="">Nama Lengkap</label></b>
                  <p id="nama_lengkap_edit"></p><br>
                  
                  <b><label for="">Tempat Tanggal Lahir</label></b>
                  <p id="ttl_edit"></p><br>
                  
                  <b><label for="">Alamat</label></b>
                  <p id="alamat_edit"></p><br>
                  
                  <b><label for="">No Telp</label></b>
                  <p id="no_telp_edit"></p><br>
                  
                  <b><label for="">NIK</label></b>
                  <p id="nik_edit"></p><br>
                  
                  <b><label for="">Kendaraan</label></b>
                  <p id="kendaraan_edit"></p><br>
                  
                  <b><label for="">Plat Nomor</label></b>
                  <p id="plat_edit"></p><br>
                  
                  <b><label for="">Tahun Kendaraan</label></b>
                  <p id="tahun_kendaraan_edit"></p><br>
                  
                  <b><label for="">Domisili</label></b>
                  <p id="domisili_edit"></p><br>
                  
                  <b><label for="">Status Ketersediaan</label></b>
                  <p id="status_edit"></p><br>
                  
                  <center>
                    <table border="1" >
                      <tr>
                        <th><center>Foto Diri</center></th>
                        <th><center>Foto NPWP</center></th>
                        <th><center>Foto KTP</center></th>
                        <th><center>Foto STNK</center></th>
                        <th><center>Foto SIM</center></th>
                      </tr>
                      <tr>
                        <td width="200px"><center><img src="" style="width:70px;height:70px" id="foto_diri_edit" alt=""></center></td>
                        <td width="200px"><center><img src="" style="width:70px;height:70px" id="foto_npwp_edit" alt=""></center></td>
                        <td width="200px"><center><img src="" style="width:70px;height:70px" id="foto_ktp_edit" alt=""></center></td>
                        <td width="200px"><center><img src="" style="width:70px;height:70px" id="foto_stnk_edit" alt=""></center></td>
                        <td width="200px"><center><img src="" style="width:70px;height:70px" id="foto_sim_edit" alt=""></center></td>
                      </tr>
                    </table>
                  </center>
                  
                  
                </div>
                {{-- <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div> --}}
              </form>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){

          $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('regional.get_mitra_kurir') }}',
            "columns": [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'email', name: 'email'},
              {data: 'nama_lengkap', name: 'nama_lengkap'},
              {data: 'kendaraan', name: 'kendaraan'},
              {data: 'plat', name: 'plat'},
              {data: 'status', name: 'status'},
              {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
          ],
          'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ]
        });

        $(document).on('click', '#btn_edit', function() {

          var id  = $(this).data("id");
          var url = '{{ route('regional.show_mitra_kurir', ":id") }}'
          url     = url.replace(':id', id)

          $.get(url , function(data, status){
              let element = JSON.parse(data)
              $('#modalEdit').modal('show')

              $('#email_edit').text(element['user'].email)
              $('#nama_lengkap_edit').text(element['biodata'].nama_lengkap)
              $('#ttl_edit').text(element['biodata'].tempat_lahir+', '+element['biodata'].tanggal_lahir)
              $('#alamat_edit').text(element['biodata'].alamat)
              $('#no_telp_edit').text(element['biodata'].no_telp)
              $('#nik_edit').text(element['biodata'].nik)
              $('#kendaraan_edit').text(element['mitra'].jenis_kendaraan+'-'+element['mitra'].nama_kendaraan)
              $('#plat_edit').text(element['mitra'].plat_kendaraan)
              $('#tahun_kendaraan_edit').text(element['mitra'].tahun_kendaraan)
              $('#domisili_edit').text(element['mitra'].domisili)
              $('#status_edit').text(element['mitra'].status_ketersediaan)
              $('#foto_diri_edit').attr('src', `{{config('app.storage')}}foto_diri/${element['biodata'].foto_diri}`)
              $('#foto_npwp_edit').attr('src', `{{config('app.storage')}}foto_npwp/${element['biodata'].foto_npwp}`)
              $('#foto_ktp_edit').attr('src', `{{config('app.storage')}}foto_ktp/${element['biodata'].foto_ktp}`)
              $('#foto_stnk_edit').attr('src', `{{config('app.storage')}}foto_stnk/${element['mitra'].foto_stnk}`)
              $('#foto_sim_edit').attr('src', `{{config('app.storage')}}foto_sim/${element['mitra'].foto_sim}`)
          })

        })


        })
      </script>
@endsection