@extends('component.master_'.Session::get('role'))

@section('content')
<style>
  div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Tarif Line</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered datatable mdl-data-table display nowrap" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>Kota Asal</center></th>
                    <th><center>Kota Tujuan</center></th>
                    <th><center>Tarif</center></th>
                    <th><center>Jenis Pengiriman</center></th>
                    <th><center>Jenis Pengangkutan</center></th>
                    <th><center>Barang Yang Diangkut (Jika jenis nya Koli)</center></th>
                    <th><center>Minimal Angkut</center></th>
                    <th><center>Maksimal Angkut</center></th>
                    <th><center>Durasi</center></th>
                    <th><center>Moda</center></th>
                    <th><center>ID Mitra / Nama Mitra</center></th>
                    <th><center>No Telp</center></th>
                    {{-- <th><center>Aksi</center></th> --}}
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){
          // $('.datatable').DataTable( {
          //     "scrollX": true
          // } );

          $('#provinsi_asal').select2();
          $('#kota_asal').select2();
          $('#provinsi_tujuan').select2();
          $('#kota_tujuan').select2();
          $('#provinsi_asal_edit').select2();
          $('#kota_asal_edit').select2();
          $('#provinsi_tujuan_edit').select2();
          $('#kota_tujuan_edit').select2();

          $('.datatable').DataTable({
            "scrollX": true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('regional.get_tarif_line') }}',
            "columns": [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'asal', name: 'asal'},
              {data: 'tujuan', name: 'tujuan'},
              {data: 'tarif', name: 'tarif'},
              {data: 'jenis_pengiriman', name: 'jenis_pengiriman'},
              {data: 'jenis_pengangkutan', name: 'jenis_pengangkutan'},
              {data: 'nama_barang', name: 'nama_barang'},
              {data: 'minimal_angkut', name: 'minimal_angkut'},
              {data: 'maksimal_angkut', name: 'maksimal_angkut'},
              {data: 'durasi', name: 'durasi'},
              {data: 'moda', name: 'moda'},
              {data: 'mitra', name: 'mitra'},
              {data: 'no_telp', name: 'no_telp'},
              // {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
          ],
          'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ]
        });

        $.get("https://x.rajaapi.com/poe", function(data, status){
          const tokenraja = data.token
          $('#token_raja').val(tokenraja)

          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/provinsi", function(dataProv, statusProv){
              $.each(dataProv.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi_asal');
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi_tujuan');
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi_asal_edit');
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi_tujuan_edit');
            });
          });

        });

        $('#provinsi_asal').change(function(){
          $('#kota_asal').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_asal');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_asal');
            });
          });
        })

        $('#provinsi_tujuan').change(function(){
          $('#kota_tujuan').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_tujuan');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_tujuan');
            });
          });
        })

        $('#provinsi_asal_edit').change(function(){
          $('#kota_asal_edit').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_asal_edit');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_asal_edit');
            });
          });
        })
        
        $('#kota_asal_edit').change(function(){
          $('#kota_asal_edit_input').empty()
          var id = $(this).find(':selected').attr('data-id')
          var val = $(this).val()
          $('#kota_asal_edit_input').val(val)
          $('#kota_asal_edit_label').text(val)
        })

        $('#kota_tujuan_edit').change(function(){
          $('#kota_tujuan_edit_input').empty()
          var id = $(this).find(':selected').attr('data-id')
          var val = $(this).val()
          $('#kota_tujuan_edit_input').val(val)
          $('#kota_tujuan_edit_label').text(val)
        })

        $('#provinsi_tujuan_edit').change(function(){
          $('#kota_tujuan_edit').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_tujuan_edit');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_tujuan_edit');
            });
          });
        })

        $(document).on('click', '#btn_edit', function() {
          

          var id  = $(this).data("id");
          var url = '{{ route('regional.show_tarif_pelanggan', ":id") }}'
          url     = url.replace(':id', id)

          $.get(url , function(data, status){
            let element = JSON.parse(data)
            console.log(element)
            $('#modalEdit').modal('show')
            $('#id_tarif').val(element.id)
            // $('#provinsi_asal_edit').val(element.provinsi)
            $('#kota_asal_edit_label').text(element.asal)
            $('#kota_tujuan_edit_label').text(element.tujuan)
            $('#kota_asal_edit_input').val(element.asal)
            $('#kota_tujuan_edit_input').val(element.tujuan)
            $('#tarif_edit').val(element.tarif)
          })

          var tokenraja = $('#token_raja').val()

          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/provinsi", function(dataProv, statusProv){
              $.each(dataProv.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi_edit');
            });
          });
        })

        $('#provinsi_edit').change(function(){
          $('#kota_edit').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_edit');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_edit');
            });
          });
        })
          // var tokenraja = $('#token_raja').val()

       

          // function readURLEdit(input) {
          //   if (input.files && input.files[0]) {
          //     var reader = new FileReader();
              
          //     reader.onload = function(e) {
          //       $('#gambar_edit_preview').attr('src', e.target.result);
          //     }
              
          //     reader.readAsDataURL(input.files[0]); // convert to base64 string
          //   }
          // }

          // $("#gambar_edit").change(function() {
          //   readURLEdit(this);
          // });

          // function readURL(input) {
          //   if (input.files && input.files[0]) {
          //     var reader = new FileReader();
              
          //     reader.onload = function(e) {
          //       $('#gambar_preview').attr('src', e.target.result);
          //     }
              
          //     reader.readAsDataURL(input.files[0]); // convert to base64 string
          //   }
          // }

          // $("#gambar").change(function() {
          //   readURL(this);
          // });

        })
      </script>
@endsection