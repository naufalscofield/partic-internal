@extends('component.master_'.Session::get('role'))

@section('content')
    
    <div class="container-fluid">
        
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Profil</h1>
        <div class="row">
            <div class="col-xl-6">
                <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Profil</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <form method="POST" action="{{route('update_profile')}}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" name="email" class="form-control" id="" placeholder="" value="{{Session::get('email')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Nama Lengkap</label>
                            <input type="text" name="nama_lengkap" class="form-control" id="" placeholder="" value="{{Session::get('nama_lengkap')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Tanggal Lahir</label>
                            <input type="date" name="tanggal_lahir" class="form-control" id="" placeholder="" value="{{Session::get('tanggal_lahir')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Tempat Lahir</label>
                            <input type="text" name="tempat_lahir" class="form-control" id="" placeholder="" value="{{Session::get('tempat_lahir')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Alamat</label>
                            <textarea type="text" name="alamat" class="form-control" id="" placeholder="">{{Session::get('alamat')}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="">No Telp</label></label>
                            <input type="number" name="no_telp" class="form-control" id="no_telp" placeholder="" value="{{Session::get('no_telp')}}">
                        </div>
                        <div class="form-group">
                            <label for="">NIK</label>
                            <input type="number" name="nik" class="form-control" id="nik" placeholder="" value="{{Session::get('nik')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Foto Diri</label><br>
                            <center>
                                <img style="width:70px;height:70px" src="{{config('app.storage')}}foto_diri/{{Session::get('foto_diri')}}" alt="">
                                <i id="arrow-diri" class="fa fa-angle-double-right"></i>
                                <img style="width:70px;height:70px" src="" id="prev_foto_diri" alt="">
                            </center>
                            <input type="file" name="foto_diri" class="form-control" id="foto_diri" placeholder="" value="{{Session::get('foto_diri')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Foto KTP</label><br>
                            <center>
                                <img style="width:70px;height:70px" src="{{config('app.storage')}}foto_ktp/{{Session::get('foto_ktp')}}" alt="">
                                <i id="arrow-ktp" class="fa fa-angle-double-right"></i>
                                <img style="width:70px;height:70px" src="" id="prev_foto_ktp" alt="">
                            </center>
                            <input type="file" name="foto_ktp" class="form-control" id="foto_ktp" placeholder="" value="{{Session::get('foto_ktp')}}">
                        </div>
                        <div class="form-group">
                            <label for="">Foto NPWP</label><br>
                            <center>
                                <img style="width:70px;height:70px" src="{{config('app.storage')}}foto_npwp/{{Session::get('foto_npwp')}}" alt="">
                                <i id="arrow-npwp" class="fa fa-angle-double-right"></i>
                                <img style="width:70px;height:70px" src="" id="prev_foto_npwp" alt="">
                            </center>
                            <input type="file" name="foto_npwp" class="form-control" id="foto_npwp" placeholder="" value="{{Session::get('foto_npwp')}}">
                        </div>
                        <center><button type="submit" class="btn btn-primary">Ubah Profil</button></center>
                    </form>
                </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold text-primary">Ganti Password</h6>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <form method="POST" action="{{route('change_password')}}">
                        @csrf
                        <div class="form-group">
                            <label for="">Password Lama</label>
                            <input type="password" name="password_lama" class="form-control" id="" placeholder="">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Password Baru</label>
                            <input type="password" name="password_baru" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="">
                        </div>
                        <center><button type="submit" class="btn btn-primary">Ubah Password</button></center>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $('#arrow_diri').hide()
        $('#arrow_ktp').hide()
        $('#arrow_npwp').hide()

        $('#profil').modal('show')

        $("#foto_diri").change(function() {
            fotoDiriSrc(this);
            $('#arrow_diri').show()
          });

          function fotoDiriSrc(input) {
            if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function(e) {
                $('#prev_foto_diri').attr('src', e.target.result);
              }
              
              reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
          }

        $("#foto_ktp").change(function() {
            fotoKtpSrc(this);
            $('#arrow_ktp').hide()
          });

          function fotoKtpSrc(input) {
            if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function(e) {
                $('#prev_foto_ktp').attr('src', e.target.result);
              }
              
              reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
          }

        $("#foto_npwp").change(function() {
            fotoNpwpSrc(this);
            $('#arrow_npwp').hide()
          });

          function fotoNpwpSrc(input) {
            if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function(e) {
                $('#prev_foto_npwp').attr('src', e.target.result);
              }
              
              reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
          }
    })
</script>
@endsection