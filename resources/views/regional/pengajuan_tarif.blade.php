@extends('component.master_'.Session::get('role'))

@section('content')
<style>
  div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Pengajuan Tarif</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered datatable mdl-data-table display nowrap" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>Kota Asal</center></th>
                    <th><center>Kota Tujuan</center></th>
                    <th><center>Tarif</center></th>
                    <th><center>Jenis Pengiriman</center></th>
                    <th><center>Jenis Pengangkutan</center></th>
                    <th><center>Barang Yang Diangkut (Jika jenis nya Koli)</center></th>
                    <th><center>Minimal Angkut</center></th>
                    <th><center>Maksimal Angkut</center></th>
                    <th><center>Durasi</center></th>
                    <th><center>Moda</center></th>
                    <th><center>ID Mitra / Nama Mitra</center></th>
                    <th><center>No Telp</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="modal" id="modalTolak" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Penolakan Pengajuan Tarif</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{route('regional.update_pengajuan_tarif', 'tolak')}}" method="POST">
                  @csrf
                  <input type="hidden" id="id_tarif" name="id_tarif" class="form-control">
                  <label for="">Keterangan / Alasan Penolakan</label><br>
                  <textarea required name="keterangan_penolakan" id="keterangan_penolakan" cols="30" rows="10" class="form-control"></textarea>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">OK</button>
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){

          $('.datatable').DataTable({
            "scrollX": true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('regional.get_pengajuan_tarif') }}',
            "columns": [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'asal', name: 'asal'},
              {data: 'tujuan', name: 'tujuan'},
              {data: 'tarif', name: 'tarif'},
              {data: 'jenis_pengiriman', name: 'jenis_pengiriman'},
              {data: 'jenis_pengangkutan', name: 'jenis_pengangkutan'},
              {data: 'nama_barang', name: 'nama_barang'},
              {data: 'minimal_angkut', name: 'minimal_angkut'},
              {data: 'maksimal_angkut', name: 'maksimal_angkut'},
              {data: 'durasi', name: 'durasi'},
              {data: 'moda', name: 'moda'},
              {data: 'mitra', name: 'mitra'},
              {data: 'no_telp', name: 'no_telp'},
              {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
          ],
          'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ]
        });

        $(document).on('click', '#btn_tolak', function() {
          
          var id  = $(this).data("id");
            $('#modalTolak').modal('show')
            $('#id_tarif').val(id)

        })
          // var tokenraja = $('#token_raja').val()

       

          // function readURLEdit(input) {
          //   if (input.files && input.files[0]) {
          //     var reader = new FileReader();
              
          //     reader.onload = function(e) {
          //       $('#gambar_edit_preview').attr('src', e.target.result);
          //     }
              
          //     reader.readAsDataURL(input.files[0]); // convert to base64 string
          //   }
          // }

          // $("#gambar_edit").change(function() {
          //   readURLEdit(this);
          // });

          // function readURL(input) {
          //   if (input.files && input.files[0]) {
          //     var reader = new FileReader();
              
          //     reader.onload = function(e) {
          //       $('#gambar_preview').attr('src', e.target.result);
          //     }
              
          //     reader.readAsDataURL(input.files[0]); // convert to base64 string
          //   }
          // }

          // $("#gambar").change(function() {
          //   readURL(this);
          // });

        })
      </script>
@endsection