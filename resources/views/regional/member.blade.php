@extends('component.master_'.Session::get('role'))

@section('content')
<style>
  div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Member</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-body">
            <div class="table-responsive">
              <center>
                <label for="">Status Verifikasi</label>
                <select name="" style=width:50% id="status" class="form-control">
                  <option required value="0">Semua</option>
                  <option value="1">Terverifikasi</option>
                  <option value="2">Belum Terverifkasi</option>
              </select><br>
              </center>
              <table class="table table-bordered datatable mdl-data-table display nowrap" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>Email</center></th>
                    <th><center>Nama Lengkap</center></th>
                    <th><center>TTL</center></th>
                    <th><center>Alamat</center></th>
                    <th><center>No Telp</center></th>
                    <th><center>Foto</center></th>
                    <th><center>Status</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>


      </div>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){

          getData()

          $(document).on('change', '#status', function(){
            table = $('.datatable').DataTable()
            table.destroy()
            getData()
          })

          function getData()
          {
            let val = $('#status').val()
            var url = '{{ route('regional.get_member', ":val") }}'
            url     = url.replace(':val', val)

            $('.datatable').DataTable({
              "scrollX" : true,
              processing: true,
              serverSide: true,
              ajax: url,
              "columns": [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'email', name: 'email'},
                {data: 'nama_lengkap', name: 'nama_lengkap'},
                {data: 'ttl', name: 'ttl'},
                {data: 'alamat', name: 'alamat'},
                {data: 'no_telp', name: 'no_telp'},
                {data: 'foto', name: 'foto'},
                {data: 'status', name: 'status'},
                {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
              ],
              'columnDefs': [
                {
                    "targets": "_all", // your case first column
                    "className": "text-center",
                }
              ]
            });
          }

          

        $(document).on('click', '#btn_edit', function() {

          var id  = $(this).data("id");
          var url = '{{ route('regional.show_mitra_kurir', ":id") }}'
          url     = url.replace(':id', id)

          $.get(url , function(data, status){
              let element = JSON.parse(data)
              $('#modalEdit').modal('show')

              $('#email_edit').text(element['user'].email)
              $('#nama_lengkap_edit').text(element['biodata'].nama_lengkap)
              $('#ttl_edit').text(element['biodata'].tempat_lahir+', '+element['biodata'].tanggal_lahir)
              $('#alamat_edit').text(element['biodata'].alamat)
              $('#no_telp_edit').text(element['biodata'].no_telp)
              $('#nik_edit').text(element['biodata'].nik)
              $('#kendaraan_edit').text(element['mitra'].jenis_kendaraan+'-'+element['mitra'].nama_kendaraan)
              $('#plat_edit').text(element['mitra'].plat_kendaraan)
              $('#tahun_kendaraan_edit').text(element['mitra'].tahun_kendaraan)
              $('#domisili_edit').text(element['mitra'].domisili)
              $('#status_edit').text(element['mitra'].status_ketersediaan)
              $('#foto_diri_edit').attr('src', `{{config('app.storage')}}foto_diri/${element['biodata'].foto_diri}`)
              $('#foto_npwp_edit').attr('src', `{{config('app.storage')}}foto_npwp/${element['biodata'].foto_npwp}`)
              $('#foto_ktp_edit').attr('src', `{{config('app.storage')}}foto_ktp/${element['biodata'].foto_ktp}`)
              $('#foto_stnk_edit').attr('src', `{{config('app.storage')}}foto_stnk/${element['mitra'].foto_stnk}`)
              $('#foto_sim_edit').attr('src', `{{config('app.storage')}}foto_sim/${element['mitra'].foto_sim}`)
          })

        })


        })
      </script>
@endsection