@extends('component.master_'.Session::get('role'))

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Pengiriman dari {{$kota}}</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered datatable mdl-data-table" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>No Resi</center></th>
                    <th><center>Kota Tujuan</center></th>
                    <th><center>Jenis Pengiriman</center></th>
                    <th><center>Jenis Pengangkutan</center></th>
                    <th><center>Berat / Dimensional / Total Unit</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="modal" tabindex="-1" id="modalEdit" role="dialog">
          <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Detail Pengiriman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="col-xl-12">
                  <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">Data Pengiriman</h6>
                    </div>
                    <div class="card-body col-md-12">
                      @csrf
                      <input type="hidden" name="id" id="id">
                      <b><i class="fa fa-number"></i><label for="">Nomor Resi</label></b><br>
                      <p id="no_resi"></p><br>

                      <b><label for="">Jenis Pengangkutan</label></b><br>
                      <p id="jenis_pengangkutan"></p><br>

                      <b><label for="">Deskripsi Barang</label></b><br>
                      <p id="deskripsi_barang"></p><br>

                      <b><label for="">Ukuran Pengiriman (Berat / Dimensi / Unit )</label></b><br>
                      <p id="ukuran"></p><br>

                      <b><label for="">Jenis Pengiriman</label></b><br>
                      <p id="jenis_pengiriman"></p><br>

                      <b><label for="">Asal</label></b><br>
                      <p id="asal"></p><br>

                      <b><label for="">Tujuan</label></b><br>
                      <p id="tujuan"></p><br>

                      <b><label for="">Alamat Tujuan</label></b><br>
                      <p id="alamat_tujuan"></p><br>

                      <b><label for="">Tarif</label></b><br>
                      <p id="tarif"></p><br>

                      <b><label for="">Kode / ID Kelompok Pengiriman</label></b><br>
                      <p id="id_kelompok_pengiriman"></p><br>
                      
                      <b><label for="">Diterima Oleh Cabang</label></b><br>
                      <p id="cabang"></p><br>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xl-6">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Data Pengirim</h6>
                      </div>
                      <div class="card-body col-md-6">
                        @csrf
                        <b><label for="">Nama Pengirim</label></b><br>
                        <p id="nama_pengirim"></p><br>

                        <b><label for="">No Telp Pengirim</label></b><br>
                        <p id="no_telp_pengirim"></p><br>

                        <b><label for="">Member</label></b><br>
                        <p id="id_member"></p><br>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-6">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Data Penerima</h6>
                      </div>
                      <div class="card-body col-md-6">
                        @csrf
                        <b><label for="">Nama Penerima</label></b><br>
                        <p id="nama_penerima"></p><br>

                        <b><label for="">No Telp Penerima</label></b><br>
                        <p id="no_telp_penerima"></p><br>

                        <b><label style="color:white"for="">blank</label></b><br>
                        <p id=""></p><br>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xl-6">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Data Kurir</h6>
                      </div>
                      <div class="card-body col-md-6">
                        @csrf
                        <b><label for="">ID Mitra Kurir</label></b><br>
                        <p id="id_mitra"></p><br>

                        <b><label for="">Nama Mitra Kurir</label></b><br>
                        <p id="nama_mitra"></p><br>

                        <b><label for="">No Telp Mitra Kurir</label></b><br>
                        <p id="no_telp_mitra"></p><br>

                        <b><label for="">ID Tarif Line</label></b><br>
                        <p id="id_tarif_line"></p><br>

                        <b><label for="">Tarif Line</label></b><br>
                        <p id="tarif_line"></p><br>

                        <b><label style="color:white"for="">blank</label></b><br>
                        <p id=""></p><br>
                      </div>
                    </div>
                  </div>
                  <div class="col-xl-6">
                    <div class="card shadow mb-4">
                      <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Keterangan</h6>
                      </div>
                      <div class="card-body col-md-6">
                        @csrf
                        <b><label for="">Tanggal Masuk</label></b><br>
                        <p id="tanggal_masuk"></p><br>

                        <b><label for="">Tanggal Pengiriman</label></b><br>
                        <p id="tanggal_pengiriman"></p><br>

                        <b><label for="">Perkiraan Tanggal Sampai</label></b><br>
                        <p id="perkiraan_tanggal_sampai"></p><br>

                        <b><label for="">Tanggal Sampai</label></b><br>
                        <p id="tanggal_sampai"></p><br>

                        <b><label for="">Jumlah Hari Telat</label></b><br>
                        <p id="jumlah_hari_telat"></p><br>

                        <b><label for="">Keterangan Tambahan</label></b><br>
                        <p id="keterangan"></p><br>

                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-12">
                  <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">Status Pengiriman</h6>
                    </div>
                    <div class="card-body col-md-12">
                      <div class="table-responsive">
                        <table class="display" style="width:100%" id="dataTableStatus" cellspacing="0">
                          <thead>
                            <tr>
                              <th><center>No</center></th>
                              <th><center>Tanggal</center></th>
                              <th><center>Status</center></th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      </div>
      {{-- <script src="{{URL::to('/')}}../node_modules/moment/moment.js"></script> --}}
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){

          $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('regional.get_pengiriman') }}',
            "columns": [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'no_resi', name: 'no_resi'},
              {data: 'kota_tujuan', name: 'kota_tujuan'},
              {data: 'jenis_pengiriman', name: 'jenis_pengiriman'},
              {data: 'jenis_pengangkutan', name: 'jenis_pengangkutan'},
              {data: 'ukuran', name: 'ukuran'},
              {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
          ],
          'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ]
        });

        $(document).on('click', '#btn_edit', function() {
          var id  = $(this).data("id");
          console.log(id)
          var url = '{{ route('regional.show_pengiriman', ":id") }}'
          url     = url.replace(':id', id)

            $.get(url , function(data, status){
              let element = JSON.parse(data)
              console.log(element)
              $('#modalEdit').modal('show')
              $('#no_resi').text(element.no_resi)
              $('#jenis_pengangkutan').text(element.jenis_pengangkutan)
              $('#deskripsi_barang').text(element.deskripsi_barang)
              $('#ukuran').text(element.ukuran)
              $('#jenis_pengiriman').text(element.jenis_pengiriman)
              $('#asal').text(element.asal)
              $('#tujuan').text(element.tujuan)
              $('#alamat_tujuan').text(element.alamat_tujuan)
              $('#tarif').text(element.tarif)
              $('#id_kelompok_pengiriman').text(element.id_kelompok_pengiriman)
              $('#cabang').text(element.cabang)
              $('#nama_pengirim').text(element.nama_pengirim)
              $('#no_telp_pengirim').text(element.no_telp_pengirim)
              $('#id_member').text(element.id_member)
              $('#nama_penerima').text(element.nama_penerima)
              $('#no_telp_penerima').text(element.no_telp_penerima)
              $('#id_mitra').text(element.id_mitra)
              $('#nama_mitra').text(element.nama_mitra)
              $('#no_telp_mitra').text(element.no_telp_mitra)
              $('#id_tarif_line').text(element.id_tarif_line)
              $('#tarif_line').text(element.tarif_line)
              $('#tanggal_masuk').text(element.tanggal_masuk)
              $('#tanggal_pengiriman').text(element.tanggal_pengiriman)
              $('#perkiraan_tanggal_sampai').text(element.perkiraan_tanggal_sampai)
              $('#tanggal_sampai').text(element.tanggal_sampai)
              $('#jumlah_hari_telat').text(element.jumlah_hari_telat)
              $('#keterangan').text(element.keterangan)

              var t = $('#dataTableStatus').DataTable();
              var counter = 1;

              $.each(element.status_pengiriman, function(index, value){
                var tanggal = moment(value.tanggal).format('dddd, DD MMMM YYYY')
                t.row.add( [
                    counter,
                    tanggal,
                    value.status,
                ] ).draw( false );
        
                counter++;
              })

            })


        })

        $('#provinsi_edit').change(function(){
          $('#kota_edit').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_edit');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_edit');
            });
          });
        })
          // var tokenraja = $('#token_raja').val()

       

          // function readURLEdit(input) {
          //   if (input.files && input.files[0]) {
          //     var reader = new FileReader();
              
          //     reader.onload = function(e) {
          //       $('#gambar_edit_preview').attr('src', e.target.result);
          //     }
              
          //     reader.readAsDataURL(input.files[0]); // convert to base64 string
          //   }
          // }

          // $("#gambar_edit").change(function() {
          //   readURLEdit(this);
          // });

          // function readURL(input) {
          //   if (input.files && input.files[0]) {
          //     var reader = new FileReader();
              
          //     reader.onload = function(e) {
          //       $('#gambar_preview').attr('src', e.target.result);
          //     }
              
          //     reader.readAsDataURL(input.files[0]); // convert to base64 string
          //   }
          // }

          // $("#gambar").change(function() {
          //   readURL(this);
          // });

        })
      </script>
@endsection