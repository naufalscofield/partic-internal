@extends('component.master_'.Session::get('role'))

@section('content')
    
    <div class="container-fluid">
        
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800">Profil Kantor Regional - {{$regional->kota}}</h1>
        <div class="row">
            <div class="col-xl-12">
                <div class="card shadow mb-4">
                <!-- Card Header - Dropdown -->
                <!-- Card Body -->
                <div class="card-body">
                    <form method="POST" action="{{route('regional.update_profil_kantor')}}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" class="form-control" id="" placeholder="" value="{{$regional->id}}">
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" name="email" class="form-control" id="" placeholder="" value="{{$regional->email}}">
                        </div>
                        <div class="form-group">
                            <label for="">No Telp</label>
                            <input type="text" name="no_telp" class="form-control" id="" placeholder="" value="{{$regional->no_telp}}">
                        </div>
                        <div class="form-group">
                            <label for="">Alamat</label>
                            <textarea type="text" name="alamat" class="form-control" id="" placeholder="">{{$regional->alamat}}</textarea>
                        </div>
                        <center><button type="submit" class="btn btn-primary">Ubah Profil</button></center>
                    </form>
                </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function(){
        $('#arrow_diri').hide()
        $('#arrow_ktp').hide()
        $('#arrow_npwp').hide()

        $('#profil').modal('show')

        $("#foto_diri").change(function() {
            fotoDiriSrc(this);
            $('#arrow_diri').show()
          });

          function fotoDiriSrc(input) {
            if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function(e) {
                $('#prev_foto_diri').attr('src', e.target.result);
              }
              
              reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
          }

        $("#foto_ktp").change(function() {
            fotoKtpSrc(this);
            $('#arrow_ktp').hide()
          });

          function fotoKtpSrc(input) {
            if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function(e) {
                $('#prev_foto_ktp').attr('src', e.target.result);
              }
              
              reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
          }

        $("#foto_npwp").change(function() {
            fotoNpwpSrc(this);
            $('#arrow_npwp').hide()
          });

          function fotoNpwpSrc(input) {
            if (input.files && input.files[0]) {
              var reader = new FileReader();
              
              reader.onload = function(e) {
                $('#prev_foto_npwp').attr('src', e.target.result);
              }
              
              reader.readAsDataURL(input.files[0]); // convert to base64 string
            }
          }
    })
</script>
@endsection