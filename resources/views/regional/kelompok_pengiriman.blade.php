@extends('component.master_'.Session::get('role'))

@section('content')
<style>
  div.dataTables_wrapper {
        width: 100%;
        margin: 0 auto;
    }
    .slidecontainer {
  width: 100%; /* Width of the outside container */
}

/* The slider itself */
.slider {
  -webkit-appearance: none;
  width: 100%;
  height: 15px;
  border-radius: 5px;  
  background: #d3d3d3;
  outline: none;
  opacity: 0.7;
  -webkit-transition: .2s;
  transition: opacity .2s;
}

.slider::-webkit-slider-thumb {
  -webkit-appearance: none;
  appearance: none;
  width: 25px;
  height: 25px;
  border-radius: 50%; 
  background: #e534ab;
  cursor: pointer;
}

.slider::-moz-range-thumb {
  width: 25px;
  height: 25px;
  border-radius: 50%;
  background: #e534ab;
  cursor: pointer;
}
</style>
<link rel="stylesheet" href="/ladda/dist/ladda-themeless.min.css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Kelompok Pengiriman dari {{$kota}}</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <button class="btn btn-primary" data-toggle="modal" id="btn_add" data-target="#modalAdd">+ Kelompok Pengiriman Baru</button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered datatable mdl-data-table display nowrap" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>ID/Nama Mitra</center></th>
                    <th><center>ID Tarif Line</center></th>
                    <th><center>Asal</center></th>
                    <th><center>Tujuan</center></th>
                    <th><center>Jenis Pengangkutan</center></th>
                    <th><center>Jenis Pengiriman</center></th>
                    <th><center>Ukuran Total (Berat / Dimensional / Unit)</center></th>
                    <th><center>Tanggal Pickup</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>


      <div class="modal" tabindex="-1" id="modalAdd" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Kelompok Pengiriman Baru</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="{{route('regional.store_kelompok_pengiriman')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <label for="">Tujuan Pengiriman</label><br>
                <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="tujuan" name="tujuan"><br>
                </select><br>
                
                <label for="">Jenis Pengangkutan</label><br>
                <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="jenis_pengangkutan" name="jenis_pengangkutan">
                </select><br>


                <label for="">Jenis Pengiriman</label><br>
                <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="jenis_pengiriman" name="jenis_pengiriman"><br>
                </select><br>

                <h5 id="total"></h5>
                <h6 id="ukuran"></h6>

                <input type="hidden" id="berat" name="berat">
                <input type="hidden" id="dimensi" name="dimensi">
                <input type="hidden" id="unit" name="unit">
                
            </div>
            <div class="modal-footer">
              <button type="submit" disabled id="btn-store" class="btn btn-primary">Buat Kelompok Pengiriman</button>
            </div>
              </form>
          </div>
        </div>
      </div>

      <div class="modal" tabindex="-1" id="modalPromethee" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">Pemilihan Mitra Kurir Dengan Metode Promethee</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form action="{{route('regional.store_promethee')}}" method="POST" enctype="multipart/form-data">
                @csrf
                <center>
                  <h3 id="sedang">Sedang Memproses Metode Promethee</h3>
                  <div class="spinner-border text-success" id="spinnerPromethee" role="status">
                    <span class="sr-only">Loading...</span>
                  </div>
                </center>

                <div class="col-xl-12">
                  <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">Tabel Penilaian</h6>
                    </div>
                    <div class="card-body col-md-12">
                      <div class="table-responsive">
                        <table class="display" style="width:100%" id="dTTabelPenilaian" cellspacing="0">
                          <thead>
                            <tr>
                              <th><center>ID Mitra</center></th>
                              <th><center>Alias</center></th>
                              <th><center>Nilai K1</center></th>
                              <th><center>Nilai K2</center></th>
                              <th><center>Nilai K3</center></th>
                              <th><center>Nilai K4</center></th>
                              {{-- <th><center>Nilai K5</center></th> --}}
                              <th><center>Jumlah</center></th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-xl-12">
                  <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">Tabel Index Preferensi</h6>
                    </div>
                    <div class="card-body col-md-12">
                      <div class="table-responsive">
                        <table class="display" style="width:100%" id="dTTabelPreferensi" cellspacing="0">
                          <thead>
                            <tr>
                              <th><center>Mitra</center></th>
                              <th><center>Nilai Preferensi</center></th>
                              <th><center>Total (Jumlah Nilai / Jumlah Kriteria)</center></th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="col-xl-12">
                  <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">Tabel Flows</h6>
                    </div>
                    <div class="card-body col-md-12">
                      <div class="table-responsive">
                        <table class="display" style="width:100%" id="dTTabelFlows" cellspacing="0">
                          <thead>
                            <tr>
                              <th><center>ID Mitra</center></th>
                              <th><center>Alias Mitra</center></th>
                              <th><center>Leaving Flow</center></th>
                              <th><center>Entering Flow</center></th>
                              <th><center>Net Flow</center></th>
                              <th><center>Ranking</center></th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
            <input type="hidden" id="id_kelompok_pengiriman" value=0 name="id_kelompok_pengiriman">
            <input type="hidden" id="calon_mitra1" value=0 name="calon_mitra1">
            <input type="hidden" id="calon_mitra2" value=0 name="calon_mitra2">
            <input type="hidden" id="calon_mitra3" value=0 name="calon_mitra3">
            <input type="hidden" id="id_tarif_line1" value=0 name="id_tarif_line1">
            <input type="hidden" id="id_tarif_line2" value=0 name="id_tarif_line2">
            <input type="hidden" id="id_tarif_line3" value=0 name="id_tarif_line3">
            <div class="modal-footer">
              <button type="submit" id="" class="btn btn-primary">Simpan</button>
            </div>
              </form>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modalSmart" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Metode Smart</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="slidecontainer">
                <input type="hidden" id="id_kelompok_smart" name="id_kelompok_smart">
                <label for="">Persentase Bobot Durasi Pengiriman </label><b><label id="labelDurasi" for="">-15%</label></b>
                <input type="range" min="1" max="100" value="15" class="slider" name="durasi" id="durasi">

                <label for="">Persentase Bobot Tarif Kurir </label><label for=""></label><b><label id="labelTarif" for="">-80%</label></b>
                <input type="range" min="1" max="100" value="80" class="slider" name="tarif" id="tarif">

                <label for="">Persentase Bobot Catatan Hitam Kurir </label><label for=""></label><b><label id="labelCatatanHitam" for="">-5%</label></b>
                <input type="range" min="1" max="100" value="5" class="slider" name="catatan_hitam" id="catatan_hitam">
              </div>
              <hr>
              <div id="afterSmart">
              <div class="col-xl-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tabel Data Normalisasi Bobot Kriteria</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="table-responsive">
                      <table class="display" style="width:100%" id="dTTabelNormalisasi" cellspacing="0">
                        <thead>
                          <tr>
                            <th><center>Kriteria</center></th>
                            <th><center>Normalisasi Bobot</center></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <center><h4>Kamus Nilai Durasi</h4><img style="width:250px;height:100px" src="{{URL::to('/')}}/img/kamusdurasi.png" alt=""></center>
              <div class="col-xl-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tabel Data Nilai Kriteria Durasi</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="table-responsive">
                      <table class="display" style="width:100%" id="dTTabelNilaiDurasi" cellspacing="0">
                        <thead>
                          <tr>
                            <th><center>ID Mitra</center></th>
                            <th><center>Nilai Durasi</center></th>
                            <th><center>Durasi</center></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <center><h4>Kamus Nilai Tarif</h4><img style="width:250px;height:100px" src="{{URL::to('/')}}/img/kamustarif.png" alt=""></center>
              <div class="col-xl-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tabel Data Nilai Kriteria Tarif</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="table-responsive">
                      <table class="display" style="width:100%" id="dTTabelNilaiTarif" cellspacing="0">
                        <thead>
                          <tr>
                            <th><center>ID Mitra</center></th>
                            <th><center>Nilai Tarif</center></th>
                            <th><center>Tarif</center></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <center><h4>Kamus Nilai Catatan Hitam</h4><img style="width:250px;height:100px" src="{{URL::to('/')}}/img/kamusch.png" alt=""></center>
              <div class="col-xl-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tabel Data Nilai Kriteria Catatan Hitam</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="table-responsive">
                      <table class="display" style="width:100%" id="dTTabelNilaiCatatanHitam" cellspacing="0">
                        <thead>
                          <tr>
                            <th><center>ID Mitra</center></th>
                            <th><center>Nilai Catatan Hitam</center></th>
                            <th><center>Jumlah Catatan Hitam</center></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <center><h3>Rumus Utility</h3><img style="width:250px;height:80px" src="{{URL::to('/')}}/img/utility.png" alt=""></center>
              <div class="col-xl-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tabel Data Utility Durasi</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="table-responsive">
                      <table class="display" style="width:100%" id="dTTabelUtilityDurasi" cellspacing="0">
                        <thead>
                          <tr>
                            <th><center>ID Mitra</center></th>
                            <th><center>Nilai Utility Durasi</center></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tabel Data Utility Tarif</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="table-responsive">
                      <table class="display" style="width:100%" id="dTTabelUtilityTarif" cellspacing="0">
                        <thead>
                          <tr>
                            <th><center>ID Mitra</center></th>
                            <th><center>Nilai Utility Tarif</center></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tabel Data Utility Catatan Hitam</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="table-responsive">
                      <table class="display" style="width:100%" id="dTTabelUtilityCatatanHitam" cellspacing="0">
                        <thead>
                          <tr>
                            <th><center>ID Mitra</center></th>
                            <th><center>Nilai Utility Catatan Hitam</center></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tabel Utility Durasi x Bobot Durasi</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="table-responsive">
                      <table class="display" style="width:100%" id="dTTabelUtilityXBobotDurasi" cellspacing="0">
                        <thead>
                          <tr>
                            <th><center>ID Mitra</center></th>
                            <th><center>Nilai Utility Durasi x Bobot Durasi</center></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tabel Utility Tarif x Bobot Tarif</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="table-responsive">
                      <table class="display" style="width:100%" id="dTTabelUtilityXBobotTarif" cellspacing="0">
                        <thead>
                          <tr>
                            <th><center>ID Mitra</center></th>
                            <th><center>Nilai Utility Tarif x Bobot Tarif</center></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tabel Utility Catatan Hitam x Bobot Catatan Hitam</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="table-responsive">
                      <table class="display" style="width:100%" id="dTTabelUtilityXBobotCatatanHitam" cellspacing="0">
                        <thead>
                          <tr>
                            <th><center>ID Mitra</center></th>
                            <th><center>Nilai Utility Catatan Hitam x Bobot Catatan Hitam</center></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-xl-12">
                <div class="card shadow mb-4">
                  <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tabel Total Nilai</h6>
                  </div>
                  <div class="card-body col-md-12">
                    <div class="table-responsive">
                      <table class="display" style="width:100%" id="dTTabelTotalNilai" cellspacing="0">
                        <thead>
                          <tr>
                            <th style="color:white"><center>ID Mitra</center></th>
                            <th style="color:white"><center>ID Tarif</center></th>
                            <th style="color:white"><center>Nilai Total</center></th>
                            <th style="color:white"><center>Ranking</center></th>
                          </tr>
                        </thead>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <form action="{{route('regional.store_smart_final')}}" method="POST">
                @csrf
                <input type="hidden" name="idKelompokPengiriman" id="idKelompokPengiriman">
                <input type="hidden" name="idMitraFix" id="idMitraFix">
                <input type="hidden" name="idTarifFix" id="idTarifFix">
                <center>
                  <button type="submit" class="btn btn-primary">Simpan Hasil</button>
                </center>
              </form>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button class="login100-form-btn ladda-button btn btn-primary" data-style="zoom-out" type="button" id="btn_aksi_smart">Olah Metode</button>
            </div>
          </div>
        </div>
      </div>

      </div>
      <script src="/ladda/dist/spin.min.js"></script>
	    <script src="/ladda/dist/ladda.min.js"></script>
      {{-- <script src="{{URL::to('/')}}../node_modules/moment/moment.js"></script> --}}
      <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){
          $('#afterSmart').hide()
          $('#dTTabelNormalisasi').css("background-color", "lightpink");
          $('#dTTabelNilaiDurasi').css("background-color", "PaleTurquoise");
          $('#dTTabelNilaiDurasi').css("background-color", "PaleTurquoise");
          $('#dTTabelNilaiTarif').css("background-color", "PaleTurquoise");
          $('#dTTabelNilaiCatatanHitam').css("background-color", "PaleTurquoise");
          $('#dTTabelUtilityDurasi').css("background-color", "palegreen");
          $('#dTTabelUtilityTarif').css("background-color", "palegreen");
          $('#dTTabelUtilityCatatanHitam').css("background-color", "palegreen");
          $('#dTTabelUtilityXBobotDurasi').css("background-color", "lemonchiffon");
          $('#dTTabelUtilityXBobotTarif').css("background-color", "lemonchiffon");
          $('#dTTabelUtilityXBobotCatatanHitam').css("background-color", "lemonchiffon");
          $('#dTTabelTotalNilai').css("background-color", "#e534ab");

          $('.datatable').DataTable({
            "scrollX" : true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('regional.get_kelompok_pengiriman') }}',
            "columns": [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'mitra', name: 'mitra'},
              {data: 'tarif_line', name: 'tarif_line'},
              {data: 'asal', name: 'asal'},
              {data: 'tujuan', name: 'tujuan'},
              {data: 'jenis_pengangkutan', name: 'jenis_pengangkutan'},
              {data: 'jenis_pengiriman', name: 'jenis_pengiriman'},
              {data: 'ukuran', name: 'ukuran'},
              {data: 'tanggal_pickup', name: 'tanggal_pickup'},
              {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
          ],
          'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ]
        });

        $(document).on('click', '#btn_promethee', function() {

          var id = $(this).data('id')
          $('#id_kelompok_pengiriman').val(id)
          $('#modalPromethee').show()
          $('#modalPromethee').modal({backdrop: 'static', keyboard: false})  
          var url = '{{ route('regional.promethee', ":id") }}'
          url     = url.replace(':id', id)

            $.get(url , function(data, status){
              var element = JSON.parse(data)
              console.log(element)
              $('#spinnerPromethee').hide()
              $('#sedang').hide()

              var t = $('#dTTabelPenilaian').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

              var t2 = $('#dTTabelPreferensi').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

              var t3 = $('#dTTabelFlows').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });
              
              $.each(element.tabel_penilaian, function(index, value){
                  t.row.add( [
                        value['id_mitra'],
                        value['alias'],
                        value['K1'],
                        value['K2'],
                        value['K3'],
                        value['K4'],
                        // value['K5'],
                        value['Jumlah'],
                    ]).draw( false )
                    
                })

                $.each(element.tabel_preferensi, function(index, value){
                  t2.row.add( [
                        value['index1']+','+value['index2'],
                        value['nilai_pref_1']+ "+" +value['nilai_pref_2']+ "+" +value['nilai_pref_3']+"+"+value['nilai_pref_4'],
                        value['Total'],
                    ]).draw( false )
                    
                })

                var ranking = 1;
                $.each(element.tabel_flows, function(index, value){
                  console.log(value)
                  t3.row.add( [
                        value['id_mitra'],
                        value['alternatif'],
                        value['leaving_flow'],
                        value['entering_flow'],
                        value['net_flow'],
                        ranking
                    ]).draw( false )
                    if (ranking == 1)
                    {
                      $('#calon_mitra1').val(value['id_mitra'])
                      $('#id_tarif_line1').val(value['id_tarif'])
                    } else if (ranking == 2)
                    {
                      $('#calon_mitra2').val(value['id_mitra'])
                      $('#id_tarif_line2').val(value['id_tarif'])
                    } else if (ranking == 3)
                    {
                      $('#calon_mitra3').val(value['id_mitra'])
                      $('#id_tarif_line3').val(value['id_tarif'])
                    }
                    
                    
                    ranking++
                    
                })

            })

        })

        $(document).on('click', '#btn_smart', function() {
          $('#modalSmart').modal('show')
          var id = $(this).data('id')
          console.log(id)
          $('#id_kelompok_smart').val(id)
          $('#idKelompokPengiriman').val(id)
        })

        $(document).on('change', '#durasi', function() {
          var durasi = $(this).val()
          var labelDurasi = " - "+durasi+"%"
          $('#labelDurasi').text(labelDurasi)


        })
        $(document).on('change', '#tarif', function() {
          var tarif = $(this).val()
          var labelTarif = " - "+tarif+"%"
          $('#labelTarif').text(labelTarif)
        })

        $(document).on('change', '#catatan_hitam', function() {
          var catatan_hitam = $(this).val()
          var labelCatatanHitam = " - "+catatan_hitam+"%"
          $('#labelCatatanHitam').text(labelCatatanHitam)
        })

        $(document).on('click', '#btn_aksi_smart', function(e) {
          var l = Ladda.create(this);
          l.start();
          e.preventDefault();

          var url = '{{ route('regional.store_smart') }}'

          $.post(url,
          {
            "_token": "{{ csrf_token() }}",
            id_kelompok_pengiriman: $('#id_kelompok_smart').val(),
            durasi: $('#durasi').val(),
            tarif: $('#tarif').val(),
            catatan_hitam: $('#catatan_hitam').val(),
          },
          function(data, status){
            var element = JSON.parse(data)
            console.log(element)

            l.stop()
            $('#afterSmart').show()

            var t0 = $('#dTTabelNormalisasi').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });
              

            var t1 = $('#dTTabelNilaiDurasi').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

            var t2 = $('#dTTabelNilaiTarif').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

            var t3 = $('#dTTabelNilaiCatatanHitam').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

            var t4 = $('#dTTabelUtilityDurasi').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

            var t5 = $('#dTTabelUtilityTarif').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

            var t6 = $('#dTTabelUtilityCatatanHitam').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

            var t7 = $('#dTTabelUtilityXBobotDurasi').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

            var t8 = $('#dTTabelUtilityXBobotTarif').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

            var t9 = $('#dTTabelUtilityXBobotCatatanHitam').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

            var t10 = $('#dTTabelTotalNilai').DataTable({
                'columnDefs': [
                      {
                          "targets": "_all", // your case first column
                          "className": "text-center",
                      }
                    ],
                  'bFilter' : false,
                  "lengthChange": false,
                  "paging"  : false,
                  "info"  : false
              });

              var kriteria = ['Durasi', 'Tarif', 'Catatan Hitam']
              var idxKriteria = 0
              $.each(element.tableBobot, function(index, value){
                  t0.row.add( [
                        kriteria[idxKriteria],
                        value
                    ]).draw( false )
                    idxKriteria++  
                })

              var idxMitraDurasi = 0;
              $.each(element.tableNilaiDurasi, function(index, value){
                  t1.row.add( [
                        element.tableMitra[idxMitraDurasi],
                        value,
                        element.tableDurasi[idxMitraDurasi]+' Hari'
                    ]).draw( false )
                    idxMitraDurasi++  
                })

              var idxMitraTarif = 0;
              $.each(element.tableNilaiTarif, function(index, value){
                  t2.row.add( [
                        element.tableMitra[idxMitraTarif],
                        value,
                        ' Rp.'+element.tableTarif[idxMitraTarif]
                    ]).draw( false )
                    idxMitraTarif++  
                })
                
              var idxMitraCatatanHitam = 0;
              $.each(element.tableNilaiCatatanHitam, function(index, value){
                  t3.row.add( [
                        element.tableMitra[idxMitraCatatanHitam],
                        value,
                        element.tableCatatanHitam[idxMitraCatatanHitam]+' Catatan Hitam'
                    ]).draw( false )
                    idxMitraCatatanHitam++  
                })
                
              var idxUtilityDurasi = 0;
              $.each(element.tableUtilityDurasi, function(index, value){
                  t4.row.add( [
                        element.tableMitra[idxUtilityDurasi],
                        value
                    ]).draw( false )
                    idxUtilityDurasi++  
                })
                
              var idxUtilityTarif = 0;
              $.each(element.tableUtilityTarif, function(index, value){
                  t5.row.add( [
                        element.tableMitra[idxUtilityTarif],
                        value
                    ]).draw( false )
                    idxUtilityTarif++  
                })
                
              var idxUtilityCatatanHitam = 0;
              $.each(element.tableUtilityCatatanHitam, function(index, value){
                  t6.row.add( [
                        element.tableMitra[idxUtilityCatatanHitam],
                        value
                    ]).draw( false )
                    idxUtilityCatatanHitam++  
                })
                
              var idxUtilityXBobotDurasi = 0;
              $.each(element.tableUtilityXNormalisasiDurasi, function(index, value){
                  t7.row.add( [
                        element.tableMitra[idxUtilityXBobotDurasi],
                        value
                    ]).draw( false )
                    idxUtilityXBobotDurasi++  
                })
                
              var idxUtilityXBobotTarif = 0;
              $.each(element.tableUtilityXNormalisasiTarif, function(index, value){
                  t8.row.add( [
                        element.tableMitra[idxUtilityXBobotTarif],
                        value
                    ]).draw( false )
                    idxUtilityXBobotTarif++  
                })
                
              var idxUtilityXBobotCatatanHitam = 0;
              $.each(element.tableUtilityXNormalisasiCatatanHitam, function(index, value){
                  t9.row.add( [
                        element.tableMitra[idxUtilityXBobotCatatanHitam],
                        value
                    ]).draw( false )
                    idxUtilityXBobotCatatanHitam++  
                })
                
              var idxTotalNilai = 1;
              $.each(element.tableTotalNilai, function(index, value){
                  t10.row.add( [
                        value.id_mitra,
                        value.id_tarif,
                        value.total,
                        idxTotalNilai
                    ]).draw( false )
                    idxTotalNilai++  
                })

              $('#idMitraFix').val(element.tableTotalNilai[0].id_mitra)
              $('#idTarifFix').val(element.tableTotalNilai[0].id_tarif)
          });
        })

        $(document).on('click', '#btn_add', function() {
          var url = '{{ route('regional.get_field_kelompok_pengiriman') }}'
          $('#tujuan').prop("disabled", true)
          $('<option>').val("").text("*Memuat Data").appendTo('#tujuan');

            $.get(url , function(data, status){
              console.log(data)
                var element = JSON.parse(data)
                $('#tujuan').prop("disabled", false)
                $('#tujuan').empty()
                $('<option>').val("").text("--Pilih Kota Tujuan--").appendTo('#tujuan');
                $.each(element, function(index, value){
                  $('<option>').val(value.kota_tujuan).text(value.kota_tujuan).appendTo('#tujuan');
                })

            })

        })

        $(document).on('change', '#tujuan', function() {
          var tujuan = $(this).val();
          var url = '{{ route('regional.get_field2_kelompok_pengiriman', ":tujuan") }}'
          url     = url.replace(':tujuan', tujuan)
          $('#jenis_pengangkutan').prop("disabled", true)
          $('<option>').val("").text("*Memuat Data").appendTo('#jenis_pengangkutan');

            $.get(url , function(data, status){
                var element = JSON.parse(data)
                $('#jenis_pengangkutan').prop("disabled", false)
                $('#jenis_pengangkutan').empty()
                $('<option>').val("").text("--Pilih Jenis Pengangkutan--").appendTo('#jenis_pengangkutan');
                $.each(element, function(index, value){
                  $('<option>').val(value.jenis_pengangkutan).text(value.jenis_pengangkutan).appendTo('#jenis_pengangkutan');
                })

            })

        })

        $(document).on('change', '#jenis_pengangkutan', function() {
          var tujuan = $("#tujuan").val();
          var jenis_pengangkutan = $(this).val();
          var url = '{{ route('regional.get_field3_kelompok_pengiriman', [":tujuan", ":jenis_pengangkutan"]) }}'
          url     = url.replace(':tujuan', tujuan)
          url     = url.replace(':jenis_pengangkutan', jenis_pengangkutan)
          $('#jenis_pengiriman').prop("disabled", true)
          $('<option>').val("").text("*Memuat Data").appendTo('#jenis_pengiriman');

            $.get(url , function(data, status){
                var element = JSON.parse(data)
                $('#jenis_pengiriman').prop("disabled", false)
                $('#jenis_pengiriman').empty()
                $('<option>').val("").text("--Pilih Jenis Pengiriman--").appendTo('#jenis_pengiriman');
                $.each(element, function(index, value){
                  $('<option>').val(value.jenis_pengiriman).text(value.jenis_pengiriman).appendTo('#jenis_pengiriman');
                })

            })

        })

        $(document).on('change', '#jenis_pengiriman', function() {
          var tujuan = $("#tujuan").val();
          var jenis_pengangkutan = $("#jenis_pengangkutan").val();
          var jenis_pengiriman = $(this).val();
          var url = '{{ route('regional.get_field4_kelompok_pengiriman', [":tujuan", ":jenis_pengangkutan", ":jenis_pengiriman"]) }}'
          url     = url.replace(':tujuan', tujuan)
          url     = url.replace(':jenis_pengangkutan', jenis_pengangkutan)
          url     = url.replace(':jenis_pengiriman', jenis_pengiriman)

            $.get(url , function(data, status){
                var element = JSON.parse(data)
                $('#total').text("*Terdapat "+ element.total_pengiriman + " Pengiriman")
                if (element.satuan == 'Kilo')
                {
                  $('#ukuran').text("*Total "+ element.berat_total + element.satuan)
                } else if (element.satuan == 'Unit')
                {
                  $('#ukuran').text("*Total "+ element.unit_total + element.satuan)
                } else {
                  $('#ukuran').text("*Total "+ element.dimensi_total + element.satuan)
                }
                $('#berat').val(element.berat_total)
                $('#unit').val(element.unit_total)
                $('#dimensi').val(element.dimensi_total)
                $('#btn-store').prop("disabled", false)

            })

        })

      })
      </script>
@endsection