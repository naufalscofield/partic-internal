<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Partic &mdash; Login</title>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1">
			<!--===============================================================================================-->	
			<link rel="icon" type="image/png" href="/img/partic-logo.ico"/>
			<!--===============================================================================================-->
			<link rel="stylesheet" type="text/css" href="/login_bootstrap/vendor/bootstrap/css/bootstrap.min.css">
			<!--===============================================================================================-->
			<link rel="stylesheet" type="text/css" href="login_bootstrap/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
			<!--===============================================================================================-->
			<link rel="stylesheet" type="text/css" href="login_bootstrap/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
			<!--===============================================================================================-->
			<link rel="stylesheet" type="text/css" href="/login_bootstrap/vendor/animate/animate.css">
			<!--===============================================================================================-->	
			<link rel="stylesheet" type="text/css" href="/login_bootstrap/vendor/css-hamburgers/hamburgers.min.css">
			<!--===============================================================================================-->
			<link rel="stylesheet" type="text/css" href="/login_bootstrap/vendor/animsition/css/animsition.min.css">
			<!--===============================================================================================-->
			<link rel="stylesheet" type="text/css" href="/login_bootstrap/vendor/select2/select2.min.css">
			<!--===============================================================================================-->	
			<link rel="stylesheet" type="text/css" href="/login_bootstrap/vendor/daterangepicker/daterangepicker.css">
			<!--===============================================================================================-->
			<link rel="stylesheet" type="text/css" href="/login_bootstrap/css/util.css">
			<link rel="stylesheet" type="text/css" href="/login_bootstrap/css/main.css">
			<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
			<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet" />
			<link rel="stylesheet" href="/ladda/dist/ladda-themeless.min.css">
			
			{{-- <link rel="stylesheet" href="sweetalert2.min.css"> --}}
			<!--===============================================================================================-->
	</head>

	<body style="background-color: #666666;">

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
			<form class="login100-form validate-form" id="form_login" method="post" action="javascript:void(0)">
					@csrf
					{{ csrf_field() }}
					<span class="login100-form-title p-b-43">
						Login
					</span>
					
					<div class="wrap-input100 validate-input" data-validate = "Perusahaan is required">
						<br>
						<span class="focus-input100"></span>
						<span class="label-input100">Perusahaan</span><br>
						<select class="js-example-basic-single input100 id_perusahaan" id="id_perusahaan" required name="id_perusahaan">
						</select>
					</div>
					
					
					<div class="wrap-input100 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" type="text" name="email" id="email">
						<span class="focus-input100"></span>
						<span class="label-input100">Email</span>
					</div>
					
					
					
					<div class="wrap-input100 validate-input" data-validate="Password is required">
						<input class="input100" type="password" name="password" id="password">
						<span class="focus-input100"></span>
						<span class="label-input100">Password</span>
					</div>

					<div class="flex-sb-m w-full p-t-3 p-b-32">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Ingat Saya
							</label>
						</div>

						<div>
							<a href="#" class="txt1">
								Lupa Password?
							</a>
						</div>
					</div>
			

					<div class="container-login100-form-btn">
						<button class="login100-form-btn ladda-button" data-style="zoom-out" id="submit" type="submit">
							<span class="ladda-label">LOGIN</span>
						</button>
					</div>
					
					<div class="text-center p-t-46 p-b-20">
						<a href="{{'/'}}">
							<span class="txt2">
								<h5>Kembali</h5>
							</span>
						</a>
					</div>

				</form>

				<div class="login100-more" style="background-image: url('/login_bootstrap/images/bg-01.jpg');">
				</div>
			</div>
		</div>
	</div>
			
	<!--===============================================================================================-->
	<script src="/login_bootstrap/vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	<!--===============================================================================================-->
	<script src="/login_bootstrap/vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="/login_bootstrap/vendor/bootstrap/js/popper.js"></script>
	<script src="/login_bootstrap/vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="/login_bootstrap/vendor/select2/select2.min.js"></script>
	<!--===============================================================================================-->
	<script src="/login_bootstrap/vendor/daterangepicker/moment.min.js"></script>
	<script src="/login_bootstrap/vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="/login_bootstrap/vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="login_bootstrap/js/main.js"></script>

	<script src="/ladda/dist/spin.min.js"></script>
	<script src="/ladda/dist/ladda.min.js"></script>

	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	{{-- <script src="sweetalert2.all.min.js"></script>
	<script src="sweetalert2.min.js"></script> --}}

	<script>
		$(document).ready(function(){

			$('#submit').click(function(e){
			if ( ($('#email').val() == '') || ($('#id_perusahaan').val() == '') || ($('#password').val() == '') )
			{
				// toastr.options = {
				// "closeButton": true,
				// "debug": false,
				// "newestOnTop": false,
				// "progressBar": false,
				// "positionClass": "toast-top-full-width",
				// "preventDuplicates": false,
				// "onclick": null,
				// "showDuration": "300",
				// "hideDuration": "1000",
				// "timeOut": "5000",
				// "extendedTimeOut": "1000",
				// "showEasing": "swing",
				// "hideEasing": "linear",
				// "showMethod": "fadeIn",
				// "hideMethod": "fadeOut"
				// }
				// Command: toastr["error"]("Harap isi dan lengkapi form", "Form tidak lengkap")

			} else
			{
				var l = Ladda.create(this);
				l.start();
				e.preventDefault();
				/*Ajax Request Header setup*/
				$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});

				/* Submit form data using ajax*/
				$.ajax({
					url: "{{ route('aksi_login')}}",
					method: 'post',
					data: $('#form_login').serialize(),
					success: function(response){
						//------------------------
							// var resp = JSON.parse(response)
							console.log(response.resp.code)
							l.stop()

							if (response.resp.code == 401)
							{
								Swal.fire({
								title: 'Kesalahan!',
								text: response.resp.message,
								icon: 'error',
								confirmButtonText: 'OK'
								})
							} else 
							{
								Swal.fire({
								title: 'Selamat datang!',
								icon: 'success',
								})
								setTimeout(function() {
									if (response.role == 'ceo')
									{
										window.location.href = "{{ route('dashboard') }}";
									}
									else if (response.role == 'regional')
									{
										window.location.href = "{{ route('regional.dashboard') }}";
									}
									else if (response.role == 'cabang')
									{
										window.location.href = "{{ route('cabang.dashboard') }}";
									}
								}, 1600);
							}
						//--------------------------
				}});
			}
			
			});

			$('.id_perusahaan').select2({
				placeholder: 'Cari...',
				ajax: {
				url: '/get-perusahaan',
				dataType: 'json',
				delay: 250,
				processResults: function (data) {
					return {
					results:  $.map(data, function (item) {
						return {
						text: item.nama_perusahaan,
						id: item.id
						}
					})
					};
				},
				cache: true
				}
			});
		
		});
	</script>

	</body>
</html>