<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Partic Users</title>

    <!-- Custom fonts for this template-->
    <link href="{{URL::to('/')}}/users/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
 	{{-- <link rel="icon" type="image/png" href="{{URL::to('/')}}/img/partic-logo.ico"/> --}}
    <link rel="stylesheet" href="{{URL::to('/')}}/fa/css/font-awesome.min.css">
    <!-- Custom styles for this template-->
    <link href="{{URL::to('/')}}/users/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
  	<link rel="icon" type="image/png" href="{{URL::to('/')}}/img/partic-logo.ico"/>
    
    <script
    src="https://code.jquery.com/jquery-3.5.1.min.js"
    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
    crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    @if(Session::has('message'))
    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
    @endif

</head>
@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
    </div>
@endif
@if ($message = Session::get('info'))
    <div class="alert alert-info alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
    </div>
@endif
@if ($message = Session::get('warning'))
    <div class="alert alert-warning alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
    </div>
@endif
@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
        <strong>{{ $message }}</strong>
    </div>
@endif
<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="">
        <img src="{{URL::to('/')}}/img/partic-logo.png" style="width:45px;height:50px" alt=""> &nbsp &nbsp | &nbsp &nbsp <img src="{{config('app.storage')}}/logo/{{Session::get('logo')}}" style="width:45px;height:50px" alt="">
        <div id="title" class="sidebar-brand-text mx-3"></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      @if (Request::segment(1) == 'cabang-dashboard')
        <li class="nav-item active">
      @else
        <li class="nav-item">
      @endif
          <a class="nav-link" href="{{ route('dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
        </li>

       <!-- Heading -->
       <div class="sidebar-heading">
        Main Content
      </div>
      <!-- Nav Item - Tables -->
      @if ( (Request::segment(1) == 'regional') || (Request::segment(1) == 'pic-regional'))
        <li class="nav-item active">        
      @else
        <li class="nav-item">
      @endif
      <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
        <i class="fa fa-building"></i>
        <span>Regional</span>
      </a>
        <div id="collapseOne" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="{{ route('regional') }}">Kantor Regional</a>
            <a class="collapse-item" href="{{route('pic_regional')}}">PIC Regional</a>
          </div>
        </div>
        </li>

      @if (Request::segment(1) == 'cabang')
        <li class="nav-item active">        
      @else
        <li class="nav-item">
      @endif
        <a class="nav-link" href="{{route('cabang')}}">
            <i class="fa fa-home" aria-hidden="true"></i>
            <span>Cabang</span></a>
        </li>
      
      @if (Request::segment(1) == 'tarif_pelanggan')
        <li class="nav-item active">        
      @else
        <li class="nav-item">
      @endif
        <a class="nav-link" href="{{route('tarif_pelanggan')}}">
            <i class="fa fa-money-bill-wave" aria-hidden="true"></i>
            <span>Tarif Pelanggan</span></a>
        </li>
      
      @if (Request::segment(1) == 'cabang-pengiriman')
        <li class="nav-item active">        
      @else
        <li class="nav-item">
      @endif
        <a class="nav-link" href="{{route('cabang.pengiriman')}}">
            <i class="fa fa-boxes" aria-hidden="true"></i>
            <span>Pengiriman</span></a>
        </li>
      
      <!-- Divider -->

      <!-- Heading -->

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

        <!-- Main Content -->
        <div id="content">
          
            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                  <a href="" id="back" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i></a>
              <!-- Sidebar Toggle (Topbar) -->
              <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                <i class="fa fa-bars"></i>
              </button>
    
              <!-- Topbar Navbar -->
              <ul class="navbar-nav ml-auto">
    
                <!-- Nav Item - Search Dropdown (Visible Only XS) -->
                <li class="nav-item dropdown no-arrow d-sm-none">
                  <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-search fa-fw"></i>
                  </a>
                  <!-- Dropdown - Messages -->
                  <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                    <form class="form-inline mr-auto w-100 navbar-search">
                      <div class="input-group">
                        <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="button">
                            <i class="fas fa-search fa-sm"></i>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </li>
    
                <div class="topbar-divider d-none d-sm-block"></div>
    
                <!-- Nav Item - User Information -->

                <li class="nav-item dropdown no-arrow">
                  <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Session::get('nama_lengkap') }}</span>
                    @if (Session::get('foto_diri') == null || Session::get('foto_diri') == '')
                      <i class="fa fa-user fa-2x" aria-hidden="true"></i>
                    @else
                      <img class="img-profile rounded-circle" src="{{config('app.storage')}}foto_diri/{{Session::get('foto_diri')}}" alt="">
                    @endif  
                  </a>
                  <!-- Dropdown - User Information -->
                  <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="{{route('profile')}}">
                      <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                      Profile
                    </a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{route('logout')}}">
                      <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                      Logout
                    </a>
                  </div>
                </li>
    
              </ul>
    
            </nav>
  <!-- End of Topbar -->

@yield('content')

<!-- Footer -->
<footer class="sticky-footer bg-white">
    <div class="container my-auto">
      <div class="copyright text-center my-auto">
        <span>Copyright &copy; Partic <span id="foot"></span> 2020</span>
      </div>
    </div>
  </footer>
  <!-- End of Footer -->

</div>
<!-- End of Content Wrapper -->

</div>
<!-- End of Page Wrapper -->

<!-- Scroll to Top Button-->
<a class="scroll-to-top rounded" href="{{URL::to('/')}}/users/#page-top">
<i class="fas fa-angle-up"></i>
</a>

<!-- Logout Modal-->
<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
  <div class="modal-content">
    <div class="modal-header">
      <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
      <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
      </button>
    </div>
    <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
    <div class="modal-footer">
      <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
      <a class="btn btn-primary" href="{{URL::to('/')}}/users/login.html">Logout</a>
    </div>
  </div>
</div>
</div>

<!-- Bootstrap core JavaScript-->
{{-- <script src="/users/vendor/jquery/jquery.min.js"></script> --}}
<script src="/users/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="/users/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="/users/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="/users/vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="/users/js/demo/chart-area-demo.js"></script>
<script src="/users/js/demo/chart-pie-demo.js"></script>
{{-- <script src="https://code.jquery.com/jquery-3.5.1.js"></script> --}}
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script>
  $(document).ready(function(){
      $('#dataTable').DataTable();

      $(document).on('click', '#back', function() {
        parent.history.back();
		    return false;
      })


  })
</script>

</body>

</html>
