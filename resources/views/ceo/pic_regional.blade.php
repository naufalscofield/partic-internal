@extends('component.master_'.Session::get('role'))

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">PIC / Pegawai Regional</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAdd">PIC / Pegawai Regional Baru</button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered datatable mdl-data-table display nowrap" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>Email</center></th>
                    <th><center>Nama Lengkap</center></th>
                    <th><center>TTL</center></th>
                    <th><center>No Telp</center></th>
                    <th><center>Regional</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="modal" tabindex="-1" id="modalAdd" role="dialog">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">PIC / Pegawai Regional Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{route('store_pic_regional')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="id_regional" name="id_regional"><br>
                    <option value="">--Pilih Regional--</option>
                  </select><br>

                  <label for="">Email</label><br>
                  <input required class="form-control js-example-basic-single" style="width:100%" type="email" id="email" name="email"><br>
                  
                  <label for="">Default Password</label><br>
                  <input required class="form-control js-example-basic-single" style="width:100%" type="password" id="password" name="password"><br>
                  
                  <label for="">Nama Lengkap</label><br>
                  <input required class="form-control js-example-basic-single" style="width:100%" type="text" id="nama_lengkap" name="nama_lengkap"><br>
                  
                  <label for="">Tanggal Lahir</label><br>
                  <input required class="form-control js-example-basic-single" style="width:100%" type="date" id="tanggal_lahir" name="tanggal_lahir"><br>
                  
                  <label for="">Tempat Lahir</label><br>
                  <input required class="form-control js-example-basic-single" style="width:100%" type="text" id="tempat_lahir" name="tempat_lahir"><br>
                  
                  <label for="">Alamat</label><br>
                  <textarea required class="form-control js-example-basic-single" style="width:100%" type="text" id="alamat" name="alamat"></textarea><br>
                  
                  <label for="">No Telp</label><br>
                  <input required class="form-control js-example-basic-single" style="width:100%" type="number" id="no_telp" name="no_telp"><br>
                  
                  <label for="">NIK</label><br>
                  <input required class="form-control js-example-basic-single" style="width:100%" type="number" id="nik" name="nik"><br>
                  
              </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="modal" tabindex="-1" id="modalEdit" role="dialog">
          <div class="modal-dialog modal-md" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">PIC Regional</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="" method="POST" enctype="multipart/form-data">
                  @csrf
                  <b><label for="">Regional</label></b>
                  <p id="regional_edit"></p><br>
                  
                  <b><label for="">Email</label></b>
                  <p id="email_edit"></p><br>
                  
                  <b><label for="">Nama Lengkap</label></b>
                  <p id="nama_lengkap_edit"></p><br>
                  
                  <b><label for="">Tempat Tanggal Lahir</label></b>
                  <p id="ttl_edit"></p><br>
                  
                  <b><label for="">Alamat</label></b>
                  <p id="alamat_edit"></p><br>
                  
                  <b><label for="">No Telp</label></b>
                  <p id="no_telp_edit"></p><br>
                  
                  <b><label for="">NIK</label></b>
                  <p id="nik_edit"></p><br>
                  
                  <center>
                    <table border="1" >
                      <tr>
                        <th><center>Foto Diri</center></th>
                        <th><center>Foto NPWP</center></th>
                        <th><center>Foto KTP</center></th>
                      </tr>
                      <tr>
                        <td width="200px"><center><img src="" style="width:70px;height:70px" id="foto_diri_edit" alt=""></center></td>
                        <td width="200px"><center><img src="" style="width:70px;height:70px" id="foto_npwp_edit" alt=""></center></td>
                        <td width="200px"><center><img src="" style="width:70px;height:70px" id="foto_ktp_edit" alt=""></center></td>
                      </tr>
                    </table>
                  </center>
                  
                  
                </div>
                {{-- <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div> --}}
              </form>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){

          $('.datatable').DataTable({
            "scrollX" : true,
            processing: true,
            serverSide: true,
            ajax: '{{ route('get_pic_regional') }}',
            "columns": [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'email', name: 'email'},
              {data: 'nama_lengkap', name: 'nama_lengkap'},
              {data: 'ttl', name: 'ttl'},
              {data: 'no_telp', name: 'no_telp'},
              {data: 'regional', name: 'regional'},
              {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
          ],
          'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ]
        });

        
        $.get("{{route('get_regional')}}", function(data, status){
            $.each(data.data, function( index, value ) {
              $('<option>').val(value.id).text(value.kota).attr('data-id', value.id).appendTo('#id_regional');
          });
        });


        $(document).on('click', '#btn_edit', function() {
          
          var id  = $(this).data("id");
          var url = '{{ route('show_pic_regional', ":id") }}'
          url     = url.replace(':id', id)

          $.get(url , function(data, status){
              let element = JSON.parse(data)
              // console.log(element)
              // console.log(element['DataUser'].email)
              $('#modalEdit').modal('show')
              // $('#id').val(element.id)
              $('#regional_edit').text(element['DataRegional'].kota)
              $('#email_edit').text(element['DataUser'].email)
              $('#nama_lengkap_edit').text(element['DataBiodata'].nama_lengkap)
              $('#ttl_edit').text(element['DataBiodata'].tempat_lahir+', '+element['DataBiodata'].tanggal_lahir)
              $('#alamat_edit').text(element['DataBiodata'].alamat)
              $('#no_telp_edit').text(element['DataBiodata'].no_telp)
              $('#nik_edit').text(element['DataBiodata'].nik)
              $('#foto_diri_edit').attr('src', `{{config('app.storage')}}foto_diri/${element['DataBiodata'].foto_diri}`)
              $('#foto_npwp_edit').attr('src', `{{config('app.storage')}}foto_npwp/${element['DataBiodata'].foto_npwp}`)
              $('#foto_ktp_edit').attr('src', `{{config('app.storage')}}foto_ktp/${element['DataBiodata'].foto_ktp}`)
          })

        })

        $('#provinsi_edit').change(function(){
          $('#kota_edit').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_edit');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_edit');
            });
          });
        })

        })
      </script>
@endsection