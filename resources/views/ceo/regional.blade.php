@extends('component.master_'.Session::get('role'))

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Regional</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <button class="btn btn-primary" data-toggle="modal" data-target="#modalAdd">Regional Baru</button>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered datatable mdl-data-table" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>Provinsi</center></th>
                    <th><center>Kota</center></th>
                    <th><center>Alamat</center></th>
                    <th><center>Aksi</center></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="modal" tabindex="-1" id="modalAdd" role="dialog">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Regional Baru</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{route('store_regional')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" id="token_raja">
                  <label for="">Provinsi</label><br>
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="provinsi" name="provinsi"><br>
                    <option value="">--Pilih Provinsi--</option>
                  </select><br>
                
                  <label for="">Kota</label>
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="kota" name="kota">
                    <option value="">--Pilih Kota--</option>
                  </select>

                  <label for="">Alamat</label>
                  <textarea rows="10" cols="10" required class="form-control" type="text" id="alamat" name="alamat"></textarea><br>

                  <label for="">Email</label>
                  <input required type="email" name="email" class="form-control"><br>

                  <label for="">No Telp</label>
                  <input required type="number" name="no_telp" class="form-control"><br>
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>

        <div class="modal" tabindex="-1" id="modalEdit" role="dialog">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Edit Regional</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form action="{{route('update_regional')}}" method="POST" enctype="multipart/form-data">
                  @csrf
                  <input type="hidden" name="id" id="id">
                  <label for="">Provinsi</label><br>
                  <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="provinsi_edit" name="provinsi_edit"><br>
                    <option value="">--Pilih Provinsi--</option>
                  </select><br>
                
                <label for="">Kota</label>
                <select required class="form-control js-example-basic-single" style="width:100%" type="text" id="kota_edit" name="kota_edit">
                  <option value="">--Pilih Kota--</option>
                </select>

                  <label for="">Alamat</label>
                  <textarea rows="10" cols="10" required class="form-control" type="text" id="alamat_edit" name="alamat_edit"></textarea><br>
                  
                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){

          $('#provinsi').select2();
          $('#kota').select2();
          $('#provinsi_edit').select2();
          $('#kota_edit').select2();

          $('.datatable').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('get_regional') }}',
            "columns": [
              {data: 'DT_RowIndex', name: 'DT_RowIndex'},
              {data: 'provinsi', name: 'provinsi'},
              {data: 'kota', name: 'kota'},
              {data: 'alamat', name: 'alamat'},
              {data: 'aksi', name: 'aksi', orderable: false, searchable: false}
          ],
          'columnDefs': [
            {
                "targets": "_all", // your case first column
                "className": "text-center",
            }
          ]
        });

        $.get("https://x.rajaapi.com/poe", function(data, status){
          const tokenraja = data.token
          $('#token_raja').val(tokenraja)

          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/provinsi", function(dataProv, statusProv){
              // console.log(dataProv.data)
              $.each(dataProv.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi');
            });
          });

        });

        $('#provinsi').change(function(){
          $('#kota').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota');
            });
          });
        })

        $(document).on('click', '#btn_edit', function() {
          

          var id  = $(this).data("id");
          var url = '{{ route('show_regional', ":id") }}'
          url     = url.replace(':id', id)

          $.get(url , function(data, status){
            let element = JSON.parse(data)
            console.log(element)
            $('#modalEdit').modal('show')
            $('#id').val(element.id)
            $('#provinsi_edit').val(element.provinsi)
            $('#kota_edit').val(element.kota)
            $('#alamat_edit').val(element.alamat)
          })

          var tokenraja = $('#token_raja').val()

          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/provinsi", function(dataProv, statusProv){
              $.each(dataProv.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#provinsi_edit');
            });
          });
        })

        $('#provinsi_edit').change(function(){
          $('#kota_edit').empty()
          var id = $(this).find(':selected').attr('data-id')
          console.log(id)
          var tokenraja = $('#token_raja').val()
          $.get("https://x.rajaapi.com/MeP7c5ne"+tokenraja+"/m/wilayah/kabupaten?idpropinsi="+id, function(dataKota, statusKota){
              console.log(dataKota.data)
                $('<option>').val("").text("--Pilih Kota--").appendTo('#kota_edit');
              $.each(dataKota.data, function( index, value ) {
                $('<option>').val(value.name).text(value.name).attr('data-id', value.id).appendTo('#kota_edit');
            });
          });
        })
          // var tokenraja = $('#token_raja').val()

       

          // function readURLEdit(input) {
          //   if (input.files && input.files[0]) {
          //     var reader = new FileReader();
              
          //     reader.onload = function(e) {
          //       $('#gambar_edit_preview').attr('src', e.target.result);
          //     }
              
          //     reader.readAsDataURL(input.files[0]); // convert to base64 string
          //   }
          // }

          // $("#gambar_edit").change(function() {
          //   readURLEdit(this);
          // });

          // function readURL(input) {
          //   if (input.files && input.files[0]) {
          //     var reader = new FileReader();
              
          //     reader.onload = function(e) {
          //       $('#gambar_preview').attr('src', e.target.result);
          //     }
              
          //     reader.readAsDataURL(input.files[0]); // convert to base64 string
          //   }
          // }

          // $("#gambar").change(function() {
          //   readURL(this);
          // });

        })
      </script>
@endsection