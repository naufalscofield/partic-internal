@extends('component.master_'.Session::get('role'))

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

      <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Cabang</h1>

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            {{-- <button class="btn btn-primary" data-toggle="modal" data-target="#modalAdd">Cabang Baru</button> --}}
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered datatable mdl-data-table" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th><center>No</center></th>
                    <th><center>Kota</center></th>
                    <th><center>Nama Cabang</center></th>
                    <th><center>Alamat</center></th>
                    <th><center>No Telp</center></th>
                    {{-- <th><center>Aksi</center></th> --}}
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>

      </div>
      <!-- /.container-fluid -->
      <script>
        $(document).ready(function(){

          $('.datatable').DataTable({
              processing: true,
              serverSide: true,
              ajax: '{{route('get_cabang')}}',
              "columns": [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'kota', name: 'kota'},
                {data: 'nama_cabang', name: 'nama_cabang'},
                {data: 'alamat_cabang', name: 'alamat_cabang'},
                {data: 'no_telp', name: 'no_telp'},
            ],
            'columnDefs': [
              {
                  "targets": "_all", // your case first column
                  "className": "text-center",
              }
            ]
          });

        
          

        })
      </script>
@endsection