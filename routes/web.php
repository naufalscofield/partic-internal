<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route Umum
Route::get('/', 'HomeController@index');
Route::get('/aktivasi', 'PerusahaanController@aktivasi')->name('aktivasi');
Route::get('/kirim-verifikasi', 'HomeController@kirimVerifikasi')->name('kirim-verifikasi');
Route::get('/daftar', 'HomeController@daftar')->name('daftar');
Route::get('/tentang', 'HomeController@tentang')->name('tentang');
Route::get('/kontak', 'HomeController@kontak')->name('kontak');
Route::get('/tracking', 'HomeController@tracking')->name('tracking');

//Route Registrasi
Route::post('/registrasi-perusahaan', 'PerusahaanController@registrasi')->name('registrasi_perusahaan');
Route::get('/get-perusahaan', 'PerusahaanController@get')->name('get_perusahaan');
Route::get('/sudah-registrasi', function () {
    return view('home.after_registrasi');
});

//Route Login
Route::group(['middleware' => 'usersessionexist'], function () {
    Route::get('/login', 'LoginController@index')->name('login');
});
Route::post('/aksi-login', 'LoginController@login')->name('aksi_login');

//Route Semua Role
// Route::group(['middleware' =>'usersession', 'revalidate'], function () {
Route::group(['middleware' =>'usersession', 'revalidate'], function () {
    Route::get('/access_denied', function () {
        return view('home.access_denied');
    })->name('access_denied');    
    Route::get('/profile', 'ProfileController@profile')->name('profile');
    Route::post('/update-profile', 'ProfileController@updateProfile')->name('update_profile');
    Route::post('/change-password', 'ProfileController@changePassword')->name('change_password');
    Route::get('/logout', 'LoginController@logout')->name('logout');
});

//Route CEO
Route::group(['middleware' => 'role:ceo', 'usersession', 'revalidate'], function () {
    // Route::get('/profile', 'ProfileController@profile')->name('profile');
    // Route::post('/update-profile', 'ProfileController@updateProfile')->name('update_profile');
    // Route::post('/change-password', 'ProfileController@changePassword')->name('change_password'); 

    Route::get('/dashboard', 'CeoController@index')->name('dashboard');
    
    Route::get('/regional', 'CeoController@regional')->name('regional');
    Route::get('/get-regional', 'CeoController@getRegional')->name('get_regional');
    Route::get('/show-regional/{id}', 'CeoController@showRegional')->name('show_regional');
    Route::get('/delete-regional/{id}', 'CeoController@deleteRegional')->name('delete_regional');
    Route::post('/store-regional', 'CeoController@storeRegional')->name('store_regional');
    Route::post('/update-regional', 'CeoController@updateRegional')->name('update_regional');

    Route::get('/pic-regional', 'CeoController@picRegional')->name('pic_regional');
    Route::get('/get-pic-regional', 'CeoController@getPicRegional')->name('get_pic_regional');
    Route::get('/show-pic-regional/{id}', 'CeoController@showPicRegional')->name('show_pic_regional');
    Route::get('/delete-pic-regional/{id}', 'CeoController@deletePicRegional')->name('delete_pic_regional');
    Route::post('/store-pic-regional', 'CeoController@storePicRegional')->name('store_pic_regional');

    Route::get('/cabang', 'CeoController@Cabang')->name('cabang');
    Route::get('/get-cabang', 'CeoController@getCabang')->name('get_cabang');
    
    Route::get('/tarif_pelanggan', 'CeoController@TarifPelanggan')->name('tarif_pelanggan');
    Route::get('/get-tarif_pelanggan', 'CeoController@getTarifPelanggan')->name('get_tarif_pelanggan');
    Route::get('/show-tarif_pelanggan/{id}', 'CeoController@showTarifPelanggan')->name('show_tarif_pelanggan');
    Route::get('/delete-tarif_pelanggan/{id}', 'CeoController@deleteTarifPelanggan')->name('delete_tarif_pelanggan');
    Route::post('/store-tarif_pelanggan', 'CeoController@storeTarifPelanggan')->name('store_tarif_pelanggan');
    Route::post('/update-tarif_pelanggan', 'CeoController@updateTarifPelanggan')->name('update_tarif_pelanggan');

    Route::get('/pengiriman', 'CeoController@pengiriman')->name('pengiriman');
    Route::get('/get-pengiriman', 'CeoController@getPengiriman')->name('get_pengiriman');
    Route::get('/show-pengiriman/{id}', 'CeoController@showPengiriman')->name('show_pengiriman');
});

Route::group(['middleware' => 'role:regional', 'usersession', 'revalidate'], function () {
    // Route::get('/profile', 'ProfileController@profile')->name('profile');
    // Route::post('/update-profile', 'ProfileController@updateProfile')->name('update_profile');
    // Route::post('/change-password', 'ProfileController@changePassword')->name('change_password'); 
    
    Route::get('/regional-dashboard', 'RegionalController@index')->name('regional.dashboard');

    Route::get('/regional-cabang', 'RegionalController@cabang')->name('regional.cabang');
    Route::get('/regional-get-cabang', 'RegionalController@getCabang')->name('regional.get_cabang');
    Route::get('/regional-get-cabang-by-regional/{idregional}', 'RegionalController@getCabangByIdRegional')->name('regional.get_cabang_by_id_regional');
    Route::get('/regional-show-cabang/{id}', 'RegionalController@showCabang')->name('regional.show_cabang');
    Route::get('/regional-delete-cabang/{id}', 'RegionalController@deleteCabang')->name('regional.delete_cabang');
    Route::post('/regional-store-cabang', 'RegionalController@storeCabang')->name('regional.store_cabang');
    Route::post('/regional-update-cabang', 'RegionalController@updateCabang')->name('regional.update_cabang');

    Route::get('/regional-pic-cabang', 'RegionalController@picCabang')->name('regional.pic_cabang');
    Route::get('/regional-get-pic-cabang', 'RegionalController@getPicCabang')->name('regional.get_pic_cabang');
    Route::get('/regional-show-pic-cabang/{id}', 'RegionalController@showPicCabang')->name('regional.show_pic_cabang');
    Route::get('/regional-delete-pic-cabang/{id}', 'RegionalController@deletePicCabang')->name('regional.delete_pic_cabang');
    Route::post('/regional-store-pic-cabang', 'RegionalController@storePicCabang')->name('regional.store_pic_cabang');
    
    Route::get('/regional-get-regional', 'RegionalController@getRegional')->name('regional.get_regional');
    
    Route::get('/regional-tarif-pelanggan', 'RegionalController@tarifPelanggan')->name('regional.tarif_pelanggan');
    Route::get('/regional-get-tarif-pelanggan', 'RegionalController@getTarifPelanggan')->name('regional.get_tarif_pelanggan');
    Route::get('/regional-show-tarif-pelanggan/{idTarif}', 'RegionalController@showTarifPelanggan')->name('regional.show_tarif_pelanggan');
    Route::post('/regional-store-tarif-pelanggan', 'RegionalController@storeTarifPelanggan')->name('regional.store_tarif_pelanggan');
    Route::post('/regional-update-tarif-pelanggan', 'RegionalController@updateTarifPelanggan')->name('regional.update_tarif_pelanggan');
    Route::get('/regional-delete-tarif-pelanggan/{idTarif}', 'RegionalController@deleteTarifPelanggan')->name('regional.delete_tarif_pelanggan');
    
    Route::get('/regional-mitra-kurir', 'RegionalController@mitraKurir')->name('regional.mitra_kurir');
    Route::get('/regional-get-mitra-kurir', 'RegionalController@getMitraKurir')->name('regional.get_mitra_kurir');
    Route::get('/regional-show-mitra-kurir/{idMitra}', 'RegionalController@showMitraKurir')->name('regional.show_mitra_kurir');
    
    Route::get('/regional-tarif-line', 'RegionalController@tarifLine')->name('regional.tarif_line');
    Route::get('/regional-get-tarif-line', 'RegionalController@getTarifLine')->name('regional.get_tarif_line');
    Route::get('/regional-show-tarif-line/{idTarif}', 'RegionalController@showTarifLine')->name('regional.show_tarif_line');
    Route::post('/regional-store-tarif-line', 'RegionalController@storeTarifLine')->name('regional.store_tarif_line');
    Route::post('/regional-update-tarif-line', 'RegionalController@updateTarifLine')->name('regional.update_tarif_line');
    Route::get('/regional-delete-tarif-line/{idTarif}', 'RegionalController@deleteTarifLine')->name('regional.delete_tarif_line');
    
    Route::get('/regional-pengajuan-tarif', 'RegionalController@pengajuanTarif')->name('regional.pengajuan_tarif');
    Route::get('/regional-get-pengajuan-tarif', 'RegionalController@getPengajuanTarif')->name('regional.get_pengajuan_tarif');
    Route::post('/regional-update-pengajuan-tarif/{status}', 'RegionalController@updatePengajuanTarif')->name('regional.update_pengajuan_tarif');
    
    Route::get('/regional-member', 'RegionalController@member')->name('regional.member');
    Route::get('/regional-get-member/{no}', 'RegionalController@getMember')->name('regional.get_member');
    Route::get('/regional-verifikasi-member/{id}', 'RegionalController@verifikasiMember')->name('regional.verifikasi_member');
    
    Route::get('/regional-profil-kantor', 'RegionalController@profilKantor')->name('regional.profil_kantor');
    Route::post('/regional-update-profil-kantor', 'RegionalController@updateProfilKantor')->name('regional.update_profil_kantor');
    
    Route::get('/regional-pengiriman', 'RegionalController@pengiriman')->name('regional.pengiriman');
    Route::get('/regional-get-pengiriman', 'RegionalController@getPengiriman')->name('regional.get_pengiriman');
    Route::get('/regional-show-pengiriman/{id}', 'RegionalController@showPengiriman')->name('regional.show_pengiriman');
    
    Route::get('/regional-kelompok-pengiriman', 'RegionalController@kelompokPengiriman')->name('regional.kelompok_pengiriman');
    Route::get('/regional-get-kelompok-pengiriman', 'RegionalController@getKelompokPengiriman')->name('regional.get_kelompok_pengiriman');
    Route::get('/regional-get-field-kelompok-pengiriman', 'RegionalController@getFieldKelompokPengiriman')->name('regional.get_field_kelompok_pengiriman');
    Route::get('/regional-get-field2-kelompok-pengiriman/{tujuan}', 'RegionalController@getField2KelompokPengiriman')->name('regional.get_field2_kelompok_pengiriman');
    Route::get('/regional-get-field3-kelompok-pengiriman/{tujuan}/{jenis_pengangkutan}', 'RegionalController@getField3KelompokPengiriman')->name('regional.get_field3_kelompok_pengiriman');
    Route::get('/regional-get-field4-kelompok-pengiriman/{tujuan}/{jenis_pengangkutan}/{jenis_pengiriman}', 'RegionalController@getField4KelompokPengiriman')->name('regional.get_field4_kelompok_pengiriman');
    Route::post('/regional-store-kelompok-pengiriman', 'RegionalController@storeKelompokPengiriman')->name('regional.store_kelompok_pengiriman');
    Route::get('/regional-promethee/{id}', 'RegionalController@promethee')->name('regional.promethee');
    Route::post('/regional-store-promethee', 'RegionalController@storePromethee')->name('regional.store_promethee');
    Route::post('/regional-store-smart', 'RegionalController@storeSmart')->name('regional.store_smart');
    Route::post('/regional-store-smart-final', 'RegionalController@storeSmartFinal')->name('regional.store_smart_final');

});

Route::group(['middleware' => 'role:cabang', 'usersession', 'revalidate'], function () {
    Route::get('/cabang-dashboard', 'CabangController@index')->name('cabang.dashboard');

    Route::get('/cabang-pengiriman', 'CabangController@pengiriman')->name('cabang.pengiriman');
    Route::get('/cabang-get-pengiriman', 'CabangController@getPengiriman')->name('cabang.get_pengiriman');
    Route::get('/cabang-show-pengiriman/{id}', 'CabangController@showPengiriman')->name('cabang.show_pengiriman');
    Route::post('/cabang-get-tarif', 'CabangController@getTarif')->name('cabang.get_tarif');
    Route::post('/cabang-store-pengiriman', 'CabangController@storePengiriman')->name('cabang.store_pengiriman');


    // Route::get('/regional-cabang', 'RegionalController@cabang')->name('regional.cabang');
    // Route::get('/regional-get-regional', 'RegionalController@getRegional')->name('regional.get_regional');
    // Route::get('/regional-show-regional/{id}', 'RegionalController@showRegional')->name('regional.show_regional');
    // Route::get('/regional-delete-regional/{id}', 'RegionalController@deleteRegional')->name('regional.delete_regional');
    // Route::post('/regional-store-regional', 'RegionalController@storeRegional')->name('regional.store_regional');
    // Route::post('/regional-update-regional', 'RegionalController@updateRegional')->name('regional.update_regional');

    // Route::get('/regional-pic-cabang', 'RegionalController@picCabang')->name('regional.pic_cabang');
    // Route::get('/get-pic-regional', 'RegionalController@getPicRegional')->name('get_pic_regional');
    // Route::get('/show-pic-regional/{id}', 'RegionalController@showPicRegional')->name('show_pic_regional');
    // Route::get('/delete-pic-regional/{id}', 'RegionalController@deletePicRegional')->name('delete_pic_regional');
    // Route::post('/store-pic-regional', 'RegionalController@storePicRegional')->name('store_pic_regional');

});
