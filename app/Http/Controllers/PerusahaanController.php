<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Session;

class PerusahaanController extends Controller
{
    public function __construct()
    {
        //
    }

    public function get()
    {
        $response = Curl::to(config('app.gatewayUrl').'perusahaan')
        ->get();

        return response(json_decode($response));
    }
    
    public function registrasi(Request $request)
    {
        $this->validate($request, [
			'foto_diri' => 'required',
			'foto_ktp'  => 'required',
			'foto_npwp' => 'required',
			'logo'      => 'required',
        ]);
        
        $foto_diri      = $request->file('foto_diri');
        $foto_ktp       = $request->file('foto_ktp');
        $foto_npwp      = $request->file('foto_npwp');
        $logo           = $request->file('logo');
        $email_token    = str::random(32);
        $nama_lengkap   = $request->nama_lengkap;
        $email          = $request->email;

        $response = Curl::to(config('app.gatewayUrl').'registrasiPerusahaan')
        ->withData( 
                [
                    'nama_perusahaan'       => $request->nama_perusahaan,
                    'alamat_perusahaan'     => $request->alamat_perusahaan,
                    'email_perusahaan'      => $request->email_perusahaan,
                    'no_telp_perusahaan'    => $request->no_telp_perusahaan,
                    'logo'                  => $logo->getClientOriginalName(),
                    'nama_lengkap'          => $nama_lengkap,
                    'tanggal_lahir'         => $request->tanggal_lahir,
                    'tempat_lahir'          => $request->tempat_lahir,
                    'alamat'                => $request->alamat,
                    'foto_ktp'              => $foto_ktp->getClientOriginalName(),
                    'foto_npwp'             => $foto_npwp->getClientOriginalName(),
                    'nik'                   => $request->nik,
                    'foto_diri'             => $foto_diri->getClientOriginalName(),
                    'no_telp'               => $request->no_telp,
                    'email'                 => $email,
                    'password'              => $request->password,
                    'email_token'           => $email_token
                ]
            )
        ->withFile( 'file_foto_diri', $foto_diri->getRealPath(), 'image/png', $foto_diri )
        ->withFile( 'file_foto_ktp', $foto_ktp->getRealPath(), 'image/png', $foto_ktp )
        ->withFile( 'file_foto_npwp', $foto_npwp->getRealPath(), 'image/png', $foto_npwp )
        ->withFile( 'file_logo', $logo->getRealPath(), 'image/png', $logo )

        ->post();
        
        $response = json_decode($response);

        Mail::send('email.aktivasi', ['nama_lengkap' => $nama_lengkap, 'email_token' => $response->DataUser->email_token, 'id'=> $response->DataUser->id], function ($message) use ($email)
        {
            $message->subject('Aktivasi Email Akun Partic');
            $message->from('ramnaufal@gmail.com', 'Partic');
            $message->to($email);
        });

        return view('home.after_registrasi');

    }

    public function aktivasi(Request $request)
    {
        $id     = $request->get('id');
        $token  = $request->get('et');

        $response = Curl::to(config('app.gatewayUrl').'aktivasiAkun')
        ->withData( 
                [
                    'id'    => $id,
                    'token' => $token
                ]
            )
        ->post();

        $resp = json_decode($response);
        
        $request->session()->flush();
        
        if ($resp->status == 200) 
        {
            return view('home.aktivasi', compact('resp'));
        } else
        {
            return view('home.aktivasi_gagal', compact('resp'));
        }

    }
}
