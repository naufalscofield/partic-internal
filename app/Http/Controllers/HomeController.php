<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Mail\Mailable;
use Illuminate\Support\Facades\Mail;
use Session;
use Ixudra\Curl\Facades\Curl;

class HomeController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index()
    {
        return view('Home/index');
    }
    
    public function daftar()
    {
        return view('home/daftar');
    }

    public function tentang()
    {
        return view('home/tentang');
    }

    public function kontak()
    {
        return view('home/kontak');
    }

    public function tracking()
    {
        return view('home/tracking');
    }

    public function kirimVerifikasi(Request $request)
    {
        $at = $request->get('at');
        $email = Session::get('email');

        $response = Curl::to('localhost:9000/updateTokenEmail/')
        ->withHeader('Authorization: Bearer ' . $at)
        ->withData(
            [
                'id' => Session::get('id')
            ]
        )
        ->post();

        $resp = json_decode($response);
        
        Mail::send('email.aktivasi', ['nama_lengkap' => Session::get('nama_lengkap'), 'email_token' => $resp->email_token, 'id'=> Session::get('id')], function ($message) use ($email)
        {
            $message->subject('Aktivasi Email Akun Partic');
            $message->from('ramnaufal@gmail.com', 'Partic');
            $message->to($email);
        });
    }

}
