<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Session;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $gatewayUrl;
    private $storage;
    private $internalUrl;
    private $externalUrl;
    private $prometheeUrl;
    private $token;

    public function __construct()
    {
        $this->gatewayUrl = "localhost:9000/";
        $this->internalUrl = "localhost:9010/";
        $this->externalUrl = "localhost:9020/";
        $this->prometheeUrl = "localhost:9030/";
        $this->token = $request->session()->get('access_token', '');
        $this->storage = "http://localhost/partic-restapi/LumenApiGateway/storage/app/images/";
    }
}
