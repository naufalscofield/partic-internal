<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Session;
use DataTables;

class CabangController extends Controller
{
    private $access_token;
    private $gatewayUrl;

    public function __construct(Request $request)
    {
        $email_verified = Session::get('email_verified_at');
        $this->access_token = Session::get('access_token');
        
        if ($email_verified == NULL)
        {
            return view('home.tidak_aktif');    
        } else
        {
            return view('cabang.dashboard');
        }
    }

    public function index()
    {
        return view('cabang.dashboard');
    }

    public function pengiriman()
    {
        $regional = Curl::to(config('app.gatewayUrl').'cabang/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;

        $cabang = Curl::to(config('app.gatewayUrl').'cabang/cabang/'.Session::get('id_cabang'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $cabang = json_decode($cabang);
        $nama = $cabang->nama_cabang;
        return view('cabang.pengiriman', compact('nama', 'kota'));
    }

    public function getPengiriman()
    {
        $pengiriman = Curl::to(config('app.gatewayUrl').'cabang/pengiriman/'.Session::get('id_cabang'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();

        $r = json_decode($pengiriman);

        return Datatables::of($r)
            ->addIndexColumn()
            ->addColumn('aksi', function ($r) {
                return '<center>
                        <button id="btn_edit" data-id="'.$r->id.'"class="btn btn-sm btn-success"><i class="fa fa-pencil-alt"></i> Detail</button>
                        </center>';
                })
            ->addColumn('no_resi', function ($r) {
                return $r->no_resi;
            })
            ->addColumn('kota_tujuan', function ($r) {
                return $r->kota_tujuan;
            })
            ->addColumn('jenis_pengiriman', function ($r) {
                if ($r->jenis_pengiriman == 'regular')
                {
                    return '<center>
                        <button class="btn btn-sm btn-warning">REGULAR</button>
                        </center>';
                    } else {
                    
                    return '<center>
                        <button class="btn btn-sm btn-info">EXPRESS</button>
                        </center>';
                }
            })
            ->addColumn('jenis_pengangkutan', function ($r) {
                if ($r->jenis_pengangkutan == 'kilo')
                {
                    return '<center>
                        <button class="btn btn-sm btn-warning">KILO</button>
                        </center>';
                    } else if ($r->jenis_pengangkutan == 'dimensional') {
                    
                    return '<center>
                        <button class="btn btn-sm btn-info">DIMENSIONAL</button>
                        </center>';
                    } else {
                        return '<center>
                            <button class="btn btn-sm btn-success">KOLI</button>
                            </center>';
                }
            })
            ->addColumn('ukuran', function ($r) {
                if ($r->jenis_pengangkutan == 'kilo')
                {
                    return $r->berat.' Kilo';
                    } else if ($r->jenis_pengangkutan == 'dimensional') {
                    
                        return $r->dimensi.' Meter Persegi';

                    } else {
                        return $r->total_unit.' Unit';

                }
            })
            ->rawColumns(['aksi' => 'aksi', 'jenis_pengangkutan' => 'jenis_pengangkutan', 'jenis_pengiriman' => 'jenis_pengiriman'])
            ->make(true);
    }

    public function showPengiriman($id)
    {
        $pengiriman = Curl::to(config('app.gatewayUrl').'cabang/pengiriman/id/'.$id)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        // dd($pengiriman); die;
        return response($pengiriman);
    }

    public function getTarif(Request $request)
    {
        $asal       = str_replace(' ', '-', $request->kotaAsal);
        $tujuan     = str_replace(' ', '-', $request->kotaTujuan);

        $pengiriman = Curl::to(config('app.gatewayUrl').'cabang/tarifPelanggan/'.Session::get('id_perusahaan').'/'.$request->jenisPengangkutan.'/'.$request->jenisPengiriman.'/'.$request->namaBarang.'/'.$asal.'/'.$tujuan)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        // dd($pengiriman);
        return response($pengiriman, 200);
    }

    public function storePengiriman(Request $request)
    {
        if ($request->berat == 0)
        {
            $berat = NULL;
        } else {
            $berat = $request->berat;
        }

        if ($request->dimensi == 0)
        {
            $dimensi = NULL;
        } else {
            $dimensi = $request->dimensi;
        }

        if ($request->total_unit == 0)
        {
            $total_unit = NULL;
        } else {
            $total_unit = $request->total_unit;
        }
        
        if ($request->id_member == 0)
        {
            $id_member = NULL;
        } else {
            $id_member = $request->id_member;
        }

        // dd($dimensi); 

        $date       = date('Y-m-d');
        $tambah     = '+'.$request->durasi.' days';
        $perkiraan  = date('Y-m-d',strtotime($date . $tambah));

        $store = Curl::to(config('app.gatewayUrl').'cabang/pengiriman')
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->withData(
            [
                'no_resi'           => rand(00000000000000, 0000000000001),
                'pin'               => rand(000000, 000001),
                'jenis_pengangkutan'=> $request->jenis_pengangkutan,
                'jenis_barang'      => $request->jenis_barang,    
                'nama_barang'       => $request->nama_barang,    
                'berat'             => $berat,
                'dimensi'           => $dimensi,
                'total_unit'        => $total_unit,
                'jenis_pengiriman'  => $request->jenis_pengiriman,
                'kota_asal'         => $request->kota_asal,    
                'provinsi_tujuan'   => $request->provinsi_tujuan,    
                'kota_tujuan'       => $request->kota_tujuan,    
                'kecamatan_tujuan'  => $request->kecamatan_tujuan,    
                'kelurahan_tujuan'  => $request->kelurahan_tujuan,    
                'kode_pos_tujuan'   => $request->kode_pos_tujuan,    
                'alamat_tujuan'     => $request->alamat_tujuan,    
                'id_member'         => $id_member,
                'nama_pengirim'     => $request->nama_pengirim,    
                'nama_penerima'     => $request->nama_penerima,    
                'no_telp_pengirim'  => $request->no_telp_pengirim,    
                'no_telp_penerima'  => $request->no_telp_penerima,    
                'tarif'             => $request->tarif,    
                'verifikasi'        => 'pending',
                'keterangan'        => $request->keterangan,
                'id_perusahaan'     => Session::get('id_perusahaan'),
                'id_cabang'         => Session::get('id_cabang'),
                'perkiraan_tanggal_sampai'  => $perkiraan,
            ]
        )
        ->post();
        dd($store);
        return redirect()->route('cabang.pengiriman')->with('success', 'Pengiriman ditambahkan!');

    }

}
