<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Session;
use DataTables;

class CeoController extends Controller
{
    private $access_token;
    private $gatewayUrl;

    public function __construct(Request $request)
    {
        $email_verified = Session::get('email_verified_at');
        $this->access_token = Session::get('access_token');
        
        if ($email_verified == NULL)
        {
            return view('home.tidak_aktif');    
        } else
        {
            return view('ceo.dashboard');
        }
    }

    public function index()
    {
        return view('ceo.dashboard');
    }

    public function regional()
    {
        return view('ceo.regional');
    }

    public function getRegional()
    {
        $resp = Curl::to(config('app.gatewayUrl').'ceo/regional/perusahaan/'.Session::get('id_perusahaan'))
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->get();
        $r = json_decode($resp);
        return Datatables::of($r)
                ->addIndexColumn()
                ->addColumn('aksi', function ($r) {
                    return '<center>
                            <a href="'. route('delete_regional', $r->id) .'" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a>&nbsp
                            <button id="btn_edit" data-id="'.$r->id.'" class="btn btn-sm btn-info"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></center>';
                    })
                ->rawColumns(['aksi' => 'aksi'])
                ->make(true);
    }

    public function showRegional($id)
    {
        $resp = Curl::to(config('app.gatewayUrl').'ceo/regional/'.$id)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->get();
        // dd($resp);
        return response($resp);
    }

    public function storeRegional(Request $request)
    {
        $resp = Curl::to(config('app.gatewayUrl').'ceo/regional')
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->withData(
                    [
                        'id_perusahaan' => session::get('id_perusahaan'),
                        'provinsi'      => $request->provinsi,
                        'kota'          => $request->kota,
                        'alamat'        => $request->alamat,
                        'no_telp'       => $request->no_telp,
                        'email'         => $request->email,
                    ]
                )
                ->post();
        return redirect()->route('regional')->with('success', 'Regional ditambahkan');
    }
    
    public function deleteRegional($id)
    {
        $resp = Curl::to(config('app.gatewayUrl').'ceo/regional/'.$id)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->delete();

        return redirect()->route('regional')->with('success', 'Regional dihapus');
    }

    public function updateRegional(Request $request)
    {
        $resp = Curl::to(config('app.gatewayUrl').'ceo/regional/'.$request->id)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->withData(
                    [
                        'provinsi'      => $request->provinsi_edit,
                        'kota'          => $request->kota_edit,
                        'alamat'        => $request->alamat_edit,
                    ]
                )
                ->put();
        return redirect()->route('regional')->with('success', 'Regional diperbarui');
    }

    public function picRegional()
    {
        return view('ceo.pic_regional');
    }

    public function getPicRegional()
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/getPegawai/'.Session::get('id_perusahaan'))
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->get();
        $r = json_decode($resp);
        return Datatables::of($r)
                ->addIndexColumn()
                ->addColumn('aksi', function ($r) {
                    return '<center>
                            <a href="'. route('delete_pic_regional', $r->id_user) .'" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a>&nbsp
                            <button id="btn_edit" data-id="'.$r->id_user.'" class="btn btn-sm btn-info"><i class="fa fa-edit" aria-hidden="true"></i> Lihat</button></center>';
                    })
                ->rawColumns(['aksi' => 'aksi'])
                ->addColumn('ttl', function ($r){
                    return $r->tempat_lahir.', '.$r->tanggal_lahir;
                })
                ->addColumn('regional', function ($r){
                    return $r->provinsi.'-'.$r->kota;
                })
                ->make(true);
    }

    public function showPicRegional($id)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/showPegawai/'.$id)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->get();
        return response($resp);
    }

    public function storePicRegional(Request $request)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/storePegawai')
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->withData(
                    [
                        'email'             => $request->email,
                        'password'          => $request->password,
                        'id_perusahaan'     => session::get('id_perusahaan'),
                        'id_regional'       => $request->id_regional,
                        'nama_lengkap'      => $request->nama_lengkap,
                        'tanggal_lahir'     => $request->tanggal_lahir,
                        'tempat_lahir'      => $request->tempat_lahir,
                        'no_telp'           => $request->no_telp,
                        'alamat'            => $request->alamat,
                        'nik'               => $request->nik,
                        'foto_diri'         => 'defaultuser.jpg',
                        'foto_ktp'          => 'defaultktp.jpg',
                        'foto_npwp'         => 'defaultnpwp.jpg',
                    ]
                )
                ->post();
        return redirect()->route('pic_regional')->with('success', 'PIC / pegawai regional ditambahkan');
    }
    
    public function deletePicRegional($id)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/deletePegawai/'.$id)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->delete();
        return redirect()->route('pic_regional')->with('success', 'PIC / Pegawai dihapus');
    }

    public function cabang()
    {
        return view('ceo.cabang');
    }

    public function getCabang()
    {
        $resp = Curl::to(config('app.gatewayUrl').'ceo/cabang/perusahaan/'.Session::get('id_perusahaan'))
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->get();
        $r = json_decode($resp);
        return Datatables::of($r)
                ->addIndexColumn()
                ->make(true);
    }

    public function TarifPelanggan()
    {
        return view('ceo.tarif_pelanggan');
    }

    public function getTarifPelanggan()
    {
        $resp = Curl::to(config('app.gatewayUrl').'ceo/tarifPelanggan/perusahaan/'.Session::get('id_perusahaan'))
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->get();
        $r = json_decode($resp);
        return Datatables::of($r)
                ->addIndexColumn()
                ->addColumn('tarif', function ($r){
                    return 'Rp.'.$r->tarif;
                })
                ->make(true);
    }

    public function pengiriman()
    {
        return view('ceo.pengiriman');
    }

    public function getPengiriman()
    {
        $pengiriman = Curl::to(config('app.gatewayUrl').'ceo/pengiriman/'.Session::get('id_perusahaan'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $r = json_decode($pengiriman);

        return Datatables::of($r)
            ->addIndexColumn()
            ->addColumn('aksi', function ($r) {
                return '<center>
                        <button id="btn_edit" data-id="'.$r->id.'"class="btn btn-sm btn-success"><i class="fa fa-pencil-alt"></i> Detail</button>
                        </center>';
                })
            ->addColumn('no_resi', function ($r) {
                return $r->no_resi;
            })
            ->addColumn('kota_tujuan', function ($r) {
                return $r->kota_tujuan;
            })
            ->addColumn('kota_asal', function ($r) {
                return $r->kota_asal;
            })
            ->addColumn('nama_cabang', function ($r) {
                return $r->nama_cabang;
            })
            ->addColumn('jenis_pengiriman', function ($r) {
                if ($r->jenis_pengiriman == 'regular')
                {
                    return '<center>
                        <button class="btn btn-sm btn-warning">REGULAR</button>
                        </center>';
                    } else {
                    
                    return '<center>
                        <button class="btn btn-sm btn-info">EXPRESS</button>
                        </center>';
                }
            })
            ->addColumn('jenis_pengangkutan', function ($r) {
                if ($r->jenis_pengangkutan == 'kilo')
                {
                    return '<center>
                        <button class="btn btn-sm btn-warning">KILO</button>
                        </center>';
                    } else if ($r->jenis_pengangkutan == 'dimensional') {
                    
                    return '<center>
                        <button class="btn btn-sm btn-info">DIMENSIONAL</button>
                        </center>';
                    } else {
                        return '<center>
                            <button class="btn btn-sm btn-success">KOLI</button>
                            </center>';
                }
            })
            ->addColumn('ukuran', function ($r) {
                if ($r->jenis_pengangkutan == 'kilo')
                {
                    return $r->berat.' Kilo';
                    } else if ($r->jenis_pengangkutan == 'dimensional') {
                    
                        return $r->dimensi.' Meter Persegi';

                    } else {
                        return $r->total_unit.' Unit';

                }
            })
            ->rawColumns(['aksi' => 'aksi', 'jenis_pengangkutan' => 'jenis_pengangkutan', 'jenis_pengiriman' => 'jenis_pengiriman'])
            ->make(true);
    }

    public function showPengiriman($id)
    {
        $pengiriman = Curl::to(config('app.gatewayUrl').'ceo/pengiriman/id/'.$id)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        // dd($pengiriman); die;
        return response($pengiriman);
    }

}
