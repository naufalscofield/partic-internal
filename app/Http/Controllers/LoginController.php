<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Session;

class LoginController extends Controller
{
    public function __construct()
    {
        //
    }

    public function index(Request $request)
    {
        if ($request->session()->has('access_token'))
        {
            return redirect()->route('dashboard');
        } else 
        {
            return view('login');
        }
        
    }


    public function login(Request $request)
    {
        $response = Curl::to(config('app.gatewayUrl').'login')
        ->withData( 
                [
                    'id_perusahaan' => $request->id_perusahaan,
                    'email'         => $request->email,
                    'password'      => $request->password,
                ]
            )
        ->post();

        $resp = json_decode($response);

        if ( isset($resp->code) )
        {
            $respFinal = [
                'resp' => json_decode($response),
            ];
            return response($respFinal);
        } else {
            $response_profile = Curl::to(config('app.gatewayUrl').'profil')
            ->withHeader('Authorization: Bearer ' . $resp->access_token)
            ->withData(
                [
                    'email' => $request->email
                ]
            )
            ->post();

            $resp_profile = json_decode($response_profile);

            if ($resp_profile->DataUser->email_verified_at == NULL)
            {
                Session::put('id', $resp_profile->DataUser->id);
                Session::put('email', $resp_profile->DataUser->email);
                Session::put('at', $resp->access_token);
                Session::put('nama_lengkap', $resp_profile->DataBiodata->nama_lengkap);

                return view('home.tidak_aktif');
            } else 
            {
                $request->session()->put('id', $resp_profile->DataUser->id);
                $request->session()->put('email', $resp_profile->DataUser->email);
                $request->session()->put('id_perusahaan', $resp_profile->DataUser->id_perusahaan);
                $request->session()->put('id_regional', $resp_profile->DataUser->id_regional);
                $request->session()->put('id_cabang', $resp_profile->DataUser->id_cabang);
                $request->session()->put('role', $resp_profile->DataUser->role);
                $request->session()->put('email_verified_at', $resp_profile->DataUser->email_verified_at);
                $request->session()->put('nama_lengkap', $resp_profile->DataBiodata->nama_lengkap);
                $request->session()->put('tanggal_lahir', $resp_profile->DataBiodata->tanggal_lahir);
                $request->session()->put('tempat_lahir', $resp_profile->DataBiodata->tempat_lahir);
                $request->session()->put('alamat', $resp_profile->DataBiodata->alamat);
                $request->session()->put('no_telp', $resp_profile->DataBiodata->no_telp);
                $request->session()->put('foto_ktp', $resp_profile->DataBiodata->foto_ktp);
                $request->session()->put('foto_diri', $resp_profile->DataBiodata->foto_diri);
                $request->session()->put('foto_npwp', $resp_profile->DataBiodata->foto_npwp);
                $request->session()->put('nik', $resp_profile->DataBiodata->nik);
                $request->session()->put('nama_perusahaan', $resp_profile->DataPerusahaan->nama_perusahaan);
                $request->session()->put('alamat_perusahaan', $resp_profile->DataPerusahaan->alamat);
                $request->session()->put('email_perusahaan', $resp_profile->DataPerusahaan->email);
                $request->session()->put('no_telp_perusahaan', $resp_profile->DataPerusahaan->no_telp);
                $request->session()->put('logo', $resp_profile->DataPerusahaan->logo);
                $request->session()->put('access_token', $resp->access_token);

                $respFinal = [
                    'resp' => $response,
                    'ses' => Session::all(),
                    'role' => $resp_profile->DataUser->role
                ];
                return response($respFinal);
            }
        }

    }

    public function logout()
    {
        Session::flush();
        return redirect()->route('login');
    }

}
