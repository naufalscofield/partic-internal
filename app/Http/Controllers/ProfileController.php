<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Session;
use DataTables;

class ProfileController extends Controller
{
    private $access_token;
    private $gatewayUrl;

    public function __construct(Request $request)
    {
        $email_verified = Session::get('email_verified_at');
        $this->access_token = Session::get('access_token');
        $role = Session::get('role');
        
        if ($email_verified == NULL)
        {
            return view('home.tidak_aktif');    
        } else
        {
            if ($role == 'ceo') {
                return view('ceo.dashboard');
            } else if ($role == 'regional'){
                return view('regional.dashboard');
            } else{
                return view('cabang.dashboard');
            }
            
        }
    }

    public function profile()
    {
        $role = Session::get('role');

        if ($role == 'ceo') {
            return view('ceo.profile');
        } else if ($role == 'regional'){
            return view('regional.profile');
        } else{
            return view('cabang.profile');
        }
    }

    public function updateProfile(Request $request)
    {
        // Kondisi tidak update semua foto
        if (!isset($request->foto_ktp) && !isset($request->foto_npwp) && !isset($request->foto_diri))
        {
            $response = Curl::to(config('app.gatewayUrl').'updateProfil/'.Session::get('id'))
            ->withHeader('Authorization: Bearer ' . $this->access_token)
            ->withData( 
                    [
                        'email'                 => $request->email,
                        'nama_lengkap'          => $request->nama_lengkap,
                        'tanggal_lahir'         => $request->tanggal_lahir,
                        'tempat_lahir'          => $request->tempat_lahir,
                        'alamat'                => $request->alamat,
                        'no_telp'               => $request->no_telp,
                        'nik'                   => $request->nik,
                    ]
                )
            ->post();
            $request->session()->put('email', $request->email);
            $request->session()->put('nama_lengkap', $request->nama_lengkap);
            $request->session()->put('tanggal_lahir', $request->tanggal_lahir);
            $request->session()->put('tempat_lahir', $request->tempat_lahir);
            $request->session()->put('alamat', $request->alamat);
            $request->session()->put('no_telp', $request->no_telp);
            $request->session()->put('nik', $request->nik);
            
            return redirect()->route('profile')->with('success', 'Profil diperbarui');
        } 
        // Kondisi tidak update foto diri dan ktp
        else if (!isset($request->foto_diri) && !isset($request->foto_ktp))
        {
            $foto_npwp      = $request->file('foto_npwp');
            $response       = Curl::to(config('app.gatewayUrl').'updateProfil/'.Session::get('id'))
            ->withHeader('Authorization: Bearer ' . $this->access_token)
            ->withData( 
                    [
                        'email'                 => $request->email,
                        'nama_lengkap'          => $request->nama_lengkap,
                        'tanggal_lahir'         => $request->tanggal_lahir,
                        'tempat_lahir'          => $request->tempat_lahir,
                        'alamat'                => $request->alamat,
                        'no_telp'               => $request->no_telp,
                        'nik'                   => $request->nik,
                        'foto_npwp'             => $foto_npwp->getClientOriginalName(),
                    ]
                )
            ->withFile( 'file_foto_npwp', $foto_npwp->getRealPath(), 'image/png', $foto_npwp )
            ->post();
            
            $request->session()->put('email', $request->email);
            $request->session()->put('nama_lengkap', $request->nama_lengkap);
            $request->session()->put('tanggal_lahir', $request->tanggal_lahir);
            $request->session()->put('tempat_lahir', $request->tempat_lahir);
            $request->session()->put('alamat', $request->alamat);
            $request->session()->put('no_telp', $request->no_telp);
            $request->session()->put('nik', $request->nik);
            $request->session()->put('foto_npwp', $foto_npwp->getClientOriginalName());
            
            return redirect()->route('profile')->with('success', 'Profil diperbarui');
        } 
        // Kondisi tidak update foto diri dan npwp
        else if (!isset($request->foto_diri) && !isset($request->foto_npwp))
        {
            $foto_ktp      = $request->file('foto_ktp');
            $response = Curl::to(config('app.gatewayUrl').'updateProfil/'.Session::get('id'))
            ->withHeader('Authorization: Bearer ' . $this->access_token)
            ->withData( 
                    [
                        'email'                 => $request->email,
                        'nama_lengkap'          => $request->nama_lengkap,
                        'tanggal_lahir'         => $request->tanggal_lahir,
                        'tempat_lahir'          => $request->tempat_lahir,
                        'alamat'                => $request->alamat,
                        'no_telp'               => $request->no_telp,
                        'nik'                   => $request->nik,
                        'foto_ktp'              => $foto_ktp->getClientOriginalName(),
                    ]
                )
            ->withFile( 'file_foto_ktp', $foto_ktp->getRealPath(), 'image/png', $foto_ktp )
            ->post();
            
            $request->session()->put('email', $request->email);
            $request->session()->put('nama_lengkap', $request->nama_lengkap);
            $request->session()->put('tanggal_lahir', $request->tanggal_lahir);
            $request->session()->put('tempat_lahir', $request->tempat_lahir);
            $request->session()->put('alamat', $request->alamat);
            $request->session()->put('no_telp', $request->no_telp);
            $request->session()->put('nik', $request->nik);
            $request->session()->put('foto_ktp', $foto_ktp->getClientOriginalName());
            
            return redirect()->route('profile')->with('success', 'Profil diperbarui');
        } 
        // Kondisi tidak update foto ktp dan npwp
        else if (!isset($request->foto_ktp) && !isset($request->foto_npwp))
        {
            $foto_diri      = $request->file('foto_diri');
            $response = Curl::to(config('app.gatewayUrl').'updateProfil/'.Session::get('id'))
            ->withHeader('Authorization: Bearer ' . $this->access_token)
            ->withData( 
                    [
                        'email'                 => $request->email,
                        'nama_lengkap'          => $request->nama_lengkap,
                        'tanggal_lahir'         => $request->tanggal_lahir,
                        'tempat_lahir'          => $request->tempat_lahir,
                        'alamat'                => $request->alamat,
                        'no_telp'               => $request->no_telp,
                        'nik'                   => $request->nik,
                        'foto_diri'             => $foto_diri->getClientOriginalName(),
                    ]
                )
            ->withFile( 'file_foto_diri', $foto_diri->getRealPath(), 'image/png', $foto_diri )
            ->post();
            
            $request->session()->put('email', $request->email);
            $request->session()->put('nama_lengkap', $request->nama_lengkap);
            $request->session()->put('tanggal_lahir', $request->tanggal_lahir);
            $request->session()->put('tempat_lahir', $request->tempat_lahir);
            $request->session()->put('alamat', $request->alamat);
            $request->session()->put('no_telp', $request->no_telp);
            $request->session()->put('nik', $request->nik);
            $request->session()->put('foto_diri', $foto_diri->getClientOriginalName());
            
            return redirect()->route('profile')->with('success', 'Profil diperbarui');
        } 
        // Kondisi tidak update foto diri
        else if (!isset($request->foto_diri))
        {
            $foto_ktp       = $request->file('foto_ktp');
            $foto_npwp      = $request->file('foto_npwp');
            $response = Curl::to(config('app.gatewayUrl').'updateProfil/'.Session::get('id'))
            ->withHeader('Authorization: Bearer ' . $this->access_token)
            ->withData( 
                    [
                        'email'                 => $request->email,
                        'nama_lengkap'          => $request->nama_lengkap,
                        'tanggal_lahir'         => $request->tanggal_lahir,
                        'tempat_lahir'          => $request->tempat_lahir,
                        'alamat'                => $request->alamat,
                        'no_telp'               => $request->no_telp,
                        'nik'                   => $request->nik,
                        'foto_ktp'              => $foto_ktp->getClientOriginalName(),
                        'foto_npwp'             => $foto_npwp->getClientOriginalName(),
                    ]
                )
            ->withFile( 'file_foto_npwp', $foto_npwp->getRealPath(), 'image/png', $foto_npwp )
            ->withFile( 'file_foto_ktp', $foto_ktp->getRealPath(), 'image/png', $foto_ktp )
            ->post();
            
            $request->session()->put('email', $request->email);
            $request->session()->put('nama_lengkap', $request->nama_lengkap);
            $request->session()->put('tanggal_lahir', $request->tanggal_lahir);
            $request->session()->put('tempat_lahir', $request->tempat_lahir);
            $request->session()->put('alamat', $request->alamat);
            $request->session()->put('no_telp', $request->no_telp);
            $request->session()->put('nik', $request->nik);
            $request->session()->put('foto_ktp', $foto_ktp->getClientOriginalName());
            $request->session()->put('foto_npwp', $foto_npwp->getClientOriginalName());
            
            return redirect()->route('profile')->with('success', 'Profil diperbarui');
        } 
        // Kondisi tidak update foto ktp
        else if (!isset($request->foto_ktp))
        {
            $foto_diri       = $request->file('foto_diri');
            $foto_npwp      = $request->file('foto_npwp');
            $response = Curl::to(config('app.gatewayUrl').'updateProfil/'.Session::get('id'))
            ->withHeader('Authorization: Bearer ' . $this->access_token)
            ->withData( 
                    [
                        'email'                 => $request->email,
                        'nama_lengkap'          => $request->nama_lengkap,
                        'tanggal_lahir'         => $request->tanggal_lahir,
                        'tempat_lahir'          => $request->tempat_lahir,
                        'alamat'                => $request->alamat,
                        'no_telp'               => $request->no_telp,
                        'nik'                   => $request->nik,
                        'foto_diri'             => $foto_diri->getClientOriginalName(),
                        'foto_npwp'             => $foto_npwp->getClientOriginalName(),
                    ]
                )
            ->withFile( 'file_foto_npwp', $foto_npwp->getRealPath(), 'image/png', $foto_npwp )
            ->withFile( 'file_foto_diri', $foto_diri->getRealPath(), 'image/png', $foto_diri )
            ->post();
            
            $request->session()->put('email', $request->email);
            $request->session()->put('nama_lengkap', $request->nama_lengkap);
            $request->session()->put('tanggal_lahir', $request->tanggal_lahir);
            $request->session()->put('tempat_lahir', $request->tempat_lahir);
            $request->session()->put('alamat', $request->alamat);
            $request->session()->put('no_telp', $request->no_telp);
            $request->session()->put('nik', $request->nik);
            $request->session()->put('foto_diri', $foto_diri->getClientOriginalName());
            $request->session()->put('foto_npwp', $foto_npwp->getClientOriginalName());

            return redirect()->route('profile')->with('success', 'Profil diperbarui');
        } 
        // Kondisi tidak update foto npwp
        else if (!isset($request->foto_npwp))
        {
            $foto_ktp       = $request->file('foto_ktp');
            $foto_diri      = $request->file('foto_diri');
            $response = Curl::to(config('app.gatewayUrl').'updateProfil/'.Session::get('id'))
            ->withHeader('Authorization: Bearer ' . $this->access_token)
            ->withData( 
                    [
                        'email'                 => $request->email,
                        'nama_lengkap'          => $request->nama_lengkap,
                        'tanggal_lahir'         => $request->tanggal_lahir,
                        'tempat_lahir'          => $request->tempat_lahir,
                        'alamat'                => $request->alamat,
                        'no_telp'               => $request->no_telp,
                        'nik'                   => $request->nik,
                        'foto_ktp'              => $foto_ktp->getClientOriginalName(),
                        'foto_diri'             => $foto_diri->getClientOriginalName(),
                    ]
                )
            ->withFile( 'file_foto_diri', $foto_diri->getRealPath(), 'image/png', $foto_diri )
            ->withFile( 'file_foto_ktp', $foto_ktp->getRealPath(), 'image/png', $foto_ktp )
            ->post();
            
            $request->session()->put('email', $request->email);
            $request->session()->put('nama_lengkap', $request->nama_lengkap);
            $request->session()->put('tanggal_lahir', $request->tanggal_lahir);
            $request->session()->put('tempat_lahir', $request->tempat_lahir);
            $request->session()->put('alamat', $request->alamat);
            $request->session()->put('no_telp', $request->no_telp);
            $request->session()->put('nik', $request->nik);
            $request->session()->put('foto_ktp', $foto_ktp->getClientOriginalName());
            $request->session()->put('foto_diri', $foto_diri->getClientOriginalName());
            
            return redirect()->route('profile')->with('success', 'Profil diperbarui');
        } 
        // Kondisi update semua foto
        else 
        {
            $foto_ktp       = $request->file('foto_ktp');
            $foto_npwp      = $request->file('foto_npwp');
            $foto_diri      = $request->file('foto_diri');
            $response = Curl::to(config('app.gatewayUrl').'updateProfil/'.Session::get('id'))
            ->withHeader('Authorization: Bearer ' . $this->access_token)
            ->withData( 
                    [
                        'email'                 => $request->email,
                        'nama_lengkap'          => $request->nama_lengkap,
                        'tanggal_lahir'         => $request->tanggal_lahir,
                        'tempat_lahir'          => $request->tempat_lahir,
                        'alamat'                => $request->alamat,
                        'no_telp'               => $request->no_telp,
                        'nik'                   => $request->nik,
                        'foto_ktp'              => $foto_ktp->getClientOriginalName(),
                        'foto_npwp'             => $foto_npwp->getClientOriginalName(),
                        'foto_diri'             => $foto_diri->getClientOriginalName(),
                    ]
                )
            ->withFile( 'file_foto_npwp', $foto_npwp->getRealPath(), 'image/png', $foto_npwp )
            ->withFile( 'file_foto_ktp', $foto_ktp->getRealPath(), 'image/png', $foto_ktp )
            ->withFile( 'file_foto_diri', $foto_diri->getRealPath(), 'image/png', $foto_diri )
            ->post();
            
            $request->session()->put('email', $request->email);
            $request->session()->put('nama_lengkap', $request->nama_lengkap);
            $request->session()->put('tanggal_lahir', $request->tanggal_lahir);
            $request->session()->put('tempat_lahir', $request->tempat_lahir);
            $request->session()->put('alamat', $request->alamat);
            $request->session()->put('no_telp', $request->no_telp);
            $request->session()->put('nik', $request->nik);
            $request->session()->put('foto_ktp', $foto_ktp->getClientOriginalName());
            $request->session()->put('foto_npwp', $foto_npwp->getClientOriginalName());
            $request->session()->put('foto_diri', $foto_diri->getClientOriginalName());

            return redirect()->route('profile')->with('success', 'Profil diperbarui');
        }
    }

    public function changePassword(Request $request)
    {
        $response = Curl::to(config('app.gatewayUrl').'changePassword')
            ->withHeader('Authorization: Bearer ' . $this->access_token)
            ->withData( 
                    [
                        'id'                     => Session::get('id'),
                        'password_lama'          => $request->password_lama,
                        'password_baru'          => $request->password_baru,
                    ]
                )
            ->put();
            $resp = json_decode($response);
            // dd($response['msg']);
            if ($resp->code != 200)
            {
                return redirect()->route('profile')->with('error', $resp->msg);
            } else{
                return redirect()->route('profile')->with('success', $resp->msg);
            }
    }
}
