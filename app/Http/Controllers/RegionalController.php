<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Ixudra\Curl\Facades\Curl;
use Session;
use DataTables;
use Illuminate\Support\Facades\Input;

class RegionalController extends Controller
{
    private $access_token;
    private $gatewayUrl;

    public function __construct(Request $request)
    {
        $email_verified = Session::get('email_verified_at');
        $this->access_token = Session::get('access_token');
        
        if ($email_verified == NULL)
        {
            return view('home.tidak_aktif');    
        } else
        {
            return view('regional.dashboard');
        }
    }

    public function index()
    {
        return view('regional.dashboard');
    }

    public function cabang()
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();

        $r = json_decode($resp);
        $kota = $r->kota;

        return view('regional.cabang', compact('kota'));
    }

    public function getCabang()
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/cabang/perusahaan/'.Session::get('id_perusahaan'))
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->get();
        $r = json_decode($resp);
        return Datatables::of($r)
                ->addIndexColumn()
                ->addColumn('aksi', function ($r) {
                    return '<center>
                            <a href="'. route('regional.delete_cabang', $r->id_cabang) .'" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a>&nbsp
                            <button id="btn_edit" data-id="'.$r->id_cabang.'" class="btn btn-sm btn-info"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></center>';
                    })
                ->rawColumns(['aksi' => 'aksi'])
                ->make(true);
    }

    public function storeCabang(Request $request)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/cabang')
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->withData(
                    [
                        'id_perusahaan'   => Session::get('id_perusahaan'),
                        'id_regional'     => Session::get('id_regional'),
                        'no_telp'         => $request->no_telp,
                        'nama_cabang'     => $request->nama_cabang,
                        'alamat'          => $request->alamat,
                        'longitude'       => NULL,
                        'latitude'        => NULL,
                    ]
                )
                ->post();
        return redirect()->route('regional.cabang')->with('success', 'Cabang ditambahkan!');
    }

    public function showCabang($id)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/cabang/'.$id)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->get();

        return response($resp);
    }

    public function updateCabang(Request $request)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/cabang/'.$request->id)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->withData(
                    [
                        'id_perusahaan'   => Session::get('id_perusahaan'),
                        'id_regional'     => Session::get('id_regional'),
                        'no_telp'         => $request->no_telp_edit,
                        'nama_cabang'     => $request->nama_cabang_edit,
                        'alamat'          => $request->alamat_edit,
                        'longitude'       => NULL,
                        'latitude'        => NULL,
                    ]
                )
                ->put();
        return redirect()->route('regional.cabang')->with('success', 'Cabang diperbarui!');
    }
    
    public function deleteCabang($id)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/cabang/'.$id)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->delete();
        return redirect()->route('regional.cabang')->with('success', 'Cabang dihapus!');
    }

    public function picCabang()
    {
        return view('regional.pic_cabang');
    }

    public function getPicCabang()
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/picCabang/'.Session::get('id_perusahaan').'/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $r = json_decode($resp);
        return Datatables::of($r)
            ->addIndexColumn()
            ->addColumn('aksi', function ($r) {
                return '<center>
                        <a href="'. route('regional.delete_pic_cabang', $r->id_user) .'" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a>&nbsp
                        <button id="btn_edit" data-id="'.$r->id_user.'" class="btn btn-sm btn-info"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></center>';
                })
            ->addColumn('ttl', function ($r) {
                return $r->tempat_lahir.', '.$r->tanggal_lahir;
            })
            ->addColumn('cabang', function ($r) {
                return $r->nama_cabang;
            })
            ->addColumn('regional', function ($r) {
                return $r->kota;
            })
            ->rawColumns(['aksi' => 'aksi'])
            ->make(true);
    }

    public function getRegional()
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/regional/perusahaan/'.Session::get('id_perusahaan'))
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->get();
        return $resp;
    }

    public function getCabangByIdRegional($id_regional)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/cabang/regional/'.$id_regional)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->get();

        return $resp;
    }

    public function storePicCabang(Request $request)
    {
        $resp = Curl::to(config('app.gatewayUrl').'cabang/storePegawai')
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->withData(
                    [
                        'email'             => $request->email,
                        'id_perusahaan'     => Session::get('id_perusahaan'),
                        'id_regional'       => $request->id_regional,
                        'id_cabang'         => $request->id_cabang,
                        'nama_lengkap'      => $request->nama_lengkap,
                        'tanggal_lahir'     => $request->tanggal_lahir,
                        'tempat_lahir'      => $request->tempat_lahir,
                        'alamat'            => $request->alamat,
                        'no_telp'           => $request->no_telp,
                        'foto_ktp'          => 'defaultktp.png',
                        'foto_npwp'         => 'defaultnpwp.png',
                        'foto_diri'         => 'defaultuser.jpg',
                        'nik'               => $request->nik,
                    ]
                )
                ->post();

        return redirect()->route('regional.pic_cabang')->with('success', 'PIC / Pegawai Cabang ditambahkan!');
    }

    public function showPicCabang($id)
    {
        $resp = Curl::to(config('app.gatewayUrl').'cabang/showPegawai/'.$id)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->get();
        return response($resp);
    }

    public function deletePicCabang($id)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/picCabang/'.$id)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->delete();
        return redirect()->route('regional.pic_cabang')->with('success', 'PIC / Pegawai dihapus');
    }

    public function tarifPelanggan()
    {
        return view('regional.tarif_pelanggan');
    }

    public function getTarifPelanggan()
    {
        // dd('$resp');
        $resp = Curl::to(config('app.gatewayUrl').'regional/tarifPelanggan/perusahaan/'.Session::get('id_perusahaan'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $r = json_decode($resp);
        return Datatables::of($r)
            ->addIndexColumn()
            ->addColumn('aksi', function ($r) {
                return '<center>
                        <a href="'. route('regional.delete_tarif_pelanggan', $r->id) .'" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a>&nbsp
                        <button id="btn_edit" data-id="'.$r->id.'" class="btn btn-sm btn-info"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></center>';
                })
            ->rawColumns(['aksi' => 'aksi'])
            ->make(true);
    }

    public function storeTarifPelanggan(Request $request)
    {
        // dd($request->all());
        $resp = Curl::to(config('app.gatewayUrl').'regional/tarifPelanggan/'.Session::get('id_perusahaan'))
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->withData(
                    [
                        'asal'             => $request->kota_asal,
                        'tujuan'           => $request->kota_tujuan,
                        'tarif'            => $request->tarif,
                    ]
                )
                ->post();

        return redirect()->route('regional.tarif_pelanggan')->with('success', 'Tarif pelanggan ditambahkan!');
    }
    
    public function showTarifPelanggan($idTarif)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/tarifPelanggan/'.$idTarif)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        return response($resp);
    }

    public function updateTarifPelanggan(Request $request)
    {
        // dd($request->all());
        $resp = Curl::to(config('app.gatewayUrl').'regional/tarifPelanggan/'.$request->id_tarif)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->withData(
                    [
                        'asal'             => $request->kota_asal_edit,
                        'tujuan'           => $request->kota_tujuan_edit,
                        'tarif'            => $request->tarif_edit,
                    ]
                )
                ->put();

        return redirect()->route('regional.tarif_pelanggan')->with('success', 'Tarif pelanggan diperbarui!');
    }

    public function deleteTarifPelanggan($idTarif)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/tarifPelanggan/'.$idTarif)
                ->withHeader('Authorization: Bearer ' . $this->access_token)
                ->delete();
        // dd($resp);
        return redirect()->route('regional.tarif_pelanggan')->with('success', 'Tarif dihapus');
    }

    public function mitraKurir()
    {
        return view('regional.mitra');
    }

    public function getMitraKurir()
    {

        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;
        $kotaUrl = str_replace(' ', '-', $kota);

        $resp = Curl::to(config('app.gatewayUrl').'regional/mitra/'.Session::get('id_perusahaan').'/'.$kotaUrl)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        // dd(json_decode($resp));
        $r = json_decode($resp);
        return Datatables::of($r)
            ->addIndexColumn()
            ->addColumn('aksi', function ($r) {
                return '<center>
                        <a href="'. route('regional.delete_pic_cabang', $r->mitra->id) .'" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i> Delete</a>&nbsp
                        <button id="btn_edit" data-id="'.$r->mitra->id.'" class="btn btn-sm btn-info"><i class="fa fa-edit" aria-hidden="true"></i> Lihat Detail</button></center>';
                })
            ->addColumn('email', function ($r) {
                return $r->user->email;
            })
            ->addColumn('nama_lengkap', function ($r) {
                return $r->biodata->nama_lengkap;
            })
            ->addColumn('kendaraan', function ($r) {
                return $r->mitra->jenis_kendaraan.'-'.$r->mitra->nama_kendaraan;
            })
            ->addColumn('plat', function ($r) {
                return $r->mitra->plat_kendaraan;
            })
            ->addColumn('status', function ($r) {
                return $r->mitra->status_ketersediaan;
            })
            ->rawColumns(['aksi' => 'aksi'])
            ->make(true);
    }

    public function showMitraKurir($idMitra)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/mitra/'.$idMitra)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();

        return response($resp);
    }

    public function tarifLine()
    {
        return view('regional.tarif_line');
    }

    public function getTarifLine()
    {
        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;
        $kotaUrl = str_replace(' ', '-', $kota);

        $resp = Curl::to(config('app.gatewayUrl').'regional/tarif/'.Session::get('id_perusahaan').'/'.$kotaUrl)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $r = json_decode($resp);
        // dd($r);
        return Datatables::of($r)
            ->addIndexColumn()
            // ->addColumn('aksi', function ($r) {
            //     return '<center>
            //             <button _mitra="btn_edit" data-id="'.$r->tarif->id_mitra.'" class="btn btn-sm btn-info"><i class="fa fa-eye" aria-hidden="true"></i> Lihat</button></center>';
            //     })
            ->addColumn('mitra', function($r) {
                return $r->tarif->id_mitra.' / '.$r->biodata->nama_lengkap;
            })
            ->addColumn('no_telp', function ($r) {
                return $r->biodata->no_telp;
            })
            ->addColumn('asal', function($r) {
                return $r->tarif->asal;
            })
            ->addColumn('tujuan', function($r) {
                return $r->tarif->tujuan;
            })
            ->addColumn('tarif', function($r) {
                return 'Rp.'.number_format($r->tarif->tarif_normal, 2, ',', '.');
            })
            ->addColumn('jenis_pengiriman', function($r) {
                return $r->tarif->jenis_pengiriman;
            })
            ->addColumn('jenis_pengangkutan', function($r) {
                return $r->tarif->jenis_pengangkutan;
            })
            ->addColumn('nama_barang', function($r) {
                return $r->tarif->nama_barang;
            })
            ->addColumn('durasi', function($r) {
                return $r->tarif->durasi.' Hari';
            })
            ->addColumn('moda', function($r) {
                return $r->tarif->moda;
            })
            ->addColumn('minimal_angkut', function($r) {
                if ($r->tarif->jenis_pengangkutan == 'kilo')
                {
                    return $r->tarif->minimal_berat.' Kilo';
                } else if ($r->tarif->jenis_pengangkutan == 'domensional')
                {
                    return $r->tarif->minimal_dimensi.' meter persegi';
                } else {
                    return $r->tarif->minimal_jumlah.' '.$r->tarif->nama_barang;
                }
            })
            ->addColumn('maksimal_angkut', function($r) {
                if ($r->tarif->jenis_pengangkutan == 'kilo')
                {
                    return $r->tarif->maksimal_berat.' Kilo';
                } else if ($r->tarif->jenis_pengangkutan == 'domensional')
                {
                    return $r->tarif->maksimal_dimensi.' meter persegi';
                } else {
                    return $r->tarif->maksimal_jumlah.' '.$r->tarif->nama_barang;
                }
            })
            ->rawColumns(['aksi' => 'aksi'])
            ->make(true);
    }

    public function pengajuanTarif()
    {
        return view('regional.pengajuan_tarif');
    }

    public function getPengajuanTarif()
    {
        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;
        $kotaUrl = str_replace(' ', '-', $kota);

        $resp = Curl::to(config('app.gatewayUrl').'regional/pengajuanTarif/'.Session::get('id_perusahaan').'/'.$kotaUrl)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $r = json_decode($resp);
        return Datatables::of($r)
            ->addIndexColumn()
            ->addColumn('aksi', function ($r) {
                return '<center>
                <form action="'. route('regional.update_pengajuan_tarif', 'acc') .'" method="POST">
                <input type="hidden" name="_token" id="csrf-token" value="'.Session::token() .'" />
                <input type="hidden" value="'.$r->tarif->id_tarif.'" name="id_tarif">
                <button type="submit" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Acc</button>
                </form>
                <button id="btn_tolak" data-id="'.$r->tarif->id_tarif.'" class="btn btn-sm btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Tolak</button></center>';
        })
            ->addColumn('mitra', function($r) {
                return $r->tarif->id_mitra.' / '.$r->biodata->nama_lengkap;
            })
            ->addColumn('no_telp', function ($r) {
                return $r->biodata->no_telp;
            })
            ->addColumn('asal', function($r) {
                return $r->tarif->asal;
            })
            ->addColumn('tujuan', function($r) {
                return $r->tarif->tujuan;
            })
            ->addColumn('tarif', function($r) {
                return 'Rp.'.number_format($r->tarif->tarif_normal, 2, ',', '.');
            })
            ->addColumn('jenis_pengiriman', function($r) {
                return $r->tarif->jenis_pengiriman;
            })
            ->addColumn('jenis_pengangkutan', function($r) {
                return $r->tarif->jenis_pengangkutan;
            })
            ->addColumn('nama_barang', function($r) {
                return $r->tarif->nama_barang;
            })
            ->addColumn('durasi', function($r) {
                return $r->tarif->durasi.' Hari';
            })
            ->addColumn('moda', function($r) {
                return $r->tarif->moda;
            })
            ->addColumn('minimal_angkut', function($r) {
                if ($r->tarif->jenis_pengangkutan == 'kilo')
                {
                    return $r->tarif->minimal_berat.' Kilo';
                } else if ($r->tarif->jenis_pengangkutan == 'domensional')
                {
                    return $r->tarif->minimal_dimensi.' meter persegi';
                } else {
                    return $r->tarif->minimal_jumlah.' '.$r->tarif->nama_barang;
                }
            })
            ->addColumn('maksimal_angkut', function($r) {
                if ($r->tarif->jenis_pengangkutan == 'kilo')
                {
                    return $r->tarif->maksimal_berat.' Kilo';
                } else if ($r->tarif->jenis_pengangkutan == 'domensional')
                {
                    return $r->tarif->maksimal_dimensi.' meter persegi';
                } else {
                    return $r->tarif->maksimal_jumlah.' '.$r->tarif->nama_barang;
                }
            })
            ->rawColumns(['aksi' => 'aksi'])
            ->make(true);
    }

    public function updatePengajuanTarif($status, Request $request)
    {
        if ($status ==  'tolak')
        {
            $resp = Curl::to(config('app.gatewayUrl').'regional/pengajuanTarif')
                    ->withHeader('Authorization: Bearer ' . $this->access_token)
                    ->withData(
                        [
                            'id_tarif'                   => $request->id_tarif,
                            'status_verifikasi'          => 2,
                            'keterangan_penolakan'       => $request->keterangan_penolakan,
                        ]
                    )
                    ->put();
                    return redirect()->route('regional.pengajuan_tarif')->with('success', 'Pengajuan Tarif ditolak!');
        } else {
            $resp = Curl::to(config('app.gatewayUrl').'regional/pengajuanTarif')
            ->withHeader('Authorization: Bearer ' . $this->access_token)
            ->withData(
                [
                    'id_tarif'                   => $request->id_tarif,
                    'status_verifikasi'          => 1,
                    'keterangan_penolakan'       => NULL,
                    ]
                    )
                    ->put();
                    return redirect()->route('regional.pengajuan_tarif')->with('success', 'Pengajuan Tarif diterima!');
        }
        
    }

    public function member()
    {
        return view('regional.member'); 
    }

    public function getMember($no)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/member/'.Session::get('id_perusahaan').'/'.$no)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $r = json_decode($resp);
        return Datatables::of($r)
            ->addIndexColumn()
            ->addColumn('aksi', function ($r) {
                if ($r->member->verifikasi == 2) {
                    return '<center>
                            <a href="'. route('regional.verifikasi_member', $r->member->id) .'" class="btn btn-sm btn-success"><i class="fa fa-check"></i> Verifikasi</a>
                            </center>';
                    }
                })
            ->addColumn('email', function ($r) {
                return $r->user->email;
            })
            ->addColumn('nama_lengkap', function ($r) {
                return $r->biodata->nama_lengkap;
            })
            ->addColumn('alamat', function ($r) {
                return $r->biodata->alamat;
            })
            ->addColumn('no_telp', function ($r) {
                return $r->biodata->no_telp;
            })
            ->addColumn('status', function ($r) {
                if ($r->member->verifikasi == 1)
                {
                    return 'Terverifikasi';
                } else {
                    return 'Belum Terverifikasi';
                }
            })
            ->addColumn('ttl', function ($r) {
                return $r->biodata->tempat_lahir.', '.$r->biodata->tanggal_lahir;
            })
            ->addColumn('foto', function ($r) {
                return '<center><img style="width:50px;height:50px" src="'.config('app.storage').'foto_diri/'.$r->biodata->foto_diri.'"</center>';
            })
            ->rawColumns(['aksi' => 'aksi', 'foto' => 'foto'])
            // ->rawColumns(['foto' => 'foto'])
            ->make(true);
    }
    
    public function verifikasiMember($id)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/member/verifikasi/'.$id)
                    ->withHeader('Authorization: Bearer ' . $this->access_token)
                    ->put();


        return redirect()->route('regional.member')->with('success', 'Member diverifikasi!');
    }

    public function profilKantor()
    {
        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);

        return view('regional.profil_kantor', compact('regional'));
    }

    public function updateProfilKantor(Request $request)
    {
        $resp = Curl::to(config('app.gatewayUrl').'regional/updateRegional/'.$request->id)
                    ->withHeader('Authorization: Bearer ' . $this->access_token)
                    ->withData(
                        [
                            'no_telp' => $request->no_telp,
                            'alamat' => $request->alamat,
                            'email' => $request->email
                        ]
                    )
                    ->put();
        return redirect()->route('regional.profil_kantor')->with('success', 'Profil diperbarui!');
    }

    public function pengiriman()
    {
        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;
        return view('regional.pengiriman', compact('kota'));
    }

    public function getPengiriman()
    {
        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;
        $kotaUrl = str_replace(' ', '-', $kota);

        $pengiriman = Curl::to(config('app.gatewayUrl').'regional/pengiriman/kota/'.Session::get('id_perusahaan').'/'.$kotaUrl)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $r = json_decode($pengiriman);

        return Datatables::of($r)
            ->addIndexColumn()
            ->addColumn('aksi', function ($r) {
                return '<center>
                        <button id="btn_edit" data-id="'.$r->id.'"class="btn btn-sm btn-success"><i class="fa fa-pencil-alt"></i> Detail</button>
                        </center>';
                })
            ->addColumn('no_resi', function ($r) {
                return $r->no_resi;
            })
            ->addColumn('kota_tujuan', function ($r) {
                return $r->kota_tujuan;
            })
            ->addColumn('jenis_pengiriman', function ($r) {
                if ($r->jenis_pengiriman == 'regular')
                {
                    return '<center>
                        <button class="btn btn-sm btn-warning">REGULAR</button>
                        </center>';
                    } else {
                    
                    return '<center>
                        <button class="btn btn-sm btn-info">EXPRESS</button>
                        </center>';
                }
            })
            ->addColumn('jenis_pengangkutan', function ($r) {
                if ($r->jenis_pengangkutan == 'kilo')
                {
                    return '<center>
                        <button class="btn btn-sm btn-warning">KILO</button>
                        </center>';
                    } else if ($r->jenis_pengangkutan == 'dimensional') {
                    
                    return '<center>
                        <button class="btn btn-sm btn-info">DIMENSIONAL</button>
                        </center>';
                    } else {
                        return '<center>
                            <button class="btn btn-sm btn-success">KOLI</button>
                            </center>';
                }
            })
            ->addColumn('ukuran', function ($r) {
                if ($r->jenis_pengangkutan == 'kilo')
                {
                    return $r->berat.' Kilo';
                    } else if ($r->jenis_pengangkutan == 'dimensional') {
                    
                        return $r->dimensi.' Meter Persegi';

                    } else {
                        return $r->total_unit.' Unit';

                }
            })
            ->rawColumns(['aksi' => 'aksi', 'jenis_pengangkutan' => 'jenis_pengangkutan', 'jenis_pengiriman' => 'jenis_pengiriman'])
            ->make(true);
    }

    public function showPengiriman($id)
    {
        $pengiriman = Curl::to(config('app.gatewayUrl').'regional/pengiriman/'.$id)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        return response($pengiriman);
    }

    public function kelompokPengiriman()
    {
        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;

        return view('regional.kelompok_pengiriman', compact('kota'));
    }

    public function getKelompokPengiriman()
    {
        $pengiriman = Curl::to(config('app.gatewayUrl').'regional/kelompok_pengiriman/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $r = json_decode($pengiriman);
        return Datatables::of($r)
            ->addIndexColumn()
            ->addColumn('mitra', function ($r) {
                return $r->id_mitra.' / '.$r->nama_lengkap;
            })
            ->addColumn('tarif_line', function ($r) {
                return $r->id_tarif_line.' / '.'Rp.'.$r->tarif_normal;
            })
            ->addColumn('tanggal_pickup', function ($r) {
                if ($r->tanggal_pickup == NULL)
                {
                    return "-";
                } else{
                    return $r->tanggal_pickup;
                }
            })
            ->addColumn('tujuan', function ($r) {
                return $r->tujuan_pengiriman;
            })
            ->addColumn('asal', function ($r) {
                return $r->asal_tujuan;
            })
            ->addColumn('jenis_pengangkutan', function ($r) {
                return $r->jenis_angkut;
            })
            ->addColumn('jenis_pengiriman', function ($r) {
                return $r->jenis_kirim;
            })
            ->addColumn('ukuran', function ($r) {
                if ($r->jenis_angkut == 'kilo')
                {
                    return $r->berat_total.' Kilo';
                    } else if ($r->jenis_angkut == 'dimensional') {
                    
                        return $r->dimensi_total.' Meter Persegi';

                    } else {
                        return $r->unit_total.' Unit';

                }
            })
            ->addColumn('aksi', function ($r) {
                if ($r->id_mitra == NULL && $r->calon_mitra1 == NULL)
                {
                    return '<center>
                            <button id="btn_promethee" data-id="'.$r->id_kelompok_pengiriman.'"class="btn btn-sm btn-success"><i class="fa fa-calculator"></i> Metode Promethee</button>
                            </center>';
                        } else if ($r->id_mitra != NULL && $r->calon_mitra1 != NULL) { 
                    return '<center>
                            <button id="btn_smart" data-id="'.$r->id_kelompok_pengiriman.'"class="btn btn-sm btn-info"><i class="fa fa-calculator"></i> Metode Smart</button>
                            </center>';
                        }
            })
            ->rawColumns(['aksi' => 'aksi', 'ukuran' => 'ukuran', 'tanggal_pickup' => 'tanggal_pickup', 'asal' => 'asal', 'jenis_pengangkutan' => 'jenis_pengangkutan', 'jenis_pengiriman' => 'jenis_pengiriman'])
            ->make(true);
    }

    public function getFieldKelompokPengiriman()
    {
        $id_perusahaan  = Session::get('id_perusahaan');
        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;
        $kotaUrl = str_replace(' ', '-', $kota);
        
        $kelompok = Curl::to(config('app.gatewayUrl').'regional/field_kelompok_pengiriman/'.$kotaUrl.'/'.$id_perusahaan)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        return response($kelompok);
    }

    public function getField2KelompokPengiriman($tujuan)
    {
        $id_perusahaan  = Session::get('id_perusahaan');
        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;
        $kotaUrl = str_replace(' ', '-', $kota);
        $tujuanUrl = str_replace(' ', '-', $tujuan);

        $kelompok = Curl::to(config('app.gatewayUrl').'regional/field2_kelompok_pengiriman/'.$tujuanUrl.'/'.$kotaUrl.'/'.$id_perusahaan)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        return response($kelompok);
    }

    public function getField3KelompokPengiriman($tujuan, $jenis_pengangkutan)
    {
        $id_perusahaan  = Session::get('id_perusahaan');
        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;
        $kotaUrl = str_replace(' ', '-', $kota);
        $tujuanUrl = str_replace(' ', '-', $tujuan);

        $kelompok = Curl::to(config('app.gatewayUrl').'regional/field3_kelompok_pengiriman/'.$tujuanUrl.'/'.$jenis_pengangkutan.'/'.$kotaUrl.'/'.$id_perusahaan)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        return response($kelompok);
    }

    public function getField4KelompokPengiriman($tujuan, $jenis_pengangkutan, $jenis_pengiriman)
    {
        $id_perusahaan  = Session::get('id_perusahaan');
        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;
        $kotaUrl = str_replace(' ', '-', $kota);
        $tujuanUrl = str_replace(' ', '-', $tujuan);

        $kelompok = Curl::to(config('app.gatewayUrl').'regional/field4_kelompok_pengiriman/'.$tujuanUrl.'/'.$jenis_pengangkutan.'/'.$jenis_pengiriman.'/'.$kotaUrl.'/'.$id_perusahaan)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        return response($kelompok);
    }

    public function storeKelompokPengiriman(Request $request)
    {
        $regional = Curl::to(config('app.gatewayUrl').'regional/regional/'.Session::get('id_regional'))
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        $regional = json_decode($regional);
        $kota = $regional->kota;

        $resp = Curl::to(config('app.gatewayUrl').'regional/kelompok_pengiriman')
                    ->withHeader('Authorization: Bearer ' . $this->access_token)
                    ->withData(
                        [
                            'id_perusahaan'     => Session::get('id_perusahaan'),
                            'id_regional'       => Session::get('id_regional'),
                            'asal'              => $kota,
                            'tujuan'            => $request->tujuan,
                            'jenis_pengangkutan'=> $request->jenis_pengangkutan,
                            'jenis_pengiriman'  => $request->jenis_pengiriman,
                            'berat_total'       => $request->berat,
                            'dimensi_total'     => $request->dimensi,
                            'unit_total'        => $request->unit,
                        ]
                    )
                    ->post();
        return redirect()->route('regional.kelompok_pengiriman')->with('success', 'Kelompok pengiriman ditambahkan!');
    }

    public function promethee($id)
    {
        $promethee = Curl::to(config('app.gatewayUrl').'regional/promethee/'.$id)
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->get();
        return response($promethee);
    }

    public function storePromethee(Request $request)
    {
        $promethee = Curl::to(config('app.gatewayUrl').'regional/promethee')
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->withData(
            [
                'id_kelompok_pengiriman'    => $request->id_kelompok_pengiriman,
                'calon_mitra1'              => $request->calon_mitra1,
                'calon_mitra2'              => $request->calon_mitra2,
                'calon_mitra3'              => $request->calon_mitra3,
                'id_tarif_line1'            => $request->id_tarif_line1,
                'id_tarif_line2'            => $request->id_tarif_line2,
                'id_tarif_line3'            => $request->id_tarif_line3,
            ]
        )
        ->put();
        $resp = json_decode($promethee);
        if ($resp->status == 200)
        {
            return redirect()->route('regional.kelompok_pengiriman')->with('success', 'Mitra Kurir Terpilih!');
        } else {
            dd($promethee);
        }
    }

    public function storeSmart(Request $request)
    {
        $smart = Curl::to(config('app.gatewayUrl').'regional/smart')
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->withData(
            [
                'id_kelompok_pengiriman'    => $request->id_kelompok_pengiriman,
                'durasi'                    => $request->durasi,
                'tarif'                     => $request->tarif,
                'catatan_hitam'             => $request->catatan_hitam,
            ]
        )
        ->post();
        return response($smart);
    }

    public function storeSmartFinal(Request $request)
    {
        $smart = Curl::to(config('app.gatewayUrl').'regional/smartFinal')
        ->withHeader('Authorization: Bearer ' . $this->access_token)
        ->withData(
            [
                'id_kelompok_pengiriman'    => $request->idKelompokPengiriman,
                'id_mitra'                  => $request->idMitraFix,
                'calon_mitra1'              => NULL,
                'calon_mitra2'              => NULL,
                'calon_mitra3'              => NULL,
                'id_tarif_line'             => $request->idTarifFix,
                'id_tarif_line1'            => NULL,
                'id_tarif_line2'            => NULL,
                'id_tarif_line3'            => NULL,
            ]
        )
        ->put();
    }
    
}
