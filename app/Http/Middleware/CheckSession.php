<?php

namespace App\Http\Middleware;

use Closure;

class CheckSession
{

    public function handle($request, Closure $next)
    {
        if (!$request->session()->exists('access_token')) {
            // username value cannot be found in session
            return redirect('/login');
        }

        return $next($request);
    }

}