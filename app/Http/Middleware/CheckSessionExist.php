<?php

namespace App\Http\Middleware;

use Closure;

class CheckSessionExist
{

    public function handle($request, Closure $next)
    {
        if ($request->session()->exists('access_token')) {
            // username value cannot be found in session
            return redirect('/dashboard');
        }

        return $next($request);
    }

}