<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use Session;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ... $roles)
    {
        $roleSess = Session::get('role');
        // dd($roleSess);
        if($roleSess) {
            foreach($roles as $role) {
                
                if($role == "ceo" && $roleSess == "ceo") {
                    return $next($request);
                }
                
                if($role == "regional" && $roleSess == "regional") {
                    // dd('$role');
                    return $next($request);
                }

                if($role == "cabang" && $roleSess == "cabang") {
                    return $next($request);
                }

                if($role == "mitra" && $roleSess == "mitra") {
                    return $next($request);
                }

                if($role == "member" && $roleSess == "member") {
                    return $next($request);
                }
                
            }
        }

        // return response()->json([
        //     'code' => 500,
        //     'message' => 'Permission denied! '.$roleSess
        // ], 500);

        return redirect()->route('access_denied');

        // return view('home.access_denied');
    }
}
